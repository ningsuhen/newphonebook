
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Appirater
#define COCOAPODS_POD_AVAILABLE_Appirater
#define COCOAPODS_VERSION_MAJOR_Appirater 2
#define COCOAPODS_VERSION_MINOR_Appirater 0
#define COCOAPODS_VERSION_PATCH_Appirater 4

// CocoaLumberjack
#define COCOAPODS_POD_AVAILABLE_CocoaLumberjack
#define COCOAPODS_VERSION_MAJOR_CocoaLumberjack 2
#define COCOAPODS_VERSION_MINOR_CocoaLumberjack 0
#define COCOAPODS_VERSION_PATCH_CocoaLumberjack 0

// CocoaLumberjack/Core
#define COCOAPODS_POD_AVAILABLE_CocoaLumberjack_Core
#define COCOAPODS_VERSION_MAJOR_CocoaLumberjack_Core 2
#define COCOAPODS_VERSION_MINOR_CocoaLumberjack_Core 0
#define COCOAPODS_VERSION_PATCH_CocoaLumberjack_Core 0

// CocoaLumberjack/Default
#define COCOAPODS_POD_AVAILABLE_CocoaLumberjack_Default
#define COCOAPODS_VERSION_MAJOR_CocoaLumberjack_Default 2
#define COCOAPODS_VERSION_MINOR_CocoaLumberjack_Default 0
#define COCOAPODS_VERSION_PATCH_CocoaLumberjack_Default 0

// CocoaLumberjack/Extensions
#define COCOAPODS_POD_AVAILABLE_CocoaLumberjack_Extensions
#define COCOAPODS_VERSION_MAJOR_CocoaLumberjack_Extensions 2
#define COCOAPODS_VERSION_MINOR_CocoaLumberjack_Extensions 0
#define COCOAPODS_VERSION_PATCH_CocoaLumberjack_Extensions 0

// DateTools
#define COCOAPODS_POD_AVAILABLE_DateTools
#define COCOAPODS_VERSION_MAJOR_DateTools 1
#define COCOAPODS_VERSION_MINOR_DateTools 5
#define COCOAPODS_VERSION_PATCH_DateTools 0

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 0

// SVWebViewController
#define COCOAPODS_POD_AVAILABLE_SVWebViewController
#define COCOAPODS_VERSION_MAJOR_SVWebViewController 1
#define COCOAPODS_VERSION_MINOR_SVWebViewController 0
#define COCOAPODS_VERSION_PATCH_SVWebViewController 0

