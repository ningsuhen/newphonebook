//
//  MergeItem.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/24/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import CoreData

class MergeItem: NSManagedObject {

    @NSManaged var dcImagePath: String
    @NSManaged var dcName: String
    @NSManaged var dcRecordID: NSNumber
    @NSManaged var dcVCardPath: String
    @NSManaged var mergeDate: NSDate
    @NSManaged var tcImagePath: String
    @NSManaged var tcName: String
    @NSManaged var tcRecordID: NSNumber
    @NSManaged var tcVCardPath: String
    @NSManaged var status: NSNumber
    @NSManaged var parentActivity: Activity

}
