//
//  Favorite.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/7/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import CoreData

class Favorite: NSManagedObject {

    @NSManaged var abRecordID: NSNumber
    @NSManaged var value: String
    @NSManaged var valueType: NSNumber
    @NSManaged var reason: NSNumber
    @NSManaged var priority: NSNumber
    @NSManaged var label: String
    @NSManaged var creationDate: NSDate
    @NSManaged var order: NSNumber
    @NSManaged var lastUpdated: NSDate
    @NSManaged var callLogs: NSOrderedSet
    @NSManaged var callLogsCount: NSNumber
    @NSManaged var isContact: Bool
    var contact: SwiftAddressBookPerson!
}
