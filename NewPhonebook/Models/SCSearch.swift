//
//  NewPhonebook.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/27/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import CoreData

class SCSearch: NSManagedObject {

    @NSManaged var creationDate: NSDate
    @NSManaged var desc: String
    @NSManaged var icon: NSData
    @NSManaged var lastUsed: NSDate
    @NSManaged var scope: String
    @NSManaged var text: String
    @NSManaged var title: String

}
