//
//  Activity.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/28/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import CoreData

class Activity: NSManagedObject {

    @NSManaged var type: NSNumber
    @NSManaged var title: String
    @NSManaged var desc: String
    @NSManaged var startTime: NSDate
    @NSManaged var endTime: NSDate
    @NSManaged var status: NSNumber
    @NSManaged var data: AnyObject?
    @NSManaged var mergeItems: NSOrderedSet
    @NSManaged var cleanItems: NSOrderedSet

}
