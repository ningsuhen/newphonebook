//
//  CleanItem.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/30/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import CoreData

class CleanItem: NSManagedObject {

    @NSManaged var vCardPath: String
    @NSManaged var name: String
    @NSManaged var imagePath: String
    @NSManaged var recordID: NSNumber
    @NSManaged var cleanDate: NSDate
    @NSManaged var parentActivity: Activity

}
