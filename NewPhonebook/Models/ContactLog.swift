//
//  NewPhonebook.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/7/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import CoreData

class ContactLog: NSManagedObject {

    @NSManaged var abRecordID: NSNumber
    @NSManaged var logType: NSNumber
    @NSManaged var startTime: NSDate
    @NSManaged var value: String
    @NSManaged var label: String
    @NSManaged var valueType: NSNumber
    @NSManaged var favoriteItem: Favorite
    @NSManaged var isContact: Bool

    var linkedLogs = [ContactLog]()
}
