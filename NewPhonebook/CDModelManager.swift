//
//  CDModelManager.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/26/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import CoreData

private let _shareCDManagerInstance = CDModelManager()

class CDModelManager {
    var managedObjectContext: NSManagedObjectContext?
    var refreshFlag = false
    
    class func getQueue() -> NSOperationQueue {
        return NSOperationQueue.mainQueue()
    }
    
    class WeakHandler {
        weak var value: SCDataChangeDelegate?
        init(value: SCDataChangeDelegate) {
            self.value = value
        }
    }
    
    var coreDataChangeHandlers = Dictionary<String, [WeakHandler]>()
    func registerCoreDataChangeHandler(handler: SCDataChangeDelegate, entityName: String) {
        logDebug("registerCoreDataChangeHandler called for entity \(entityName)")
        if var handlers = coreDataChangeHandlers[entityName] {
            handlers.append(WeakHandler(value: handler))
            coreDataChangeHandlers[entityName] = handlers
        } else {
            coreDataChangeHandlers[entityName] = [WeakHandler(value: handler)]
        }
        
        handler.onCoreDataChange(entityName)
    }
    
    class var sharedInstance: CDModelManager {
        return _shareCDManagerInstance
    }
    
    init() {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        self.managedObjectContext = appDelegate.managedObjectContext
    }
    class var context: NSManagedObjectContext {
        return CDModelManager.sharedInstance.managedObjectContext!
    }
    
    func notifyCoreDataChangeForEnity(entity: String) {
        NSOperationQueue.mainQueue().addOperationWithBlock({
            logDebug("notifyCoreDataChangeForEnity called for entity: \(entity)")
            if var handlers = self.coreDataChangeHandlers[entity] {
                for index in stride(from: handlers.count - 1, through: 0, by: -1) {
                    let weakHandler = handlers[index]
                    if let handler = weakHandler.value {
                        logDebug("Calling CoreDataChangeHandler at index \(index)")
                        handler.onCoreDataChange(entity)
                    } else {
                        logDebug("Removing CoreDataChangeHandler at index \(index)")
                        handlers.removeAtIndex(index)
                    }
                }
                self.coreDataChangeHandlers[entity] = handlers
            }
        })
    }
    
    var pendingEntitiesToSave = NSMutableSet()
    func addPendingEntityToSave(entityName: String) {
        pendingEntitiesToSave.addObject(entityName)
    }
    
    
    func commit() {
        CDModelManager.getQueue().waitForOperationWithBlock({
            var e: NSError?
            if !CDModelManager.context.save(&e) {
                logError("insert error: \(e!.localizedDescription)")
                abort()
            }
            for entity in self.pendingEntitiesToSave {
                self.notifyCoreDataChangeForEnity(entity as String)
            }
            self.pendingEntitiesToSave.removeAllObjects()
        })
    }
}