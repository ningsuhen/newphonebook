import Foundation
import UIKit


protocol SCDataChangeDelegate: class {
    func onContactsChange(newContacts: [SwiftAddressBookPerson])
    
    func onCoreDataChange(entityName: String!)
}

extension UIViewController: SCDataChangeDelegate {
    func onContactsChange(newContacts: [SwiftAddressBookPerson]) {
        
    }
    
    func onCoreDataChange(entityName: String!) {
        
    }
    
    func showNoConnectionAlert(){
        showAlert(NSLocalizedString("No Internet Connection", comment: ""), message: NSLocalizedString("Try again once you have an internet connection",comment:""))
    }
    
    func showAlert(title: String!, message: String!) {
        if title == nil || message == nil {
            logError("Invalid Arguments supplied to UIViewController.showAlert")
            return
        }
        let cancelButtonTitle = NSLocalizedString("OK", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        // Create the action.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
        }
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    /*func showAlertInMainThread(title: String!, message: String!) {
    if title == nil || message == nil {
    logError("Invalid Arguments supplied to UIViewController.showAlert")
    return
    }
    NSOperationQueue.mainQueue().addOperationWithBlock({
    self.showAlert(title,message:message)
    })
    }*/
    
    func showConfirmAlert(title: String!, message: String!, cancelBlock:((UIAlertAction!) -> Void)?,confirmBlock:((UIAlertAction!) -> Void)?) {
        if title == nil || message == nil {
            logError("Invalid Arguments supplied to UIViewController.showAlert")
        }
        let cancelButtonTitle = NSLocalizedString("Cancel", comment: "")
        let confirmButtonTitle = NSLocalizedString("OK", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        // Create the action.
        var cancelHandler:((UIAlertAction!) -> Void)? = {
            action in
        }
        if cancelBlock != nil{
            cancelHandler = cancelBlock!
        }
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel, handler:cancelHandler)
        alertController.addAction(cancelAction)
        // Create the action.
        var confirmHandler:((UIAlertAction!) -> Void)? = {
            action in
        }
        if confirmBlock != nil{
            confirmHandler = confirmBlock!
        }
        
        let confirmAction = UIAlertAction(title: confirmButtonTitle, style: .Default, handler:confirmHandler)
        alertController.addAction(confirmAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showWaitIndicatorWithProgress(label: String!) -> MBProgressHUD {
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.mode = MBProgressHUDModeDeterminateHorizontalBar
        hud.labelText = label
        UIApplication.sharedApplication().idleTimerDisabled = true
        return hud
    }
    
    func showWaitIndicator(label: String!) {
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.labelText = label
        UIApplication.sharedApplication().idleTimerDisabled = true
    }
    
    func hideWaitIndicator() {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        UIApplication.sharedApplication().idleTimerDisabled = false
    }
    
    func getTextInput(title: String!, message: String!, defaultValue: String!, completion: ((value:String!) -> Void)!) {
        let title = title
        let message = message
        let cancelButtonTitle = NSLocalizedString("Cancel", comment: "")
        let otherButtonTitle = NSLocalizedString("OK", comment: "")
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addTextFieldWithConfigurationHandler {
            textField in
            textField.text = defaultValue
        }
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
        }
        let otherAction = UIAlertAction(title: otherButtonTitle, style: .Default) {
            action in
            let textFields = alertController.textFields as [UITextField]
            completion(value: textFields[0].text)
        }
        alertController.addAction(cancelAction)
        alertController.addAction(otherAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
}

class BaseUIViewController: UIViewController {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if SCABManager.swiftAddressBook == nil {
            let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
            appDelegate.showAddressBookPermissionAlert()
        }
    }
    
    deinit {
        logDebug("BaseUIViewController deinit")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        logWarn("BaseUIViewController.didReceiveMemoryWarning called")
        SCABManager.sharedInstance.cleanMemoryCaches()
    }
}

class BaseUITableViewController: UITableViewController {
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if SCABManager.swiftAddressBook == nil {
            let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
            appDelegate.showAddressBookPermissionAlert()
        }
    }
    
    
    deinit {
        logDebug("BaseUITableViewController deinit")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        logWarn("BaseUITableViewController.didReceiveMemoryWarning called")
        SCABManager.sharedInstance.cleanMemoryCaches()
    }
}
