//
//  ConfirmMergeViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 2/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBookUI

protocol ConfirmMergeDelegate {
    func showMergeCompletedMessage(mergeRecordMap: Dictionary<ABRecordID, ABRecordID>)
}

class ConfirmMergeViewController: BaseUIViewController, MergeTableCellDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var buttonTableView: UITableView!
    
    
    @IBOutlet var navigationBar: UINavigationBar!
    var delegate: ConfirmMergeDelegate?
    
    @IBOutlet var defaultTableView: UITableView!
    var duplicateRecordMap = Dictionary<ABRecordID, ABRecordID>()
    var duplicateRecordIds = Array<ABRecordID>()
    var uncheckedRecordIds = Array<ABRecordID>()
    var invertedMergeRecordIds = Array<ABRecordID>()
    var mergeCompleted = false
    var isActivity = false
    
    
    func setRecords(records: Dictionary<ABRecordID, ABRecordID>) {
        self.duplicateRecordMap = records
        self.duplicateRecordIds = Array(records.keys)
    }
    
    func leftCardTapped(cell: MergeTableCell) {
        if cell.cellIndex < 0 || cell.cellIndex > self.duplicateRecordIds.count {
            logError("leftCardTapped called with invalid cell index")
            return
        }
        let duplicateRecordId = self.duplicateRecordIds[cell.cellIndex]
        if let targetRecordId = self.duplicateRecordMap[duplicateRecordId] {
            if let person = SCABManager.swiftAddressBook?.personFromRecordId(targetRecordId) {
                let personController = ABPersonViewController()
                person.performRecordBlock({
                    (internalRecord: ABRecord) in
                    personController.displayedPerson = internalRecord
                })
                personController.shouldShowLinkedPeople = true
                self.navigationController!.pushViewController(personController, animated: true)
            }
            
        }
    }
    
    func rightCardTapped(cell: MergeTableCell) {
        if cell.cellIndex < 0 || cell.cellIndex > self.duplicateRecordIds.count {
            logError("rightCardTapped called with invalid cell index")
            return
        }
        let duplicateRecordId = self.duplicateRecordIds[cell.cellIndex]
        if let person = SCABManager.swiftAddressBook?.personFromRecordId(duplicateRecordId) {
            let personController = ABPersonViewController()
            person.performRecordBlock({
                (internalRecord: ABRecord) in
                personController.displayedPerson = internalRecord
            })
            personController.shouldShowLinkedPeople = true
            self.navigationController!.pushViewController(personController, animated: true)
        }
    }
    
    
    func buttonTapped(sender: ButtonTableCell) {
        
        var mergeRecordMap = Dictionary<ABRecordID, ABRecordID>()
        for recordId in duplicateRecordIds {
            if SCABManager.sharedInstance.personExistsWithRecordID(self.duplicateRecordMap[recordId]!) &&
                SCABManager.sharedInstance.personExistsWithRecordID(recordId) {
                    if contains(uncheckedRecordIds, recordId) {
                        continue
                    } else if contains(invertedMergeRecordIds, recordId) {
                        mergeRecordMap[self.duplicateRecordMap[recordId]!] = recordId
                    } else {
                        mergeRecordMap[recordId] = self.duplicateRecordMap[recordId]!
                    }
            }
        }
        if mergeRecordMap.count == 0 {
            showAlert(NSLocalizedString("No active duplicate found", comment: ""), message: NSLocalizedString("There are no valid duplicates to merge", comment: ""))
        } else {
            self.mergeDuplicates(mergeRecordMap) {
                self.mergeCompleted = true
                self.navigationController!.popViewControllerAnimated(true)
                self.delegate?.showMergeCompletedMessage(mergeRecordMap)
            }
        }
        sender.selected = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = String(format: NSLocalizedString("%d Duplicates Found", comment: ""), self.duplicateRecordIds.count)
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        self.defaultTableView.registerNib(UINib(nibName: "MergeTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "MergeTableCell")
        self.buttonTableView.registerNib(UINib(nibName: "ButtonTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ButtonTableCell")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == buttonTableView {
            return 1
        }
        return self.duplicateRecordMap.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == buttonTableView {
            return 10
        }
        return 0
    }
    
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if tableView == buttonTableView {
            return NSLocalizedString("Tap to arrows to change direction of Merge.", comment: "")
        }
        return nil
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == buttonTableView {
            return 80
        }
        return 44
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView == buttonTableView {
            var cell: ButtonTableCell! = tableView.dequeueReusableCellWithIdentifier("ButtonTableCell") as? ButtonTableCell
            if cell == nil {
                cell = NSBundle.mainBundle().loadNibNamed("ButtonTableCell", owner: self, options: nil)[0] as? ButtonTableCell
            }
            //            scanButtonImage = cell.icon
            // cell.title.text = "Remove Dummies"
            // //            scanButtonDesc = cell.subtitle
            // cell.progressView.hidden = true
            // cell.progressCount.hidden = true
            cell.buttonType = ButtonTableCellTypes.Plain
            cell.titleText = NSLocalizedString("Merge Duplicates", comment: "")
            cell.iconImage = UIImage(named: IconSet.MergeGloss64)
            return cell
        }
        
        var cell: MergeTableCell! = tableView.dequeueReusableCellWithIdentifier("MergeTableCell") as? MergeTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("MergeTableCell", owner: self, options: nil)[0] as? MergeTableCell
        }
        let dupeRecordId = self.duplicateRecordIds[indexPath.row]
        let targetRecordId = self.duplicateRecordMap[dupeRecordId]
        if targetRecordId != nil {
            var isInvalid = false
            if let targetPerson: SwiftAddressBookPerson = SCABManager.swiftAddressBook?.personFromRecordId(targetRecordId!) {
                if targetPerson.image != nil {
                    cell.leftProfileImage.image = targetPerson.image
                } else {
                    //do something
                    cell.leftProfileImage.image = kDefaultAvatarImage
                }
                // cell.leftProfileImage.layer.cornerRadius = 5
                // cell.leftProfileImage.clipsToBounds = true;
                cell.leftContactName.text = targetPerson.getName()
            } else {
                isInvalid = true
            }
            if let duplicatePerson: SwiftAddressBookPerson = SCABManager.swiftAddressBook?.personFromRecordId(dupeRecordId) {
                if duplicatePerson.image != nil {
                    cell.rightProfileImage.image = duplicatePerson.image
                } else {
                    //do something
                    cell.rightProfileImage.image = kDefaultAvatarImage
                }
                // cell.rightProfileImage.layer.cornerRadius = 5
                // cell.rightProfileImage.clipsToBounds = true;
                cell.rightContactName.text = duplicatePerson.getName()
                cell.rightContactName.textColor = UIColor.lightGrayColor()
            } else {
                isInvalid = true
            }
            
            
            cell.mergeDirectionButton.tag = indexPath.row
            cell.direction = MergeDirections.RightToLeft
            if contains(self.invertedMergeRecordIds, dupeRecordId) {
                cell.direction = MergeDirections.LeftToRight
            }
            cell.cellIndex = indexPath.row
            cell.delegate = self
            
            if contains(self.uncheckedRecordIds, dupeRecordId) {
                //it is currently skipped, checkbox should not be ticked
                cell.checkboxButton.setImage(UIImage(named: IconSet.Checkbox), forState: UIControlState.Normal)
                cell.checkboxButton.tintColor = UIColor.lightGrayColor()
            } else {
                //it is currently NOT skipped, checkbox should be ticked
                cell.checkboxButton.setImage(UIImage(named: IconSet.CheckboxTicked), forState: UIControlState.Normal)
                cell.checkboxButton.tintColor = kDefaultTint
            }
            if isInvalid {
                cell.userInteractionEnabled = false
            } else {
                cell.userInteractionEnabled = true
                cell.mergeDirectionButton.addTarget(self, action: "invertMergeDirection:", forControlEvents: UIControlEvents.TouchUpInside)
                cell.checkboxButton.addTarget(self, action: "checkboxTouched:", forControlEvents: UIControlEvents.TouchUpInside)
                cell.checkboxButton.tag = indexPath.row
            }
            
        }
        return cell!
    }
    
    func invertMergeDirection(sender: UIButton) {
        let tagValue = sender.tag
        let cell = self.defaultTableView.cellForRowAtIndexPath(NSIndexPath(forRow: tagValue, inSection: 0)) as MergeTableCell
        if contains(self.invertedMergeRecordIds, self.duplicateRecordIds[tagValue]) {
            self.invertedMergeRecordIds.removeAtIndex(find(self.invertedMergeRecordIds, self.duplicateRecordIds[tagValue])!)
            cell.direction = MergeDirections.RightToLeft
        } else {
            var mergeRecordMap = Dictionary<ABRecordID, ABRecordID>()
            for recordId in self.duplicateRecordIds {
                if contains(self.uncheckedRecordIds, recordId) {
                    continue
                } else if contains(invertedMergeRecordIds, recordId) {
                    mergeRecordMap[self.duplicateRecordMap[recordId]!] = recordId
                    
                } else {
                    mergeRecordMap[recordId] = self.duplicateRecordMap[recordId]!
                }
            }
            let selectedRecordId = self.duplicateRecordMap[self.duplicateRecordIds[tagValue]]!
            if mergeRecordMap[selectedRecordId] != nil {
                showAlert(NSLocalizedString("Invalid Selection", comment: ""), message: NSLocalizedString("You cannot merge the same contact to multiple targets", comment: ""))
            } else {
                self.invertedMergeRecordIds.append(self.duplicateRecordIds[tagValue])
                cell.direction = MergeDirections.LeftToRight
            }
            
        }
    }
    
    func checkboxTouched(sender: UIButton) {
        toggleMerge(sender.tag)
    }
    
    
    func toggleMerge(tagValue: Int) {
        let cell = self.defaultTableView.cellForRowAtIndexPath(NSIndexPath(forRow: tagValue, inSection: 0)) as MergeTableCell
        if contains(self.uncheckedRecordIds, self.duplicateRecordIds[tagValue]) {
            //tick this cell so that it will be processed
            cell.checkboxButton.setImage(UIImage(named: IconSet.CheckboxTicked), forState: UIControlState.Normal)
            cell.checkboxButton.tintColor = kDefaultTint
            self.uncheckedRecordIds.removeAtIndex(find(self.uncheckedRecordIds, self.duplicateRecordIds[tagValue])!)
        } else {
            // need to skip this contact from cleanup
            cell.checkboxButton.setImage(UIImage(named: IconSet.Checkbox), forState: UIControlState.Normal)
            cell.checkboxButton.tintColor = UIColor.lightGrayColor()
            self.uncheckedRecordIds.append(self.duplicateRecordIds[tagValue])
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == defaultTableView {
            toggleMerge(indexPath.row)
        } else {
            if let buttonCell = tableView.cellForRowAtIndexPath(indexPath) as? ButtonTableCell {
                buttonTapped(buttonCell)
            }
        }
    }
    
    func mergeDuplicates(recordsMap: Dictionary<ABRecordID, ABRecordID>, completion: (() -> Void)) {
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.mode = MBProgressHUDModeDeterminateHorizontalBar
        hud.labelText = NSLocalizedString("Merging Duplicates", comment: "")
        SCABManager.sharedInstance.mergeDuplicates(recordsMap, progress: {
            (stats: ContactsProcessingStatus) -> Void in
            var progress = Double(stats.completed) / Double(stats.total)
            hud.progress = Float(progress)
            return
            }, completion: {
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                completion()
        })
    }
    
    override func viewWillDisappear(animated: Bool) {
        if !self.mergeCompleted && !self.isActivity {
            let title = String(format: NSLocalizedString("%d Duplicates Found", comment: ""), self.duplicateRecordMap.count)
            let nsDict = NSMutableDictionary()
            for (key, value) in self.duplicateRecordMap {
                nsDict.setObject(NSNumber(int: value), forKey: NSNumber(int: key))
            }
            Activity.addEntry(title, type: ActivityTypes.DUPLICATE_SCAN, data: nsDict)
        }
    }
}