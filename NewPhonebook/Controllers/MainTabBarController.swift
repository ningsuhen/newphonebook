//
//  MainTabBarController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/23/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation

class MainTabBarController: UITabBarController, WSCoachMarksViewDelegate {
    var searchFocused = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        UITabBar.appearance().tintColor = UIColor(red:0.29, green:0.29, blue:0.29, alpha:1)
        UITabBar.appearance().barTintColor = UIColor(red: 0.969, green:0.969, blue:0.969, alpha: 1) /*#f7f7f7*/
        
        if !Preferences.sharedInstance.walkThroughDone {
            showCoachMarks()
            Preferences.sharedInstance.walkThroughDone = true
        }
        Appirater.appLaunched(true)
        loadFacebookUserInfo()
    }
    func loadFacebookUserInfo(){
        
        if (FBSession.activeSession().state == FBSessionState.Open || FBSession.activeSession().state == FBSessionState.CreatedTokenLoaded) && Preferences.sharedInstance.facebookAccount == kUnlinkedAccount {
            logWarn("MainTabBarController.loadFacebookUserInfo fetching FBUser")
            FBRequest.requestForMe()?.startWithCompletionHandler({
                (connection: FBRequestConnection!, user: AnyObject!, error: NSError!) in
                if error == nil {
                    if let graphObject = user as? FBGraphObject {
                        Preferences.sharedInstance.facebookAccount = "\(user.first_name) \(user.last_name)"
                        if let userInfo = Preferences.sharedInstance.userInfo {
                            Preferences.sharedInstance.userInfo = userInfo.update(graphObject)
                        } else {
                            Preferences.sharedInstance.userInfo = SCUserInfo(graphObject: graphObject)
                        }
                        SCABManager.sharedInstance.facebookLoginCallback()
                    }
                } else {
                    logError("error fetching Facebook user Info in MainTabBarController: \(error)")
                }
            })
        }
    }
    
    func showCoachMarks() {
        let screenSize = self.view.frame.size
        let screenWidth = Double(screenSize.width)
        let screenHeight = Double(screenSize.height)
        
        //0
        var coachMarks = [["rect": NSValue(CGRect: CGRect(x: 0, y: 100, width: 0, height: 0)),
            "caption": NSLocalizedString("Welcome to NewPhonebook. \n Let's take a quick tour of the app.", comment: "")
            ]]
        
        //1
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: 0, y: (screenHeight - 49), width: (screenWidth / 5), height: 49)),
            "caption": NSLocalizedString("Access your Smart Favorites here. You can also edit or add your own favorites. ", comment
                : "")
            ])
        //2 - set index to 1
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: (screenWidth / 5), y: (screenHeight - 49), width: (screenWidth / 5), height: 49)),
            "caption": NSLocalizedString("Smart groups and saved searches for quick access. You can also add custom groups by saving a search.", comment
                : "")
            ])
        //3 - set index to 2
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: ((screenWidth / 5) * 2), y: (screenHeight - 49), width: (screenWidth / 5), height: 49)),
            "caption": NSLocalizedString("Your Full Contact list. Use the shortcut call/email buttons for quick actions.", comment
                : "")
            ])
        //4
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: 0, y: 20, width: screenWidth, height: 88)),
            "caption": NSLocalizedString("You can search your contacts with different scopes and even save them to create smart groups.", comment
                : "")
            ])
        //5 - set index to 3
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: ((screenWidth / 5) * 3), y: (screenHeight - 49), width: (screenWidth / 5), height: 49)),
            "caption": NSLocalizedString("The Keypad. Nothing special but still useful.", comment
                : "")
            ])
        //6 - set index to 4
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: ((screenWidth / 5) * 4), y: (screenHeight - 49), width: (screenWidth / 5), height: 49)),
            "caption": NSLocalizedString("Access the tools - Backup, Restore, Merge, Cleanup, Settings, etc.", comment
                : "")
            ])
        var buttonHeight: Double = 72
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: 0, y: buttonHeight, width: screenWidth, height: 84)),
            "caption": NSLocalizedString("Merge Duplicates. Easily merge duplicates without loosing any useful information. ", comment
                : "")
            ])
        buttonHeight += 89
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: 0, y: buttonHeight, width: screenWidth, height: 84)),
            "caption": NSLocalizedString("Clean Dummies. Remove empty or useless contacts with couple of clicks.", comment
                : "")
            ])
        
        buttonHeight += 89
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: 0, y: buttonHeight, width: screenWidth, height: 84)),
            "caption": NSLocalizedString("Backup your Contacts with ease. Options to save in iCloud, Google Drive, Dropbox, etc.", comment
                : "")
            ])
        
        buttonHeight += 89
        coachMarks.append([
            "rect": NSValue(CGRect: CGRect(x: 0, y: buttonHeight, width: screenWidth, height: 84)),
            "caption": NSLocalizedString("Easily restore contacts from the backups you uploaded in iCloud, Google Drive, Dropbox, etc.", comment
                : "")
            ])
        
        buttonHeight += 89
        if (buttonHeight + 84) < screenHeight {
            coachMarks.append([
                "rect": NSValue(CGRect: CGRect(x: 0, y: buttonHeight, width: screenWidth, height: 84)),
                "caption": NSLocalizedString("Settings - Customize the settings to suit your needs of searching and merging contacts.", comment
                    : "")
                ])
            
        }
        let coachMarksView = WSCoachMarksView(frame: self.view.bounds, coachMarks: coachMarks)
        coachMarksView.delegate = self
        self.view.addSubview(coachMarksView)
        coachMarksView.start()
        
    }
    
    func coachMarksView(coachMarksView: WSCoachMarksView, willNavigateToIndex index: Int) {
        switch index {
        case 2:
            self.selectedIndex = 1
            break
        case 3:
            self.selectedIndex = 2
            break
        case 4:
            //Search coach Mark, move skip button below
            coachMarksView.skipButtonTop = 110.0
            //focus on search
            if let navigationController = self.selectedViewController as? UINavigationController {
                if let contactsViewController = navigationController.topViewController as? ContactsViewController {
                    contactsViewController.focusOnSearch()
                    searchFocused = true
                }
            }
            break
        case 5:
            coachMarksView.skipButtonTop = 26.0
            self.selectedIndex = 3
            break
        case 6:
            self.selectedIndex = 4
            break
        default:
            break
        }
    }
    
    func coachMarksViewWillCleanup(coachMarksView: WSCoachMarksView!) {
        
        //remove focus on search
        if searchFocused{
            if let navigationController = self.viewControllers?[2] as? UINavigationController {
                if let contactsViewController = navigationController.topViewController as? ContactsViewController {
                    contactsViewController.removeFocusOnSearch()
                }
            }
        }
    }
}
