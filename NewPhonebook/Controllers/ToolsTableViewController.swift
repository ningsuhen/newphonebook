import Foundation


class ToolsTableViewController: BaseUITableViewController {
    
    
    @IBAction func feedbackButtonTapped(sender: UIBarButtonItem) {
        if IJReachability.isConnectedToNetwork(){
            let webViewController: SVModalWebViewController = SVModalWebViewController(address: "https://feedback.userreport.com/4989b0a5-f99c-4b2f-ad1e-a974b1216f4c/")
            self.presentViewController(webViewController, animated: true, completion: nil)
        }
        else{
            showAlert(NSLocalizedString("No Internet Connection", comment: ""), message: NSLocalizedString("Try again once you have an internet connection",comment:""))
        }
    }
    
    
    @IBAction func helpButtonTapped(sender: UIBarButtonItem) {
        if IJReachability.isConnectedToNetwork(){
            let webViewController: SVModalWebViewController = SVModalWebViewController(address: "http://www.newphonebook.io/help.html")
            self.presentViewController(webViewController, animated: true, completion: nil)
        }
        else{
            showAlert(NSLocalizedString("No Internet Connection", comment: ""), message: NSLocalizedString("Try again once you have an internet connection",comment:""))
        }
        
    }
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 10
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 5 {
            return 0
        } else {
            return 8
        }
    }
}