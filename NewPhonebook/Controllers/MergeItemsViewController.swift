//
//  MergeItemsViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 2/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation

class MergedItemsViewController: BaseUIViewController, UITableViewDelegate, UITableViewDataSource {
    var recentItems = Array<MergeItem>()
    var parentActivity: Activity!
    var documentsPath: String?
    let defaultPicture = kDefaultAvatarImage
    @IBOutlet var activityTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityTableView.registerNib(UINib(nibName: "MergeTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "MergeTableCell")
        self.recentItems = self.parentActivity.mergeItems.array as [MergeItem]
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recentItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: MergeTableCell! = tableView.dequeueReusableCellWithIdentifier("MergeTableCell") as? MergeTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("MergeTableCell", owner: self, options: nil)[0] as? MergeTableCell
        }
        cell.userInteractionEnabled = false
        let recentItem = self.recentItems[indexPath.row]
        cell.leftContactName.text = recentItem.tcName
        cell.leftProfileImage.image = defaultPicture
        
        
        cell.rightContactName.text = recentItem.dcName
        cell.rightProfileImage.image = defaultPicture
        cell.showCheckbox = false
        cell.direction = MergeDirections.RightToLeft
        
        //Update Left Profile Image
        
        
        NSOperationQueue().addOperationWithBlock({
            var tcImagePath = recentItem.tcImagePath
            if tcImagePath != IconSet.DefaultAvatar {
                tcImagePath = SCABManager.sharedInstance.libraryPath.stringByAppendingPathComponent(tcImagePath)
                if !NSFileManager.defaultManager().fileExistsAtPath(tcImagePath) {
                    tcImagePath = IconSet.DefaultAvatar
                }
                let tcPicture = UIImage(named: tcImagePath)
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    if let updateCell = tableView.cellForRowAtIndexPath(indexPath) as? MergeTableCell {
                        updateCell.leftProfileImage.image = tcPicture
                    }
                })
            }
        })
        
        NSOperationQueue().addOperationWithBlock({
            var dcImagePath = recentItem.dcImagePath
            if dcImagePath != IconSet.DefaultAvatar {
                dcImagePath = SCABManager.sharedInstance.libraryPath.stringByAppendingPathComponent(dcImagePath)
                if !NSFileManager.defaultManager().fileExistsAtPath(dcImagePath) {
                    dcImagePath = IconSet.DefaultAvatar
                }
                let dcPicture = UIImage(named: dcImagePath)
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    if let updateCell = tableView.cellForRowAtIndexPath(indexPath) as? MergeTableCell {
                        updateCell.rightProfileImage.image = dcPicture
                    }
                })
            }
        })
        
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return NSLocalizedString("Recent Activity", comment: "")
    }
    
}