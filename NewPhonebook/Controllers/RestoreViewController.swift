//
//  BackupViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/24/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation

import UIKit
import AddressBook
import MessageUI

class RestoreViewController: BaseUITableViewController {
    
    var _query: NSMetadataQuery?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 10
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 5 {
            return 0
        } else {
            return 8
        }
    }
    
    
    func getRestoreUrl(completion: ((restoreUrl:String) -> Void)) {
        let title = NSLocalizedString("Enter VCF Url", comment: "")
        let message = NSLocalizedString("Enter the url of the VCF file. It should be accessible without any authentication", comment: "")
        let cancelButtonTitle = NSLocalizedString("Cancel", comment: "")
        let otherButtonTitle = NSLocalizedString("OK", comment: "")
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        // Add the text field for text entry.
        alertController.addTextFieldWithConfigurationHandler {
            textField in
            textField.text = NSLocalizedString("http://", comment: "")
            // If you need to customize the text field, you can do so here.
        }
        
        // Create the actions.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
            logDebug("The \"Text Entry\" alert's cancel action occured.")
        }
        
        let otherAction = UIAlertAction(title: otherButtonTitle, style: .Default) {
            action in
            logDebug("The \"Text Entry\" alert's other action occured.")
            let textFields = alertController.textFields as [UITextField]
            completion(restoreUrl: textFields[0].text)
        }
        
        // Add the actions.
        alertController.addAction(cancelAction)
        alertController.addAction(otherAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    func  validateUrl (candidate:String) -> Bool{
        let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        return candidate.match(urlRegEx)
    }
    func openChoicesModal(choices:[RestoreChoice]){
        let choiceViewController = self.storyboard!.instantiateViewControllerWithIdentifier("RestoreSourceViewController") as RestoreSourceViewController
        choiceViewController.choices = choices
        choiceViewController.delegate = self
        self.hideWaitIndicator()
        self.presentViewController(choiceViewController, animated: true,completion:nil)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.selected = false
        if indexPath.section == RestoreTypes.PHONE {
            self.showWaitIndicator(NSLocalizedString("Reading Phone", comment: ""))
            SCABManager.sharedInstance.getVCardFilesInPhone() {
                (files: Array<Dictionary<String, AnyObject>>) in
                var choices = [RestoreChoice]()
                for file in files {
                    let creationDate = file["creationDate"] as? NSDate
                    let creationDateAgo = creationDate!.timeAgoSinceNow()
                    let fileSize = file["size"] as? NSNumber
                    let fileSizeHuman = NSByteCountFormatter.stringFromByteCount(Int64(fileSize!.integerValue), countStyle: NSByteCountFormatterCountStyle.File)
                    let description = String(format: NSLocalizedString("Size: %@ Created %@", comment: ""), fileSizeHuman, creationDateAgo)
                    let choice = RestoreChoice(title: file["title"] as String, description: description, type: RestoreTypes.PHONE, data: file)
                    choices.append(choice)
                }
                self.openChoicesModal(choices)
            }
        } else if indexPath.section == RestoreTypes.URL {
            if IJReachability.isConnectedToNetwork(){
                self.getRestoreUrl() {
                    (inputString: String) in
                    if !self.validateUrl(inputString){
                        let title = NSLocalizedString("You must enter a valid Url", comment:"")
                        let message = NSLocalizedString("Empty or invalid urls are not allowed. Please try again.", comment:"")
                        self.showAlert(title, message: message)
                        return
                    }
                    if let restoreUrl = NSURL(string: inputString){
                        var hud = self.showWaitIndicatorWithProgress(NSLocalizedString("Downloading VCard data", comment: ""))
                        SCABManager.sharedInstance.fetchVCardFromUrl(restoreUrl, progress: {
                            (progress: Float) in
                            hud.progress = progress
                            }, completion: {
                                (data: NSData?) in
                                self.handleRestoreSelected(data)
                        })
                    }
                    else{
                        let title = NSLocalizedString("You must enter a valid Url", comment:"")
                        let message = NSLocalizedString("Empty or invalid urls are not allowed. Please try again.", comment:"")
                        self.showAlert(title, message: message)
                    }
                }
            }
            else{
                showNoConnectionAlert()
            }
        } else if indexPath.section == RestoreTypes.ICLOUD {
            
            if SCABManager.sharedInstance.getICloudAccess() {
                var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                hud.labelText = NSLocalizedString("Reading iCloud", comment: "")
                let query = NSMetadataQuery()
                self._query = query;
                query.searchScopes = [NSMetadataQueryUbiquitousDocumentsScope]
                let pred = NSPredicate(format: "%K like[c] %@", argumentArray: [NSMetadataItemFSNameKey, "*.vcf"])
                query.predicate = pred
                NSNotificationCenter.defaultCenter().addObserver(self, selector: "queryDidFinishGathering:", name: NSMetadataQueryDidFinishGatheringNotification, object: query)
                query.startQuery()
            } else {
                self.showAlert(NSLocalizedString("iCloud Not Available", comment: ""), message: NSLocalizedString("Unable to access iCloud. If iCloud is not enabled in your account, enable it and try again!", comment: ""))
            }
            
        } else if indexPath.section == RestoreTypes.DROPBOX {
            if IJReachability.isConnectedToNetwork(){
                SCABManager.sharedInstance.getDropboxAccess(controller: nil) {
                    self.showWaitIndicator(NSLocalizedString("Reading Dropbox ..", comment: ""))
                    SCABManager.sharedInstance.fetchDropboxItems(self) {
                        (results: [DBMetadata]?) in
                        if results != nil{
                            var choices = [RestoreChoice]()
                            for metaData in results! {
                                let choice = RestoreChoice(title: metaData.filename, description: "", type: RestoreTypes.DROPBOX, data: metaData)
                                choices.append(choice)
                            }
                            self.openChoicesModal(choices)
                        }
                        else{
                            self.hideWaitIndicator()
                            let title = NSLocalizedString("Dropbox Error!", comment:"")
                            let message = NSLocalizedString("There was an error fetching items from Dropbox. Please try again.", comment:"")
                            self.showAlert(title, message: message)
                        }
                    }
                }
            }
            else{
                showNoConnectionAlert()
            }
        } else if indexPath.section == RestoreTypes.GOOGLEDRIVE {
            if IJReachability.isConnectedToNetwork(){
                SCABManager.sharedInstance.getGDriveAccess(controller: nil) {
                    self.showWaitIndicator(NSLocalizedString("Reading Google Drive ..", comment: ""))
                    SCABManager.sharedInstance.listGDriveFiles() {
                        (results: GTLDriveFileList?) in
                        if results != nil {
                            var choices = [RestoreChoice]()
                            if results!.items() != nil {
                                for metaData in results!.items() {
                                    let file = metaData as GTLDriveFile
                                    let choice = RestoreChoice(title: file.title!, description: "", type: RestoreTypes.GOOGLEDRIVE, data: file)
                                    choices.append(choice)
                                }
                            }
                            self.openChoicesModal(choices)
                        } else {
                            self.hideWaitIndicator()
                            logError("Nil returned for GTLDriveFileList")
                            let title = NSLocalizedString("Google Drive Error", comment:"")
                            let message = NSLocalizedString("Error reading Google Drive. Try re-connecting to Google Drive from Settings tab.", comment:"")
                            self.showAlert(title, message: message)
                        }
                    }
                }
            }
            else{
                showNoConnectionAlert()
            }
        }
    }
    
    
    func queryDidFinishGathering(notification: NSNotification) {
        let query: NSMetadataQuery? = notification.object as? NSMetadataQuery
        query!.disableUpdates()
        query!.stopQuery()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSMetadataQueryDidFinishGatheringNotification, object: query)
        self._query = nil
        var choices = [RestoreChoice]()
        for item in query!.results {
            let title = item.valueForAttribute(NSMetadataItemFSNameKey) as String
            let choice = RestoreChoice(title: title, description: "", type: RestoreTypes.ICLOUD, data: item)
            choices.append(choice)
        }
        openChoicesModal(choices)
    }
    
    
    func alertContactsSaved() {
        let title = NSLocalizedString("Contacts Saved", comment: "")
        let message = NSLocalizedString("Your contacts have been saved to your phone ", comment: "")
        let cancelButtonTitle = NSLocalizedString("OK", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        // Create the action.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
        }
        // Add the action.
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func reset(segue: UIStoryboardSegue) {
        //do stuff
        
    }
    
    func handleRestoreSelected(data:NSData?){
        if data != nil {
            NSOperationQueue().addOperationWithBlock({
                autoreleasepool({
                    if let defaultSource = SCABManager.swiftAddressBook?.defaultSource {
                        if let newContacts = SwiftAddressBookPerson.createInSourceWithVCard(defaultSource, vCard: data!) {
                            NSOperationQueue.mainQueue().addOperationWithBlock({
                                logDebug("\(newContacts.count)")
                                let restoreItemsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("RestoreItemsViewController") as RestoreItemsViewController
                                restoreItemsViewController.choices = newContacts
                                self.hideWaitIndicator()
                                self.presentViewController(restoreItemsViewController, animated: true,completion:nil)
                                
                            })
                        }
                        else{
                            NSOperationQueue.mainQueue().addOperationWithBlock({
                                self.hideWaitIndicator()
                                self.showAlert(NSLocalizedString("VCard Error!", comment:""), message: NSLocalizedString("Unable to process VCard. The VCard File is probably corrupted",comment:""))
                            })
                        }
                    }
                    else{
                        NSOperationQueue.mainQueue().addOperationWithBlock({
                            self.hideWaitIndicator()
                            self.showAlert(NSLocalizedString("AddressBook Source Error!", comment:""), message: NSLocalizedString("Unable to get Default Address Book Source. Please report a bug",comment:""))
                        })
                    }
                })
            })
        }
        else{
            self.hideWaitIndicator()
            let title = NSLocalizedString("Fetch Error!", comment:"")
            let message = NSLocalizedString("Error while fetching VCard. Try again.", comment:"")
            self.showAlert(title, message: message)
        }
    }
    
    func restoreSelected(choice: RestoreChoice) {
        if choice.type == RestoreTypes.PHONE {
            let fileName = choice.title
            self.showWaitIndicator(NSLocalizedString("Processing VCard data", comment: ""))
            SCABManager.sharedInstance.readVCardFromLocalFile(fileName) {
                (data: NSData?) in
                self.handleRestoreSelected(data)
            }
        } else if choice.type == RestoreTypes.ICLOUD {
            let fileName = choice.title
            self.showWaitIndicator(NSLocalizedString("Downloading VCard", comment: ""))
            SCABManager.sharedInstance.readVCardFromICloudFile(fileName) {
                (data: NSData?) in
                self.handleRestoreSelected(data)
            }
        } else if choice.type == RestoreTypes.DROPBOX {
            let fileName = choice.title
            let hud = self.showWaitIndicatorWithProgress(NSLocalizedString("Downloading VCard", comment: ""))
            SCABManager.sharedInstance.readVCardFromDropbox(fileName, progress: {
                (progress: Float) -> Void in
                hud.progress = progress
                }, completion: {
                    (data: NSData?) in
                    self.handleRestoreSelected(data)
            })
        } else if choice.type == RestoreTypes.GOOGLEDRIVE {
            let fileName = choice.title
            let driveFile = choice.data as GTLDriveFile
            let hud = self.showWaitIndicatorWithProgress(NSLocalizedString("Downloading VCard", comment: ""))
            SCABManager.sharedInstance.readVCardFromGoogleDrive(driveFile, progress: {
                (progress: Float) -> Void in
                hud.progress = progress
                }, completion: {
                    (data: NSData?) in
                    self.handleRestoreSelected(data)
            })
        }
    }
}

