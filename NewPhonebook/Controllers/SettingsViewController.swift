//
//  SettingsViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/25/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import UIKit


class SettingsViewController: IASKAppSettingsViewController, IASKSettingsDelegate, SettingsTableCellDelegate {
    
    @IBOutlet var defaultTableView: UITableView?
    
    @IBOutlet var dropboxLinkStatus: UILabel!
    @IBOutlet var googleDriveLinkStatus: UILabel!
    @IBOutlet var facebookLinkStatus: UILabel!
    @IBOutlet var toggleDropboxButton: UIButton!
    @IBOutlet var toggleGoogleDriveButton: UIButton!
    @IBOutlet var toggleFacebookButton: UIButton!
    
    lazy var abManager = SCABManager.sharedInstance
    
    func buttonTappedForCell(cell: SettingsTableCell) {
        if cell.identifier == AccountKeys.Dropbox {
            toggleDropboxSession(cell)
        } else if cell.identifier == AccountKeys.GoogleDrive {
            toggleGDriveSession(cell)
        } else if cell.identifier == AccountKeys.Facebook {
            toggleFacebookSession(cell)
        }
    }
    
    @IBAction func toggleDropboxSession(cell: SettingsTableCell) {
        if abManager.hasDropboxAccess() {
            SCABManager.sharedInstance.unlinkDropbox()
            self.reloadTable()
        } else {
            if IJReachability.isConnectedToNetwork(){
                SCABManager.sharedInstance.getDropboxAccess(controller: self) {
                    SCABManager.sharedInstance.getDropboxUser() {
                        (userName: String) in
                        self.reloadTable()
                    }
                }
            }
            else{
                showNoConnectionAlert()
            }
        }
    }
    
    func reloadTable() {
        logDebug("dropbox \(Preferences.sharedInstance.dropboxAccount)")
        logDebug("google \(Preferences.sharedInstance.googleDriveAccount)")
        logDebug("facebook \(Preferences.sharedInstance.facebookAccount)")
        self.tableView?.reloadData()
    }
    
    @IBAction func toggleGDriveSession(cell: SettingsTableCell) {
        if abManager.isGDriveAuthorized() {
            SCABManager.sharedInstance.unlinkGoogleDrive()
            self.reloadTable()
        } else {
            if IJReachability.isConnectedToNetwork(){
                SCABManager.sharedInstance.getGDriveAccess(controller: nil) {
                    SCABManager.sharedInstance.getGDriveUser()
                    self.reloadTable()
                }
            }
            else{
                showNoConnectionAlert()
            }
        }
    }
    
    @IBAction func toggleFacebookSession(cell: SettingsTableCell) {
        if IJReachability.isConnectedToNetwork(){
            if (FBSession.activeSession().state == FBSessionState.Open || FBSession.activeSession().state == FBSessionState.CreatedTokenLoaded) {
                self.showConfirmFacebookLogoutAlert() {
                    self.abManager.logoutFacebook()
                }
            }
        }
        else{
            showNoConnectionAlert()
        }
        
    }
    
    func settingsViewControllerDidEnd(sender: IASKAppSettingsViewController) {
        
    }
    
    func settingsViewController(sender: IASKAppSettingsViewController, buttonTappedForSpecifier specifier: IASKSpecifier) {
        logDebug("tapped")
        if specifier.key() == "shareWithFriends" {
            shareApp()
        } else if specifier.key() == "rateApp" {
            //            UIApplication.sharedApplication().openURL(NSURL(string : "http://itunes.com/apps/myappnameinlowercase")!)
            if IJReachability.isConnectedToNetwork(){
                Appirater.rateApp()
            }
            else{
                showNoConnectionAlert()
            }
        } else if specifier.key() == "clearData" {
            showClearDataAlert({
                let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                //        hud.labe
                hud.labelText = "Cleaning App Data ..."
                NSOperationQueue().addOperationWithBlock({
                    SCABManager.sharedInstance.cleanAppData()
                    NSOperationQueue.mainQueue().addOperationWithBlock({
                        MBProgressHUD.hideHUDForView(self.view, animated: true)
                        return
                    })
                })
                
            })
        }
    }
    
    func shareApp() {
        let textToShare = "Hey, Check this out!! NewPhonebook is a new App for iphone! Download it now at http://www.codingexplorer.com/"
        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        let shareCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 3, inSection: 1))
        
        activityVC.popoverPresentationController?.sourceView = shareCell?.imageView
        //New Excluded Activities Code
        activityVC.excludedActivityTypes = [UIActivityTypeAddToReadingList, UIActivityTypeCopyToPasteboard]
        
        self.presentViewController(activityVC, animated: true, completion: nil)
        
    }
    
    func tableView(tableView: UITableView, heightForSpecifier specifier: IASKSpecifier) -> CGFloat {
        if contains([AccountKeys.Dropbox, AccountKeys.Facebook, AccountKeys.GoogleDrive], specifier.key()) {
            return 44
        }
        return 44
    }
    
    func tableView(tableView: UITableView, cellForSpecifier specifier: IASKSpecifier) -> UITableViewCell {
        var cell: SettingsTableCell? = self.defaultTableView!.dequeueReusableCellWithIdentifier("SettingsTableCell") as? SettingsTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("SettingsTableCell", owner: tableView, options: nil)[0] as? SettingsTableCell
        }
        if let imagePath = specifier.valueForKey("Icon") as? String {
            cell?.icon.tintImage = UIImage(named: imagePath)
        }
        cell?.title.text = specifier.title()
        
        
        var value = kUnlinkedAccount
        if specifier.key() == AccountKeys.Dropbox {
            value = Preferences.sharedInstance.dropboxAccount
        } else if specifier.key() == AccountKeys.Facebook {
            value = Preferences.sharedInstance.facebookAccount
            println("\(value)")
            
        } else if specifier.key() == AccountKeys.GoogleDrive {
            value = Preferences.sharedInstance.googleDriveAccount
        }
        cell?.subTitle.text = value
        if value == kUnlinkedAccount {
            cell?.button.setTitle(NSLocalizedString("Connect", comment: ""), forState: UIControlState.Normal)
            cell?.button.setTitleColor(kDefaultTint, forState: UIControlState.Normal)
        } else {
            cell?.button.setTitle(NSLocalizedString("Disconnect", comment: ""), forState: UIControlState.Normal)
            cell?.button.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        }
        cell?.identifier = specifier.key()
        cell?.delegate = self
        return cell!
    }
    
    func showConfirmFacebookLogoutAlert(completion: (() -> Void)) {
        let title = NSLocalizedString("Confirm Facebook Logout", comment: "")
        let message = NSLocalizedString("This App requires you to be logged in using facebook. Are you sure you want to logout?", comment: "")
        let cancelButtonTitle = NSLocalizedString("Cancel", comment: "")
        let otherButtonTitle = NSLocalizedString("Logout", comment: "")
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        // Create the actions.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
            logDebug("The \"Okay/Cancel\" alert's cancel action occured.")
        }
        
        let otherAction = UIAlertAction(title: otherButtonTitle, style: .Default) {
            action in
            completion()
        }
        
        // Add the actions.
        
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showClearDataAlert(completion: (() -> Void)) {
        let title = NSLocalizedString("Confirm Data Cleanup", comment: "")
        let message = NSLocalizedString("This will delete all cached app data and logs. Are you sure you want to continue?", comment: "")
        let cancelButtonTitle = NSLocalizedString("NO", comment: "")
        let otherButtonTitle = NSLocalizedString("YES", comment: "")
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        // Create the actions.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
            logDebug("The \"Okay/Cancel\" alert's cancel action occured.")
        }
        
        let otherAction = UIAlertAction(title: otherButtonTitle, style: .Default) {
            action in
            completion()
        }
        
        // Add the actions.
        
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func clearData(sender: AnyObject) {
        self.showClearDataAlert() {
            
            SCABManager.sharedInstance.cleanAppData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.defaultTableView?.registerNib(UINib(nibName: "SettingsTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "SettingsTableCell")
        if abManager.hasDropboxAccess() {
            abManager.getDropboxUser() {
                (userName: String) in
                self.reloadTable()
            }
        } else if Preferences.sharedInstance.dropboxAccount != kUnlinkedAccount {
            Preferences.sharedInstance.dropboxAccount = kUnlinkedAccount
            self.reloadTable()
        }
        if abManager.isGDriveAuthorized() {
            SCABManager.sharedInstance.getGDriveUser()
            self.reloadTable()
        } else if Preferences.sharedInstance.googleDriveAccount != kUnlinkedAccount {
            Preferences.sharedInstance.googleDriveAccount = kUnlinkedAccount
            self.reloadTable()
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if (FBSession.activeSession().state == FBSessionState.Open || FBSession.activeSession().state == FBSessionState.CreatedTokenLoaded) && Preferences.sharedInstance.facebookAccount == kUnlinkedAccount {
            FBRequest.requestForMe()?.startWithCompletionHandler({
                (connection: FBRequestConnection!, user: AnyObject!, error: NSError!) in
                if error == nil {
                    if let graphObject = user as? FBGraphObject {
                        Preferences.sharedInstance.facebookAccount = "\(user.first_name) \(user.last_name)"
                        if let userInfo = Preferences.sharedInstance.userInfo {
                            Preferences.sharedInstance.userInfo = userInfo.update(graphObject)
                        } else {
                            Preferences.sharedInstance.userInfo = SCUserInfo(graphObject: graphObject)
                        }
                        self.reloadTable()
                    }
                } else {
                    logError("Error fetching Facebook User Info at SettingsViewController: \(error)")
                }
            })
        }
    }
    
        
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if file == "Root" && section == 0 {
            return 35
        }
        return super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        logDebug("\(file)")
        if file == "Root" && section == 0 {
            return 5
        }
        return super.tableView(tableView, heightForFooterInSection: section)
    }
}


class GenericSettingsViewController: IASKAppSettingsViewController, IASKSettingsDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        navigationItem.rightBarButtonItem = nil
        showDoneButton = false
    }
    
    func settingsViewControllerDidEnd(sender: IASKAppSettingsViewController) {
        
    }
    
    init(file: String, specifier: IASKSpecifier) {
        super.init(style: UITableViewStyle.Grouped)
        
        self.file = file
        //                        self.customTitle = [specifier localizedObjectForKey:kIASKChildTitle];
        self.title = specifier.title();
        
        //        return self;
        
        
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}