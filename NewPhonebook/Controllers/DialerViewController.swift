import Foundation
import UIKit
import AddressBook
import AddressBookUI

class DialerViewController: BaseUIViewController, NumberPadDelegate, ABNewPersonViewControllerDelegate {
    @IBOutlet var numberPad: NumberPad!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberPad.delegate = self
    }
    
    
    func callButtonTapped(numberValue: String) {
        UIApplication.sharedApplication().openURL(telURL(numberValue))
    }
    
    func addContactButtonTapped(numberValue: String) {
        if let currentNumber = numberPad.currentNumber.text {
            let addNewPersonController = ABNewPersonViewController()
            addNewPersonController.newPersonViewDelegate = self
            let newPerson = SwiftAddressBookPerson.create()
            
            let phoneNumberMultiValue = MultivalueEntry<String>(value: currentNumber, label: kABHomeLabel, id: -1)
            newPerson.phoneNumbers = [phoneNumberMultiValue]
            newPerson.performRecordBlock({
                (internalRecord: ABRecord) in
                addNewPersonController.displayedPerson = internalRecord
            })
            
            let nvc = UINavigationController(rootViewController: addNewPersonController)
            presentViewController(nvc, animated: true,completion:nil)
        }
        
    }
    
    func newPersonViewController(newPersonView: ABNewPersonViewController!, didCompleteWithNewPerson person: ABRecord!) {
        newPersonView.dismissViewControllerAnimated(true) {
            
        }
        if person != nil {
            logDebug("newPersonViewController delegate called")
            SCABManager.sharedInstance.dummy()
        }
    }
}