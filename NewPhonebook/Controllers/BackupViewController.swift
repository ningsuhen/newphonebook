//
//  BackupViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/24/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation

import UIKit
import AddressBook
import MessageUI

class BackupViewController: BaseUITableViewController, MFMailComposeViewControllerDelegate {
    
    //MARK: Section Sizes
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 10
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 5 {
            return 0
        } else {
            return 8
        }
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.selected = false
        if indexPath.section == BackupOptions.PHONE {
            showWaitIndicator(NSLocalizedString("Creating Backup ...", comment: ""))
            SCABManager.sharedInstance.saveVCard() {
                (vCardPath: String, vCardFileName: String) in
                self.hideWaitIndicator()
                self.showAlert(NSLocalizedString("Contacts Saved!!", comment: ""), message: NSLocalizedString("Contacts backup saved to phone, Go to Tools > Help for instructions", comment: ""))
            }
            
        } else if indexPath.section == BackupOptions.EMAIL {
            showWaitIndicator(NSLocalizedString("Creating Backup ...", comment: ""))
            SCABManager.sharedInstance.getVCard() {
                (vCard: NSData, vCardFileName: String) in
                autoreleasepool {
                    if MFMailComposeViewController.canSendMail() {
                        NSOperationQueue.mainQueue().addOperationWithBlock({
                            self.configuredMailComposeViewController(vCard, mimeType: kVCardMimeType, fileName: vCardFileName)
                            return
                        })
                    } else {
                        self.showCannotSendMailErrorAlert()
                    }
                }
            }
            
        } else if indexPath.section == BackupOptions.ICLOUD {
            //iCloud Backup
            if SCABManager.sharedInstance.getICloudAccess() {
                self.showWaitIndicator(NSLocalizedString("Creating Backup ...", comment: ""))
                SCABManager.sharedInstance.getVCard() {
                    (vCard: NSData, vCardFileName: String) in
                    self.hideWaitIndicator()

                    self.showWaitIndicator(NSLocalizedString("Uploading to iCloud ...", comment: ""))
                    SCABManager.sharedInstance.uploadToiCloud(vCardFileName, data: vCard) {
                        self.hideWaitIndicator()
                        self.showAlert(NSLocalizedString("Added to iCloud!!", comment: ""), message: NSLocalizedString("Note that iCloud Sync happens in background when idle and the backup is not uploaded instantaneously.", comment: ""))
                        return
                    }
                }
            } else {
                self.showAlert(NSLocalizedString("iCloud Not Available", comment: ""), message: NSLocalizedString("Oops! Unable to access iCloud. If iCloud is not enabled in your account, please enable it and try again!", comment: ""))
            }
        } else if indexPath.section == BackupOptions.DROPBOX {
            if IJReachability.isConnectedToNetwork(){
                SCABManager.sharedInstance.getDropboxAccess(controller: nil) {
                    self.showWaitIndicator(NSLocalizedString("Creating Backup ...", comment: ""))
                    SCABManager.sharedInstance.saveVCard() {
                        (vCardPath: String, vCardFileName: String) in
                        self.hideWaitIndicator()
                        var hud = self.showWaitIndicatorWithProgress(NSLocalizedString("Uploading to Dropbox", comment: ""))
                        SCABManager.sharedInstance.uploadToDropBox(vCardFileName, path: vCardPath, controller: self, progress: {
                            (progress: Float) in
                            hud.progress = progress
                            }, completion: {
                                (success:Bool) in
                                if success{
                                    self.hideWaitIndicator()
                                    self.showAlert(NSLocalizedString("Contacts Uploaded!!", comment: ""), message: NSLocalizedString("Contacts backup uploaded to Dropbox, Go to Tools > Help for instructions", comment: ""))
                                    
                                }
                                else{
                                    self.hideWaitIndicator()
                                    let title = NSLocalizedString("Dropbox Error!", comment:"")
                                    let message = NSLocalizedString("There was an error while uploading to Dropbox. Please try again.", comment:"")
                                    self.showAlert(title, message: message)
                                }
                                SCABManager.sharedInstance.removeVCard(vCardPath)
                        })
                    }
                }
            }
            else{
                showNoConnectionAlert()
            }
        } else if indexPath.section == BackupOptions.GOOGLEDRIVE {
            if IJReachability.isConnectedToNetwork(){
                SCABManager.sharedInstance.getGDriveAccess(controller: nil) {
                    self.showWaitIndicator(NSLocalizedString("Creating Backup", comment: ""))
                    SCABManager.sharedInstance.getVCard() {
                        (vCard: NSData, vCardFileName: String) in
                        self.hideWaitIndicator()
                        var hud = self.showWaitIndicatorWithProgress(NSLocalizedString("Uploading to Google Drive", comment: ""))
                        SCABManager.sharedInstance.uploadFile(vCardFileName, data: vCard, progress: {
                            (progress: Float) in
                            hud.progress = progress
                            }, completion: {
                                (success:Bool) in
                                if success{
                                    self.hideWaitIndicator()
                                    self.showAlert(NSLocalizedString("Contacts Uploaded!!", comment: ""), message: NSLocalizedString("Contacts backup uploaded to Google Drive, Go to Tools > Help for instructions", comment: ""))
                                    return
                                }
                                else{
                                    self.hideWaitIndicator()
                                    let title = NSLocalizedString("Upload Error!", comment:"")
                                    let message = NSLocalizedString("There was an error while uploading to Google Drive. Please try again.", comment:"")
                                    self.showAlert(title, message: message)
                                    
                                }
                        })
                    }
                }
            }
            else{
                showNoConnectionAlert()
            }
            
        }
    }
    func showCannotSendMailErrorAlert(){
        let title = NSLocalizedString("Cannot Send Email",comment:"")
        let message = NSLocalizedString("Your Device is not configured properly for sending emails. Please fix and try again.", comment:"")
        showAlert(title, message: message)
    }
    
    
    
    func configuredMailComposeViewController(attachment: NSData, mimeType: String, fileName: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        if let email = Preferences.sharedInstance.userInfo?.email{
            mailComposerVC.setToRecipients([email])
        }
        else{
            logWarn("Email not available in Preferences, launching mail composer with no sender's email")
        }
        mailComposerVC.setSubject(NSLocalizedString("Sending VCard to yourself", comment: ""))
        mailComposerVC.setMessageBody(NSLocalizedString("Attached is the VCard exported from your PhoneBook ", comment: ""), isHTML: false)
        mailComposerVC.addAttachmentData(attachment, mimeType: mimeType, fileName: fileName)
        self.hideWaitIndicator()
        self.presentViewController(mailComposerVC, animated: true, completion: nil)
        return mailComposerVC
    }
    
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        if error == nil{
            controller.dismissViewControllerAnimated(true, completion: nil)
            if result.value == MFMailComposeResultSent.value{
                showAlert(NSLocalizedString("Backup Mail Queued.", comment: ""), message: NSLocalizedString("Email has been added to your outbox. It may take a while to be sent completely.", comment: ""))
            }
            else if result.value == MFMailComposeResultFailed.value{
                showAlert(NSLocalizedString("Backup Failed", comment: ""), message: NSLocalizedString("An error occured while trying to send the email. Please try again!", comment: ""))
            }
        }
        else{
            showAlert(NSLocalizedString("Backup Failed", comment: ""), message: NSLocalizedString("An error occured while trying to send the email. Please try again!", comment: ""))
        }
        
    }
}