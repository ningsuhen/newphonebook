//
//  ConfirmDummyContactsController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 2/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBookUI

protocol ConfirmDummyContactsDelegate {
    func showMergeCompletedMessage(records: [ABRecordID])
}

//MARK: ConfirmDummyContactsController - before cleaning items

class ConfirmDummyContactsController: BaseUIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var buttonTableView: UITableView!
    
    @IBOutlet var navigationBar: UINavigationBar!
    var delegate: ConfirmDummyContactsDelegate?
    
    @IBOutlet var defaultTableView: UITableView!
    var dummyRecordIds = [ABRecordID]()
    var uncheckedRecordIds = Array<ABRecordID>()
    var cleanCompleted = false
    var isActivity = false
    
    func setRecords(records: [ABRecordID]) {
        self.dummyRecordIds = records
    }
    
    func buttonTapped(sender: ButtonTableCell) {
        
        var recordsToClean = [ABRecordID]()
        for recordId in dummyRecordIds {
            if SCABManager.sharedInstance.personExistsWithRecordID(recordId) {
                if contains(uncheckedRecordIds, recordId) {
                    continue
                } else {
                    recordsToClean.append(recordId)
                }
            }
        }
        if recordsToClean.count == 0 {
            showAlert(NSLocalizedString("No active dummy found", comment: ""), message: NSLocalizedString("There are no valid dummies to remove", comment: ""))
        } else {
            self.cleanDummies(recordsToClean) {
                self.cleanCompleted = true
                
                self.navigationController!.popViewControllerAnimated(true)
                self.delegate?.showMergeCompletedMessage(recordsToClean)
            }
        }
        sender.selected = false
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = String(format: NSLocalizedString("%d Empty Contacts Found", comment: ""), self.dummyRecordIds.count)
        self.defaultTableView.registerNib(UINib(nibName: "MergeTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "MergeTableCell")
        self.buttonTableView.registerNib(UINib(nibName: "ButtonTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ButtonTableCell")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == buttonTableView {
            return 1
        }
        return self.dummyRecordIds.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == buttonTableView {
            return 10
        }
        return 0
    }
    
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if tableView == buttonTableView {
            return NSLocalizedString("Tap to arrows to change direction of Merge.", comment: "")
        }
        return nil
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == buttonTableView {
            return 80
        }
        return 44
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView == buttonTableView {
            var cell: ButtonTableCell! = tableView.dequeueReusableCellWithIdentifier("ButtonTableCell") as? ButtonTableCell
            if cell == nil {
                cell = NSBundle.mainBundle().loadNibNamed("ButtonTableCell", owner: self, options: nil)[0] as? ButtonTableCell
            }
            //            scanButtonImage = cell.icon
            // cell.title.text = "Remove Dummies"
            //            scanButtonDesc = cell.subtitle
            // cell.progressView.hidden = true
            // cell.progressCount.hidden = true
            cell.buttonType = ButtonTableCellTypes.Plain
            cell.titleText = NSLocalizedString("Remove Dummies", comment: "")
            cell.iconImage = UIImage(named: IconSet.RemoveGloss64)
            return cell
        }
        var cell: DummyTableCell! = tableView.dequeueReusableCellWithIdentifier("DummyTableCell") as? DummyTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("DummyTableCell", owner: self, options: nil)[0] as? DummyTableCell
        }
        var isInvalid = false
        let dummyRecordID = self.dummyRecordIds[indexPath.row]
        if let contact = SCABManager.swiftAddressBook?.personFromRecordId(dummyRecordID) {
            cell.leftContactName.text = contact.getFullName()
            if contact.image != nil {
                cell.leftProfileImage.image = contact.image
            } else {
                cell.leftProfileImage.image = UIImage(named: IconSet.DefaultAvatar)
            }
            
        } else {
            isInvalid = true
        }
        if contains(self.uncheckedRecordIds, dummyRecordID) {
            //it is currently skipped, checkbox should not be ticked
            cell.checkboxSelected = false
        } else {
            //it is currently NOT skipped, checkbox should be ticked
            cell.checkboxSelected = true
        }
        
        if isInvalid {
            cell.userInteractionEnabled = false
        } else {
            cell.userInteractionEnabled = true
            cell.checkboxButton.addTarget(self, action: "checkboxTouched:", forControlEvents: UIControlEvents.TouchUpInside)
            cell.checkboxButton.tag = indexPath.row
        }
        return cell
    }
    
    
    func checkboxTouched(sender: UIButton) {
        toggleMerge(sender.tag)
    }
    
    
    func toggleMerge(tagValue: Int) {
        let cell = self.defaultTableView.cellForRowAtIndexPath(NSIndexPath(forRow: tagValue, inSection: 0)) as DummyTableCell
        if contains(self.uncheckedRecordIds, self.dummyRecordIds[tagValue]) {
            //tick this cell so that it will be processed
            cell.checkboxButton.setImage(UIImage(named: IconSet.CheckboxTicked), forState: UIControlState.Normal)
            cell.checkboxButton.tintColor = kDefaultTint
            self.uncheckedRecordIds.removeAtIndex(find(self.uncheckedRecordIds, self.dummyRecordIds[tagValue])!)
        } else {
            // need to skip this contact from cleanup
            cell.checkboxButton.setImage(UIImage(named: IconSet.Checkbox), forState: UIControlState.Normal)
            cell.checkboxButton.tintColor = UIColor.lightGrayColor()
            self.uncheckedRecordIds.append(self.dummyRecordIds[tagValue])
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == buttonTableView {
            if let buttonCell = tableView.cellForRowAtIndexPath(indexPath) as? ButtonTableCell {
                buttonTapped(buttonCell)
            }
            return
        }
        let cellIndex = indexPath.row
        if cellIndex < 0 || cellIndex > self.dummyRecordIds.count {
            logError("leftCardTapped called with invalid cell index")
            return
        }
        let dummyRecordId = self.dummyRecordIds[cellIndex]
        if let person = SCABManager.swiftAddressBook?.personFromRecordId(dummyRecordId) {
            let personController = ABPersonViewController()
            person.performRecordBlock({
                (internalRecord: ABRecord) in
                personController.displayedPerson = internalRecord
            })
            personController.shouldShowLinkedPeople = true
            self.navigationController!.pushViewController(personController, animated: true)
        }
    }
    
    func cleanDummies(records: [ABRecordID], completion: (() -> Void)) {
        var hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.mode = MBProgressHUDModeDeterminateHorizontalBar
        hud.labelText = NSLocalizedString("Cleaning Dummies", comment: "")
        SCABManager.sharedInstance.cleanDummies(records, progress: {
            (stats: ContactsProcessingStatus) -> Void in
            var progress = Double(stats.completed) / Double(stats.total)
            hud.progress = Float(progress)
            return
            }, completion: {
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                completion()
        })
    }
    
    override func viewWillDisappear(animated: Bool) {
        if !self.cleanCompleted && !self.isActivity {
            let title = NSLocalizedString("\(self.dummyRecordIds.count) Empty Contacts Found", comment: "")
            let nsArray = NSMutableArray()
            for recordId in self.dummyRecordIds {
                nsArray.addObject(NSNumber(int: recordId))
            }
            Activity.addEntry(title, type: ActivityTypes.DUMMIES_SCAN, data: nsArray)
        }
    }
}