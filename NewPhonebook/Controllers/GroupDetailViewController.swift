//
//  GroupDetailViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 2/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBookUI

class GroupDetailViewController: BaseUIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UISearchBarDelegate, UISearchDisplayDelegate {
    var groupType: SCGroupType = SCGroupTypes.SEARCH
    var contactLogs: [ContactLog]?
    var groupContacts = [SwiftAddressBookPerson]()
    var filteredContacts = [SwiftAddressBookPerson]()
    var currentActionSheetContact: SwiftAddressBookPerson?
    
    @IBOutlet var defaultTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None
        self.defaultTableView.registerNib(UINib(nibName: "ContactsTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ContactsTableCell")
        
    }
    
    
    func filterContentForSearchText(searchText: String, scope: String = kSearchDefaultScopeLabel) {
        SCABManager.sharedInstance.filterContacts(self.groupContacts, text: searchText, scope: scope) {
            (results: [SwiftAddressBookPerson]) -> Void in
            self.filteredContacts = results
            self.searchDisplayController!.searchResultsTableView.reloadData()
        }
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        let scopes = self.searchDisplayController!.searchBar.scopeButtonTitles as [String]
        let selectedScope = scopes[self.searchDisplayController!.searchBar.selectedScopeButtonIndex] as String
        self.filterContentForSearchText(searchString, scope: selectedScope)
        return false
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        let scope = self.searchDisplayController!.searchBar.scopeButtonTitles as [String]
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text, scope: scope[searchOption])
        return false
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return self.filteredContacts.count
        }
        return self.groupContacts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if groupType == SCGroupTypes.RECENT || groupType == SCGroupTypes.TOP {
            var cell: FavoriteTableCell! = tableView.dequeueReusableCellWithIdentifier("FavoriteTableCell") as? FavoriteTableCell
            if cell == nil {
                cell = NSBundle.mainBundle().loadNibNamed("FavoriteTableCell", owner: self, options: nil)[0] as? FavoriteTableCell
            }
            var contact: SwiftAddressBookPerson!
            if tableView == self.searchDisplayController!.searchResultsTableView {
                contact = self.filteredContacts[indexPath.row]
            } else {
                contact = self.groupContacts[indexPath.row]
            }
            if contact.image != nil {
                cell.picture.image = contact.image
            } else {
                cell.picture.image = kDefaultAvatarImage
            }
            cell.fullName.text = contact.getFullName()
            if contactLogs != nil {
                let contactLog = contactLogs![indexPath.row]
                cell.status.text = "\(ABAddressBookCopyLocalizedLabel(contactLog.label).takeRetainedValue()) \(contactLog.value)"
                if groupType == SCGroupTypes.TOP {
                    let callCount = contactLog.linkedLogs.count + 1
                    if callCount == 1 {
                        cell.subStatus.text = String(format: NSLocalizedString("%d Call", comment: ""), callCount)
                    } else {
                        cell.subStatus.text = String(format: NSLocalizedString("%d Calls", comment: ""), callCount)
                    }
                } else {
                    cell.subStatus.text = contactLog.startTime.timeAgoSinceNow()
                }
                
            }
            //            cell.callButton.hidden = true
            cell.accessoryType = UITableViewCellAccessoryType.DetailButton
            return cell!
        }
        //Search Tables
        var cell: ContactsTableCell! = tableView.dequeueReusableCellWithIdentifier("ContactsTableCell") as? ContactsTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("ContactsTableCell", owner: self, options: nil)[0] as? ContactsTableCell
        }
        var contact: SwiftAddressBookPerson!
        if tableView == self.searchDisplayController!.searchResultsTableView {
            contact = self.filteredContacts[indexPath.row]
        } else {
            contact = self.groupContacts[indexPath.row]
        }
        if contact.image != nil {
            cell.picture.image = contact.image
        } else {
            cell.picture.image = kDefaultAvatarImage
        }
        var status = ""
        if let jobTitle = contact.jobTitle {
            
            status = jobTitle
        }
        if let organization = contact.organization {
            if status != "" {
                status += ", "
            }
            status += organization
        }
        if status == "" {
            cell.fullNameCentered.hidden = false
            cell.fullNameCentered.text = contact.getFullName()
            cell.fullName.text = ""
            cell.status.text = ""
        } else {
            cell.fullNameCentered.text = ""
            cell.fullName.text = contact.getFullName()
            cell.status.text = status
        }
        
        
        cell.callButton.hidden = false
        
        cell.accessoryType = UITableViewCellAccessoryType.None
        if contact.phoneNumbers != nil {
            cell.callButton.setBackgroundImage(UIImage(named: "call-skyblue.png"), forState: UIControlState.Normal)
        } else {
            cell.callButton.setBackgroundImage(UIImage(named: "call-gray.png"), forState: UIControlState.Normal)
        }
        
        if tableView == self.searchDisplayController!.searchResultsTableView {
            cell.callButton.tag = ButtonTags.Search
        }
        else{
            cell.callButton.tag = ButtonTags.Normal
        }
        cell.callButton.addTarget(self, action: "callButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        return cell!
    }
    
    func callButtonTapped(sender: UIButton) {
        let cell = sender.superview!.superview as UITableViewCell //HACK: Find a better solution
        var contact: SwiftAddressBookPerson!
        if sender.tag == ButtonTags.Search {
            let indexPath = self.searchDisplayController!.searchResultsTableView.indexPathForCell(cell)
            contact = self.filteredContacts[indexPath!.row]
        } else {
            let indexPath = self.defaultTableView.indexPathForCell(cell)
            contact = self.groupContacts[indexPath!.row]
        }
        if let phoneNumbers = contact.phoneNumbers {
            if phoneNumbers.count > 1 {
                var defaultNumber: String?
                var defaultLabel: String?
                let preCallActionSheet = UIActionSheet(title: NSLocalizedString("Choose Default Number. Will be remembered", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("Cancel", comment: ""), destructiveButtonTitle: nil)
                for phoneNumber in phoneNumbers {
                    if phoneNumber.label == kDefaultLabel {
                        defaultNumber = phoneNumber.value
                        defaultLabel = phoneNumber.label
                    } else {
                        preCallActionSheet.addButtonWithTitle(phoneNumber.value)
                    }
                }
                if defaultNumber == nil {
                    preCallActionSheet.cancelButtonIndex = 0
                    self.currentActionSheetContact = contact
                    preCallActionSheet.showInView(self.view)
                } else {
                    ContactLog.addEntry(defaultNumber!, label: defaultLabel!, abRecordID: contact.getRecordID())
                    let numberToCall = defaultNumber!.replace("[\\s]", template: "-")
                    UIApplication.sharedApplication().openURL(telURL(numberToCall))
                }
            } else {
                ContactLog.addEntry(phoneNumbers[0].value, label: phoneNumbers[0].label, abRecordID: contact.getRecordID())
                let numberToCall = phoneNumbers[0].value.replace("[\\s]", template: "-")
                UIApplication.sharedApplication().openURL(telURL(numberToCall))
            }
        } else {
            logDebug("Call button clicked for contact with No number")
        }
    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        if groupType == SCGroupTypes.RECENT || groupType == SCGroupTypes.TOP {
            var contactOptional: SwiftAddressBookPerson?
            
            if tableView == self.searchDisplayController!.searchResultsTableView {
                contactOptional = self.filteredContacts[indexPath.row]
            } else {
                contactOptional = self.groupContacts[indexPath.row]
            }
            let contact = contactOptional!
            let personController = ABPersonViewController()
            contact.performRecordBlock({
                (internalRecord: ABRecord) in
                personController.displayedPerson = internalRecord
            })
            personController.shouldShowLinkedPeople = true
            self.navigationController!.pushViewController(personController, animated: true)
        } else {
            //
            logError("entered a code which shouldn't be executed")
        }
        
    }
    
    func peoplePickerNavigationController(peoplePicker: ABPeoplePickerNavigationController!, didSelectPerson person: ABRecord!) {
        peoplePicker.dismissViewControllerAnimated(true) {
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var contactOptional: SwiftAddressBookPerson?
        
        if tableView == self.searchDisplayController!.searchResultsTableView {
            contactOptional = self.filteredContacts[indexPath.row]
        } else {
            contactOptional = self.groupContacts[indexPath.row]
        }
        let contact = contactOptional!
        if groupType == SCGroupTypes.RECENT || groupType == SCGroupTypes.TOP {
            contactOptional = self.groupContacts[indexPath.row]
            let contactLog = contactLogs![indexPath.row]
            ContactLog.addEntry(contactLog.value, label: contactLog.label, abRecordID: contactOptional!.getRecordID())
            let numberToCall = contactLog.value.replace("[\\s]", template: "-")
            UIApplication.sharedApplication().openURL(telURL(numberToCall))
        } else {
            let personController = ABPersonViewController()
            contact.performRecordBlock({
                (internalRecord: ABRecord) in
                personController.displayedPerson = internalRecord
            })
            personController.shouldShowLinkedPeople = true
            self.navigationController!.pushViewController(personController, animated: true)
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
}