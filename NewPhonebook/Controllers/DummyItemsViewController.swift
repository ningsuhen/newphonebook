//
//  DummyItemsViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 2/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import CoreData


//MARK:DummyItemsViewController - CleanItem Details

class DummyItemsViewController: BaseUIViewController, UITableViewDelegate, UITableViewDataSource {
    var recentItems = Array<CleanItem>()
    var parentActivity: Activity!
    var documentsPath: String?
    var nsFetchPerformed = false
    @IBOutlet var activityTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityTableView.registerNib(UINib(nibName: "MergeItemTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "mergeItemTableCell")
        self.recentItems = self.parentActivity.cleanItems.array as [CleanItem]
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.recentItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: DummyTableCell! = tableView.dequeueReusableCellWithIdentifier("DummyTableCell") as? DummyTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("DummyTableCell", owner: self, options: nil)[0] as? DummyTableCell
        }
        cell.showCheckbox = false
        cell.userInteractionEnabled = false
        
        let recentItem = self.recentItems[indexPath.row]
        
        let dirPath = SCABManager.sharedInstance.libraryPath
        
        cell.leftContactName.text = recentItem.name
        cell.leftProfileImage.image = kDefaultAvatarImage
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var imagePath = recentItem.imagePath
            if imagePath != IconSet.DefaultAvatar {
                imagePath = dirPath.stringByAppendingPathComponent(imagePath)
                if !NSFileManager.defaultManager().fileExistsAtPath(imagePath) {
                    imagePath = IconSet.DefaultAvatar
                }
                let picture = UIImage(named: imagePath)
                dispatch_async(dispatch_get_main_queue()) {
                    if let updateCell = tableView.cellForRowAtIndexPath(indexPath) as? DummyTableCell {
                        updateCell.leftProfileImage.image = picture
                    }
                }
            }
        }
        
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        
        return cell
        
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String {
        return NSLocalizedString("Recent Activity",comment:"")
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController!) {
        self.activityTableView.reloadData()
    }
}