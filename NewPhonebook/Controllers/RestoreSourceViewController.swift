//
//  RestoreSourceViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 2/6/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation

class RestoreSourceViewController: BaseUIViewController, UITableViewDelegate, UITableViewDataSource {
    var choices = [RestoreChoice]()
    var selectedChoiceIndex = 0
    var delegate: RestoreViewController?
    
    @IBAction func restoreSelected(sender: AnyObject) {
        if selectedChoiceIndex < choices.count {
            let selectedChoice = choices[selectedChoiceIndex]
            self.dismissViewControllerAnimated(true) {
                if self.delegate != nil {
                    self.delegate!.restoreSelected(selectedChoice)
                }
            }
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell! = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: nil)
        if self.choices.count == 0{
            cell.textLabel!.text = NSLocalizedString("Backup not available",comment:"")
            cell.detailTextLabel!.text = NSLocalizedString("You must upload/save a backup first", comment:"")
            cell.accessoryType = UITableViewCellAccessoryType.None
            cell.userInteractionEnabled = false
            return cell
        }
        
        let choice = self.choices[indexPath.row]
        cell.textLabel!.text = choice.title
        cell.detailTextLabel!.text = choice.description
        cell.imageView!.image = UIImage(named: IconSet.Contacts)?.imageTintedWithColor(kDefaultTint)
        cell.userInteractionEnabled = true
        if indexPath.row == selectedChoiceIndex {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cellIndex = indexPath.row
        let cell: UITableViewCell! = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: cellIndex, inSection: 0))
        if selectedChoiceIndex == cellIndex {
            
        } else {
            let cell2: UITableViewCell! = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: selectedChoiceIndex, inSection: 0))
            cell2.accessoryType = UITableViewCellAccessoryType.None
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            selectedChoiceIndex = cellIndex
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.choices.count == 0 ? 1 : self.choices.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 50
    }
    
}
