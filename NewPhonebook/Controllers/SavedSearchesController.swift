//
//  SavedSearchesController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 2/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation

class SavedSearchesController: BaseUIViewController, UITableViewDataSource, UITableViewDelegate {
    var savedSearches = Array<SCSearch>()
    @IBAction func editSavedSearches(sender: UIBarButtonItem) {
        if defaultTableView.editing {
            defaultTableView.setEditing(false, animated: true)
            sender.style = UIBarButtonItemStyle.Plain
            sender.title = NSLocalizedString("Edit", comment: "")
        } else {
            defaultTableView.setEditing(true, animated: true)
            sender.style = UIBarButtonItemStyle.Done
            sender.title = NSLocalizedString("Done", comment: "")
        }
        
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // Update data source array here, something like [array removeObjectAtIndex:indexPath.row];
            var savedSearch = savedSearches[indexPath.row]
            self.savedSearches.removeAtIndex(indexPath.row)
            SCSearch.deleteEntry(savedSearch)
            tableView.deleteRowsAtIndexPaths(NSArray(object: indexPath), withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    
    @IBOutlet var defaultTableView: UITableView!
    var delegate: SavedSearchesDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultTableView.registerNib(UINib(nibName: "GroupsTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "GroupsTableCell")
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: GroupsTableCell! = tableView.dequeueReusableCellWithIdentifier("GroupsTableCell") as? GroupsTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("GroupsTableCell", owner: self, options: nil)[0] as? GroupsTableCell
        }
        cell.accessoryType = UITableViewCellAccessoryType.None
        
        let savedSearch = savedSearches[indexPath.row] as SCSearch
        cell.titleLabel.text = savedSearch.title
        cell.descriptionLabel.text = String(format: NSLocalizedString("%@ Search", comment: ""), savedSearch.scope)
        
        var scopeImageName = "bb-" + savedSearch.scope.lowercaseString
        if savedSearch.scope == "Social" && contains(["facebook", "twitter", "linkedin", "plus.google"], savedSearch.text.lowercaseString) {
            scopeImageName = "bb-" + savedSearch.text.lowercaseString
        }
        if let icon = UIImage(named: scopeImageName) {
            cell.icon.tintImage = icon
        } else {
            cell.icon.tintImage = UIImage(named: "bb-search")
        }
        cell.tagLabel.text = savedSearch.lastUsed.timeAgoSinceNow()
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedSearches.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Saved Searches",comment:"")
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.savedSearches.count > 0 {
            return 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.delegate!.didSelectSearch(self.savedSearches[indexPath.row])
        self.dismissViewControllerAnimated(true,completion:nil)
    }
}

protocol SavedSearchesDelegate {
    func didSelectSearch(search: SCSearch)
}