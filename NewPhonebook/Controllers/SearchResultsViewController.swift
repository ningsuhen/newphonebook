//
//  SearchResultsViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/27/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import UIKit

class SearchResultsViewController: BaseUITableViewController, UISearchResultsUpdating {


    // MARK: Types

    struct StoryboardConstants {
        /// The identifier string that corresponds to the SearchResultsViewController's view controller defined in the main storyboard.
        static let identifier = "SearchResultsViewControllerIdentifier"
    }

    // MARK: UISearchResultsUpdating

    func updateSearchResultsForSearchController(searchController: UISearchController) {
        if !searchController.active {
            return
        }
    }
}
