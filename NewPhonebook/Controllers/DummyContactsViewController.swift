//
//  ViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/17/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import UIKit
import AddressBook
import AddressBookUI
import CoreData

//MARK:DummyContactsViewController

class DummyContactsViewController: BaseUIViewController, ConfirmDummyContactsDelegate, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    //MARK: OutLets DummyContactsViewController
    
    
    @IBOutlet var buttonTableView: UITableView!
    
    @IBOutlet var activityTableView: UITableView!
    @IBOutlet var scanButonView: UIView!
    @IBOutlet var scanButtonImage: UIImageView!
    @IBOutlet var scanButtonTitle: UILabel!
    @IBOutlet var scanButtonDesc: UILabel!
    @IBOutlet var scanButtonCounter: UILabel?
    @IBOutlet var scanButtonProgress: UIProgressView!
    
    //MARK: Actions DummyContactsViewController
    
    
    func buttonTapped(sender: ButtonTableCell) {
        if scanButtonState == ScanButtonStates.NORMAL {
            self.startScan()
        } else if scanButtonState == ScanButtonStates.STARTED {
            self.promptForStopScan()
        } else if scanButtonState == ScanButtonStates.STOPPED {
            self.resumeScan()
        } else if scanButtonState == ScanButtonStates.FINISHED {
            self.startScan()
        } else {
            logError("Unknown State of Scan Dummies Button State")
        }
        sender.selected = false
    }
    
    @IBAction func reset(segue: UIStoryboardSegue) {
        //do stuff
        
    }
    
    //MARK: Variables DummyContactsViewController
    var scanButtonState = ScanButtonStates.NORMAL
    var recentActivities = [Activity]()
    var dummyRecordIds = [ABRecordID]()
    
    
    //MARK: Overrides DummyContactsViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SCABManager.sharedInstance.registerContactsChangeHandler(self)
        CDModelManager.sharedInstance.registerCoreDataChangeHandler(self, entityName: "Activity")
        // Do any additional setup after loading the view, typically from a nib.
        self.activityTableView.dataSource = self
        self.activityTableView.delegate = self
        self.buttonTableView.registerNib(UINib(nibName: "ButtonTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ButtonTableCell")
        
    }
    
    override func onCoreDataChange(entityName: String!) {
        if entityName == "Activity"{
            Activity.fetchRecentDummyEntries() {
                (results: [AnyObject]?) -> Void in
                if results != nil {
                    self.recentActivities = results! as [Activity]
                    self.activityTableView.reloadData()
                }
            }
        }
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let contactsCount = SCABManager.sharedInstance.contacts.count
        if let counterText = scanButtonCounter?.text {
            let splitArray = split(counterText) {
                $0 == "/"
            }
            let format = NSLocalizedString("%d/%d", comment: "")
            scanButtonCounter?.text = String(format: NSLocalizedString("%@/%d", comment: ""), splitArray[0], contactsCount)
        } else {
            scanButtonCounter?.text = String(format: NSLocalizedString("%d/%d", comment: ""), 0, contactsCount)
        }
    }
    
    override func onContactsChange(newContacts: [SwiftAddressBookPerson]) {
        let contactsCount = newContacts.count
        if let counterText = scanButtonCounter?.text {
            let splitArray = split(counterText) {
                $0 == "/"
            }
            scanButtonCounter?.text = String(format: NSLocalizedString("%@/%d", comment: ""), splitArray[0], contactsCount)
        } else {
            scanButtonCounter?.text = String(format: NSLocalizedString("%d/%d", comment: ""), 0, contactsCount)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        logWarn("didReceiveMemoryWarning called")
    }
    
    
    func startScan() {
        self.scanButtonState = ScanButtonStates.STARTED
        self.scanButtonImage.image = UIImage(named: "stop-64")
        self.scanButtonTitle.text = NSLocalizedString("Stop Scan", comment: "")
        self.scanButtonDesc.text = NSLocalizedString("Scan Started! Tap to Stop Scan.", comment: "")
        UIApplication.sharedApplication().idleTimerDisabled = true
        SCABManager.sharedInstance.scanDummyContacts({
            (results: [ABRecordID]) in
            UIApplication.sharedApplication().idleTimerDisabled = false
            //Store results
            self.dummyRecordIds = results
            if self.scanButtonState == ScanButtonStates.STARTED {
                self.scanButtonState = ScanButtonStates.FINISHED
                self.scanButtonImage.image = UIImage(named: IconSet.ScanGloss64)
                self.scanButtonTitle.text = NSLocalizedString("Restart Scan", comment: "")
                self.scanButtonDesc.text = NSLocalizedString("Scan Finished! Tap to Restart.", comment: "")
            }
            if results.count > 0 {
                let confirmDummyContactsController = self.storyboard!.instantiateViewControllerWithIdentifier("ConfirmDummyContactsController") as ConfirmDummyContactsController
                confirmDummyContactsController.setRecords(self.dummyRecordIds)
                confirmDummyContactsController.delegate = self
                confirmDummyContactsController.title = String(format: NSLocalizedString("%d Empty Contacts Found", comment: ""), results.count)
                if self.navigationController != nil {
                    self.navigationController!.pushViewController(confirmDummyContactsController, animated: true)
                }
            } else if self.scanButtonState != ScanButtonStates.STOPPED {
                self.alertNoDummyContacts()
            }
            }, progress: {
                (stats: ContactsProcessingStatus) in
                if stats.total != 0 {
                    self.scanButtonProgress.progress = Float(Double(stats.completed) / Double(stats.total))
                    self.scanButtonCounter?.text = String(format: NSLocalizedString("%d/%d", comment: ""), stats.completed, stats.total)
                }
        })
    }
    
    func resumeScan() {
        self.startScan()
    }
    
    func alertNoDummyContacts() {
        let title = NSLocalizedString("No Dummies Found", comment: "")
        let message = NSLocalizedString("Congratulations!! No empty contacts were found on your phone. ", comment: "")
        let cancelButtonTitle = NSLocalizedString("OK", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        // Create the action.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
        }
        // Add the action.
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func promptForStopScan() {
        let title = NSLocalizedString("Cancel Scan", comment: "")
        let message = NSLocalizedString("Are you sure you want to stop this scan?", comment: "")
        let cancelButtonTitle = NSLocalizedString("NO", comment: "")
        let otherButtonTitle = NSLocalizedString("YES", comment: "")
        let alertCotroller = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        // Create the actions.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
            //do nothing
        }
        
        let otherAction = UIAlertAction(title: otherButtonTitle, style: .Default) {
            action in
            self.stopScan()
        }
        
        // Add the actions.
        alertCotroller.addAction(cancelAction)
        alertCotroller.addAction(otherAction)
        presentViewController(alertCotroller, animated: true, completion: nil)
        
    }
    
    func stopScan() {
        SCABManager.sharedInstance.cancelDummyContactsScan()
        self.scanButtonState = ScanButtonStates.STOPPED
        self.scanButtonImage.image = UIImage(named: IconSet.ScanGloss64)
        self.scanButtonTitle.text = NSLocalizedString("Restart Scan", comment: "")
        self.scanButtonDesc.text = NSLocalizedString("Scan Cancelled! Tap to restart.", comment: "")
    }
    
    
    func doMerge() {
        //merge
        
    }
    
    func printMergeInfo(person: SwiftAddressBookPerson, person2: SwiftAddressBookPerson) {
        var personName = ""
        if person.firstName != nil {
            personName = person.firstName!
        } else if person.lastName != nil {
            personName = person.lastName!
        } else {
            personName = NSLocalizedString("Unknown Contact", comment: "")
        }
        var person2Name = ""
        if person2.firstName != nil {
            person2Name = person2.firstName!
        } else if person2.lastName != nil {
            person2Name = person2.lastName!
        } else {
            person2Name = NSLocalizedString("Unknown Person", comment: "")
        }
    }
    
    
    func showMergeCompletedMessage(records: [ABRecordID]) {
        showAlert(NSLocalizedString("Contacts Cleaned",comment: ""), message: String(format:NSLocalizedString("%d contacts removed",  comment: ""),records.count))
    }
    
    //MARK: TableView Delegate Methods DummyContactsViewController
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView == buttonTableView {
            var cell: ButtonTableCell! = tableView.dequeueReusableCellWithIdentifier("ButtonTableCell") as? ButtonTableCell
            if cell == nil {
                cell = NSBundle.mainBundle().loadNibNamed("ButtonTableCell", owner: self, options: nil)[0] as? ButtonTableCell
            }
            scanButtonImage = cell.icon
            scanButtonTitle = cell.title
            scanButtonDesc = cell.subtitle
            scanButtonProgress = cell.progressView
            scanButtonCounter = cell.progressCount
            let contactsCount = SCABManager.sharedInstance.contacts.count
            scanButtonCounter?.text = String(format: NSLocalizedString("%d/%d", comment: ""), 0, contactsCount)
            return cell
        }
        let activity = self.recentActivities[indexPath.row]
        let cell: UITableViewCell! = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "recentActivityTablecell")
        cell.textLabel?.text = activity.title
        cell.detailTextLabel?.text = activity.endTime.timeAgoSinceNow()
        
        if activity.type == ActivityTypes.DUMMIES_SCAN {
            cell.imageView!.image = UIImage(named: IconSet.EmptyGloss)?.imageTintedWithColor(kDefaultTint)
            
        } else if activity.type == ActivityTypes.CLEAN_DUMMIES {
            cell.imageView!.image = UIImage(named: IconSet.RemoveGloss)?.imageTintedWithColor(kDefaultTint)
        }
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == buttonTableView {
            return 1
        }
        return self.recentActivities.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == buttonTableView {
            return 10
        }
        return 0
    }
    
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if tableView == buttonTableView {
            return NSLocalizedString("RECENT", comment:"")
        }
        return nil
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == buttonTableView {
            return 80
        }
        return 44
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == buttonTableView {
            if let buttonCell = tableView.cellForRowAtIndexPath(indexPath) as? ButtonTableCell {
                buttonTapped(buttonCell)
            }
        } else {
            let activity = self.recentActivities[indexPath.row]
            if activity.type == ActivityTypes.DUMMIES_SCAN {
                let dummies = (activity.data as NSArray) as [NSNumber]
                var dummyRecordIds = [ABRecordID]()
                for dummy in dummies {
                    dummyRecordIds.append(ABRecordID(dummy.integerValue))
                }
                self.dummyRecordIds = dummyRecordIds
                let confirmDummyContactsController = storyboard!.instantiateViewControllerWithIdentifier("ConfirmDummyContactsController") as ConfirmDummyContactsController
                confirmDummyContactsController.setRecords(self.dummyRecordIds)
                confirmDummyContactsController.delegate = self
                confirmDummyContactsController.isActivity = true
                self.navigationController!.pushViewController(confirmDummyContactsController, animated: true)
                
            } else if activity.type == ActivityTypes.CLEAN_DUMMIES {
                let dummyItemsViewController = storyboard!.instantiateViewControllerWithIdentifier("DummyItemsViewController") as DummyItemsViewController
                dummyItemsViewController.title = activity.title
                dummyItemsViewController.parentActivity = activity
                self.navigationController!.pushViewController(dummyItemsViewController, animated: true)
            }
        }
        
    }
}


