//
//  RestoreItemsViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 2/6/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation

class RestoreItemsViewController: BaseUIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var choices = [SwiftAddressBookPerson]()
    var uncheckedIndices = [Int]()
    
    @IBAction func confirmRestoration(sender: UIBarButtonItem) {
        let hud = self.showWaitIndicatorWithProgress(NSLocalizedString("Importing Contact", comment: ""))
        NSOperationQueue().addOperationWithBlock({
            autoreleasepool({
                let total = self.choices.count
                for (index, choice) in enumerate(self.choices) {
                    if !contains(self.uncheckedIndices, index) {
                        SCABManager.swiftAddressBook?.addRecord(choice)
                    }
                    var progress = Float(Double(index) / Double(total))
                    if progress > 0.1 {
                        progress -= 0.1
                    } else {
                        progress = 0.0
                    }
                    NSOperationQueue.mainQueue().addOperationWithBlock({
                        hud.progress = progress
                    })
                    
                }
                let error = SCABManager.sharedInstance.saveAddressBook()
                if error != nil {
                    logError("Error importing contacts")
                    logError("\(error)")
                }
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    hud.progress = 1.0
                    self.hideWaitIndicator()
                    self.showAlert(NSLocalizedString("Contacts Imported!", comment:""), message: NSLocalizedString("%d Contacts have been imported without merging duplicates. To remove Duplicates, go to Tools > Merge Duplicates", comment:""))
                    self.dismissViewControllerAnimated(true,completion:nil)
                })
            })
            
        })
        
    }
    
    @IBOutlet var defaultTableView: UITableView!
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: nil)
        let choice = self.choices[indexPath.row]
        cell.textLabel!.text = choice.getFullName()
        cell.detailTextLabel!.text = ""
        if let contactImage = choice.image {
            cell.imageView!.image = contactImage
        } else {
            cell.imageView!.image = kDefaultAvatarImage
        }
        cell.imageView!.layer.cornerRadius = 10
        cell.imageView!.clipsToBounds = true
        cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cellIndex = indexPath.row
        let cell: UITableViewCell! = self.defaultTableView.cellForRowAtIndexPath(NSIndexPath(forRow: cellIndex, inSection: 0))
        if let index = find(uncheckedIndices, cellIndex) {
            //already in unchecked. remove it
            uncheckedIndices.removeAtIndex(index)
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            uncheckedIndices.append(cellIndex)
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.choices.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 50
    }
    
}
