//
//  ViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/17/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import UIKit
import AddressBook
import AddressBookUI
import CoreData

class MergeViewController: BaseUIViewController, ConfirmMergeDelegate, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate {
    
    
    @IBOutlet var buttonTableView: UITableView!
    
    @IBOutlet var activityTableView: UITableView!
    
    var scanButonView: UIView!
    var scanButtonImage: UIImageView!
    var scanButtonTitle: UILabel!
    var scanButtonDesc: UILabel!
    var scanButtonCounter: UILabel?
    var scanButtonProgress: UIProgressView!
    
    var scanButtonState = ScanButtonStates.NORMAL
    var recentActivities = [Activity]()
    
    
    func buttonTapped(sender: ButtonTableCell) {
        logDebug("buttonTapped called")
        if scanButtonState == ScanButtonStates.NORMAL {
            logDebug("scanButtonState= Normal, starting scan")
            self.startScan()
        } else if scanButtonState == ScanButtonStates.STARTED {
            logDebug("scanButtonState= STARTED, stopping scan")
            self.promptForStopScan()
        } else if scanButtonState == ScanButtonStates.STOPPED {
            logDebug("scanButtonState= Stopped, restarting scan")
            self.resumeScan()
        } else if scanButtonState == ScanButtonStates.FINISHED {
            logDebug("scanButtonState= finished, restarting scan")
            self.startScan()
        } else {
            logError("Unknown State in Scan Duplicates Button")
        }
        sender.setSelected(false, animated: false)
    }
    
    
    var duplicateRecords = Dictionary<ABRecordID, ABRecordID>()
    
    
    func startScan() {
        self.scanButtonState = ScanButtonStates.STARTED
        self.scanButtonImage.image = UIImage(named: "stop-64")
        self.scanButtonTitle.text = NSLocalizedString("Stop Scan", comment: "")
        self.scanButtonDesc.text = NSLocalizedString("Scan Started! Tap to Stop Scan.", comment: "")
        UIApplication.sharedApplication().idleTimerDisabled = true
        SCABManager.sharedInstance.scanDuplicates({
            (duplicates: Dictionary<ABRecordID, ABRecordID>) in
            UIApplication.sharedApplication().idleTimerDisabled = false
            self.duplicateRecords = duplicates
            if self.scanButtonState == ScanButtonStates.STARTED {
                self.scanButtonState = ScanButtonStates.FINISHED
                self.scanButtonImage.image = UIImage(named: IconSet.ScanGloss64)
                self.scanButtonTitle.text = NSLocalizedString("Restart Scan", comment: "")
                self.scanButtonDesc.text = NSLocalizedString("Scan Finished! Tap to Restart.", comment: "")
            }
            if duplicates.count > 0 {
                let confirmMergeViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ConfirmMergeViewController") as ConfirmMergeViewController
                confirmMergeViewController.setRecords(self.duplicateRecords)
                confirmMergeViewController.delegate = self
                confirmMergeViewController.title = NSLocalizedString("\(duplicates.count) Duplicates Found", comment: "")
                if self.navigationController != nil {
                    self.navigationController!.pushViewController(confirmMergeViewController, animated: true)
                }
            } else if self.scanButtonState != ScanButtonStates.STOPPED {
                logDebug("\(self.scanButtonState)")
                self.alertNoDuplicates()
            }
            
            
            }, progress: {
                (stats: ContactsProcessingStatus) in
                if stats.total != 0 {
                    self.scanButtonProgress.progress = Float(Double(stats.completed) / Double(stats.total))
                    self.scanButtonCounter?.text = NSLocalizedString("\(stats.completed)/\(stats.total)", comment: "")
                }
        })
    }
    
    func resumeScan() {
        self.startScan()
    }
    
    func alertNoDuplicates() {
        let title = NSLocalizedString("No Duplicates Found", comment: "")
        let message = NSLocalizedString("Congratulations!! No duplicate contacts were found on your phone. ", comment: "")
        let cancelButtonTitle = NSLocalizedString("OK", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        // Create the action.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
        }
        // Add the action.
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func promptForStopScan() {
        let title = NSLocalizedString("Cancel Scan", comment: "")
        let message = NSLocalizedString("Are you sure you want to stop this scan?", comment: "")
        let cancelButtonTitle = NSLocalizedString("NO", comment: "")
        let otherButtonTitle = NSLocalizedString("YES", comment: "")
        let alertCotroller = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        // Create the actions.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
            //do nothing
        }
        
        let otherAction = UIAlertAction(title: otherButtonTitle, style: .Default) {
            action in
            self.stopScan()
        }
        
        // Add the actions.
        alertCotroller.addAction(cancelAction)
        alertCotroller.addAction(otherAction)
        presentViewController(alertCotroller, animated: true, completion: nil)
        
    }
    
    func stopScan() {
        SCABManager.sharedInstance.cancelDuplicateScan()
        self.scanButtonState = ScanButtonStates.STOPPED
        self.scanButtonImage.image = UIImage(named: IconSet.ScanGloss64)
        self.scanButtonTitle.text = NSLocalizedString("Restart Scan", comment: "")
        self.scanButtonDesc.text = NSLocalizedString("Scan Cancelled! Tap to restart.", comment: "")
    }
    
    
    func doMerge() {
        //merge
        
    }
    
    func printMergeInfo(person: SwiftAddressBookPerson, person2: SwiftAddressBookPerson) {
        var personName = ""
        if person.firstName != nil {
            personName = person.firstName!
        } else if person.lastName != nil {
            personName = person.lastName!
        } else {
            personName = NSLocalizedString("Unknown Contact", comment: "")
        }
        var person2Name = ""
        if person2.firstName != nil {
            person2Name = person2.firstName!
        } else if person2.lastName != nil {
            person2Name = person2.lastName!
        } else {
            person2Name = NSLocalizedString("Unknown Contact", comment: "")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SCABManager.sharedInstance.registerContactsChangeHandler(self)
        CDModelManager.sharedInstance.registerCoreDataChangeHandler(self, entityName: "Activity")
        // Do any additional setup after loading the view, typically from a nib.
        self.activityTableView.dataSource = self
        self.activityTableView.delegate = self
        self.buttonTableView.registerNib(UINib(nibName: "ButtonTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ButtonTableCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let contactsCount = SCABManager.sharedInstance.contacts.count
        if let counterText = scanButtonCounter?.text {
            let splitArray = split(counterText) {
                $0 == "/"
            }
            let format = NSLocalizedString("%d/%d", comment: "")
            scanButtonCounter?.text = String(format: NSLocalizedString("%@/%d", comment: ""), splitArray[0], contactsCount)
        } else {
            scanButtonCounter?.text = String(format: NSLocalizedString("%d/%d", comment: ""), 0, contactsCount)
        }
    }
    
    override func onCoreDataChange(entityName: String) {
        if entityName == "Activity"{
            
            Activity.fetchRecentMergeEntries() {
                (results: [AnyObject]?) -> Void in
                if results != nil {
                    self.recentActivities = results! as [Activity]
                    self.activityTableView.reloadData()
                }
            }
        }
    }
    
    override func onContactsChange(newContacts: [SwiftAddressBookPerson]) {
        let contactsCount = SCABManager.sharedInstance.contacts.count
        if let counterText = scanButtonCounter?.text {
            let splitArray = split(counterText) {
                $0 == "/"
            }
            scanButtonCounter?.text = String(format: NSLocalizedString("%@/%d", comment: ""), splitArray[0], contactsCount)
        } else {
            scanButtonCounter?.text = String(format: NSLocalizedString("%d/%d", comment: ""), 0, contactsCount)
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView == activityTableView {
            let activity = self.recentActivities[indexPath.row]
            let cell: UITableViewCell! = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "recentActivityTablecell")
            cell.textLabel?.text = activity.title
            cell.detailTextLabel?.text = activity.endTime.timeAgoSinceNow()
            
            if activity.type == ActivityTypes.DUPLICATE_SCAN {
                cell.imageView!.image = UIImage(named: IconSet.DuplicateGloss)?.imageTintedWithColor(kDefaultTint)
                
            } else if activity.type == ActivityTypes.MERGE_DUPLICATES {
                cell.imageView!.image = UIImage(named: IconSet.MergeGloss)?.imageTintedWithColor(kDefaultTint)
            }
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            return cell
            
        }
        var cell: ButtonTableCell! = tableView.dequeueReusableCellWithIdentifier("ButtonTableCell") as? ButtonTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("ButtonTableCell", owner: self, options: nil)[0] as? ButtonTableCell
        }
        scanButtonImage = cell.icon
        scanButtonTitle = cell.title
        scanButtonDesc = cell.subtitle
        scanButtonProgress = cell.progressView
        scanButtonCounter = cell.progressCount
        let contactsCount = SCABManager.sharedInstance.contacts.count
        scanButtonCounter?.text = String(format: NSLocalizedString("%d/%d", comment: ""), 0, contactsCount)
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == buttonTableView {
            return 1
        }
        return self.recentActivities.count
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == buttonTableView {
            return 10
        }
        return 0
    }
    
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if tableView == buttonTableView {
            return NSLocalizedString("RECENT", comment: "")
        }
        return nil
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == buttonTableView {
            return 80
        }
        return 44
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func reset(segue: UIStoryboardSegue) {
        //do stuff
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "duplicatesModalSegue" {
            if let confirmMergeViewController: ConfirmMergeViewController = segue.destinationViewController as? ConfirmMergeViewController {
                confirmMergeViewController.setRecords(self.duplicateRecords)
                confirmMergeViewController.delegate = self
                confirmMergeViewController.title = String(format: NSLocalizedString("%d Duplicates Found", comment: ""), self.duplicateRecords.count)
            } else {
                logError("Unable to create confirmMergeViewController")
            }
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == buttonTableView {
            if let cell = tableView.cellForRowAtIndexPath(indexPath) as? ButtonTableCell {
                buttonTapped(cell)
            }
        } else {
            let activity = self.recentActivities[indexPath.row]
            if activity.type == ActivityTypes.DUPLICATE_SCAN {
                let duplicates = (activity.data as NSDictionary) as Dictionary<NSNumber, NSNumber>
                var duplicateRecords = Dictionary<ABRecordID, ABRecordID>()
                for (key, value) in duplicates {
                    duplicateRecords[ABRecordID(key.integerValue)] = ABRecordID(value.integerValue)
                }
                self.duplicateRecords = duplicateRecords
                let confirmMergeViewController = storyboard!.instantiateViewControllerWithIdentifier("ConfirmMergeViewController") as ConfirmMergeViewController
                confirmMergeViewController.setRecords(self.duplicateRecords)
                confirmMergeViewController.delegate = self
                confirmMergeViewController.isActivity = true
                self.navigationController!.pushViewController(confirmMergeViewController, animated: true)
                
            } else if activity.type == ActivityTypes.MERGE_DUPLICATES {
                let mergedItemsViewController = storyboard!.instantiateViewControllerWithIdentifier("MergedItemsViewController") as MergedItemsViewController
                mergedItemsViewController.title = activity.title
                mergedItemsViewController.parentActivity = activity
                self.navigationController!.pushViewController(mergedItemsViewController, animated: true)
            }
        }
        
        
    }
    
    
    func showMergeCompletedMessage(mergeRecordMap: Dictionary<ABRecordID, ABRecordID>) {
        let title = NSLocalizedString("Merge Completed", comment: "")
        let message = String(format: NSLocalizedString("%d contacts merged", comment: ""), mergeRecordMap.count)
        showAlert(title, message: message)
    }
}



