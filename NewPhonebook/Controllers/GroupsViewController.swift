//
//  FavoritesViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/26/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import AddressBook
import AddressBookUI
import UIKit

struct ButtonTags{
    static let Search = 10
    static let Normal = 20
}

class GroupsViewController: BaseUITableViewController, UITableViewDataSource, UITableViewDelegate {
    let kAllSegmentControl = 0
    let kGroupsSegmentControl = 1
    let kSearchesSegmentControl = 2
    
    @IBAction func addGroupButtonClicked(sender: UIBarButtonItem) {
    }
    
    @IBAction func editGroupsButtonClicked(sender: UIBarButtonItem) {
        if defaultTableView.editing {
            defaultTableView.setEditing(false, animated: true)
            sender.style = UIBarButtonItemStyle.Plain
            sender.title = NSLocalizedString("Edit", comment: "")
        } else {
            defaultTableView.setEditing(true, animated: true)
            
            sender.style = UIBarButtonItemStyle.Done
            sender.title = NSLocalizedString("Done", comment: "")
        }
        
    }
    
    
    var groupEntries = [SCSearch]()
    
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // Update data source array here, something like [array removeObjectAtIndex:indexPath.row];
            var groupEntry = groupEntries[indexPath.row - 2]
            self.groupEntries.removeAtIndex(indexPath.row - 2)
            SCSearch.deleteEntry(groupEntry)
            tableView.deleteRowsAtIndexPaths(NSArray(object: indexPath), withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if indexPath.row < 2 {
            return false
        }
        return true
    }
    //View Connections
    @IBOutlet var defaultTableView: UITableView!
    @IBOutlet var segmentControl: UISegmentedControl!
    
    @IBAction func segmentControlChanged(sender: UISegmentedControl) {
        self.defaultTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CDModelManager.sharedInstance.registerCoreDataChangeHandler(self, entityName: "SCSearch")
        //import custom table cell nib
        self.tableView!.registerNib(UINib(nibName: "GroupsTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "groupsTableCell")
    }
    
    override func onCoreDataChange(entityName: String!) {
        logDebug("onCoreDataChange called")
        refreshGroups()
    }
    
    func refreshGroups() {
        if !defaultTableView.editing {
            //            NSOperationQueue().addOperationWithBlock({
            SCSearch.fetchRecentlyUsedEntries() {
                (results: [SCSearch]?) -> Void in
                if let groupEntries = results {
                    self.groupEntries = groupEntries
                    NSOperationQueue.mainQueue().addOperationWithBlock({
                        self.defaultTableView.reloadData()
                    })
                }
            }
            //            })
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    override func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        return 0
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: GroupsTableCell! = tableView.dequeueReusableCellWithIdentifier("groupsTableCell") as? GroupsTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("GroupsTableCell", owner: self, options: nil)[0] as? GroupsTableCell
        }
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        if indexPath.row == 0 {
            cell.titleLabel.text = NSLocalizedString("Recent Items", comment: "")
            
            cell.icon.tintImage = UIImage(named: "bb-recent-padding")
            cell.descriptionLabel.text = NSLocalizedString("Last Called", comment: "")
            cell.tagLabel.text = ""
        } else if indexPath.row == 1 {
            cell.titleLabel.text = NSLocalizedString("Top Contacts", comment: "")
            cell.icon.tintImage = UIImage(named: "bb-star")
            cell.descriptionLabel.text = NSLocalizedString("Frequency", comment: "")
            cell.tagLabel.text = ""
        } else {
            let savedSearch = self.groupEntries[indexPath.row - 2] as SCSearch
            cell.titleLabel.text = savedSearch.title
            cell.descriptionLabel.text = String(format: NSLocalizedString("%@ Search", comment: ""), savedSearch.scope)
            
            var scopeImageName = "bb-" + savedSearch.scope.lowercaseString
            if savedSearch.scope == "Social" && contains(["facebook", "twitter", "linkedin", "plus.google"], savedSearch.text.lowercaseString) {
                scopeImageName = "bb-" + savedSearch.text.lowercaseString
            }
            if let icon = UIImage(named: scopeImageName) {
                cell.icon.tintImage = icon
            } else {
                cell.icon.tintImage = UIImage(named: "bb-search")
            }
            cell.tagLabel.text = savedSearch.lastUsed.timeAgoSinceNow()
        }
        return cell
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.groupEntries.count + 2
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let groupDetailViewController = storyboard!.instantiateViewControllerWithIdentifier("GroupDetailViewController") as GroupDetailViewController
        self.navigationController!.pushViewController(groupDetailViewController, animated: true)
        if indexPath.row == 0 {
            groupDetailViewController.groupType = SCGroupTypes.RECENT
            groupDetailViewController.title = NSLocalizedString("Recently Contacted", comment: "")
            ContactLog.fetchRecentEntries() {
                (results: [ContactLog]?) -> Void in
                if let contactLogs = results {
                    var contactLogMap = Dictionary<String, ContactLog>()
                    var uniqueContactLogs = [ContactLog]()
                    for contactLog in contactLogs {
                        if let mappedContactLog = contactLogMap[contactLog.uniqueString()] {
                            mappedContactLog.linkedLogs.append(contactLog)
                        } else {
                            contactLogMap[contactLog.uniqueString()] = contactLog
                            uniqueContactLogs.append(contactLog)
                        }
                    }
                    
                    var groupContacts = [SwiftAddressBookPerson]()
                    for contactLog in uniqueContactLogs {
                        if contactLog.isContact {
                            if let person = SCABManager.swiftAddressBook?.personFromRecordId(ABRecordID(contactLog.abRecordID.integerValue)) {
                                groupContacts.append(person)
                            }
                        }
                    }
                    
                    groupDetailViewController.contactLogs = uniqueContactLogs
                    groupDetailViewController.groupContacts = groupContacts
                    if groupDetailViewController.defaultTableView != nil {
                        groupDetailViewController.defaultTableView.reloadData()
                    }
                    
                } else {
                    //No contactLogs. Nothing to do for now
                }
            }
        } else if indexPath.row == 1 {
            groupDetailViewController.title = NSLocalizedString("Top Contacts", comment: "")
            groupDetailViewController.groupType = SCGroupTypes.TOP
            ContactLog.fetchRecentEntries() {
                (results: [ContactLog]?) -> Void in
                if let contactLogs = results {
                    var contactLogMap = Dictionary<String, ContactLog>()
                    var uniqueContactLogs = [ContactLog]()
                    for contactLog in contactLogs {
                        if let mappedContactLog = contactLogMap[contactLog.uniqueString()] {
                            mappedContactLog.linkedLogs.append(contactLog)
                        } else {
                            contactLogMap[contactLog.uniqueString()] = contactLog
                            uniqueContactLogs.append(contactLog)
                        }
                    }
                    uniqueContactLogs.sort {
                        $0.linkedLogs.count > $1.linkedLogs.count
                    }
                    var groupContacts = [SwiftAddressBookPerson]()
                    for contactLog in uniqueContactLogs {
                        if contactLog.isContact {
                            if let person = SCABManager.swiftAddressBook?.personFromRecordId(ABRecordID(contactLog.abRecordID.integerValue)) {
                                groupContacts.append(person)
                            }
                        }
                    }
                    groupDetailViewController.contactLogs = uniqueContactLogs
                    groupDetailViewController.groupContacts = groupContacts
                    if groupDetailViewController.defaultTableView != nil {
                        groupDetailViewController.defaultTableView.reloadData()
                    }
                    
                } else {
                    //No contactLogs. Nothing to do for now
                }
            }
        } else {
            groupDetailViewController.groupType = SCGroupTypes.SEARCH
            let currentGroup = self.groupEntries[indexPath.row - 2] as SCSearch
            groupDetailViewController.title = currentGroup.title
            
            let hud = MBProgressHUD.showHUDAddedTo(groupDetailViewController.view,animated:true)
            hud.labelText = NSLocalizedString("Searching Contacts", comment: "")
            SCABManager.sharedInstance.getSmartGroup(currentGroup) {
                (results: [SwiftAddressBookPerson]) -> Void in
                groupDetailViewController.groupContacts = results
                if groupDetailViewController.defaultTableView != nil {
                    groupDetailViewController.defaultTableView.reloadData()
                    MBProgressHUD.hideHUDForView(groupDetailViewController.view,animated:true)
                }
                
            }
        }
        
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
}

