//
//  ContactsViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/25/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import UIKit
import AddressBook
import AddressBookUI
import MessageUI


class ContactsViewController: BaseUIViewController, MFMailComposeViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, ABNewPersonViewControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate, SavedSearchesDelegate {
    //IBOutlets
    @IBOutlet var defaultTableView: UITableView!
    
    var defaultCallButtonColor: UIColor?
    var defaultCallButtonImage: UIImage?
    
    @IBAction func editContactButtonClicked(sender: UIBarButtonItem) {
        if defaultTableView.editing {
            defaultTableView.setEditing(false, animated: true)
            sender.style = UIBarButtonItemStyle.Plain
            sender.title = NSLocalizedString("Edit", comment: "")
        } else {
            defaultTableView.setEditing(true, animated: true)
            
            sender.style = UIBarButtonItemStyle.Done
            sender.title = NSLocalizedString("Done", comment: "")
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // Update data source array here, something like [array removeObjectAtIndex:indexPath.row];
            let contact = self.sections[indexPath.section].contacts[indexPath.row]
            
            self.sections[indexPath.section].contacts.removeAtIndex(indexPath.row)
            if let contactIndex = find(self.contacts, contact) {
                self.contacts.removeAtIndex(contactIndex)
            }
            
            SCABManager.sharedInstance.removeContact(contact)
            
            tableView.deleteRowsAtIndexPaths(NSArray(object: indexPath), withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    @IBOutlet var editContactButton: UIBarButtonItem!
    //IBActions
    @IBAction func addContactButtonTapped(sender: UIBarButtonItem) {
        let addNewPersonController = ABNewPersonViewController()
        addNewPersonController.newPersonViewDelegate = self
        self.navigationController!.pushViewController(addNewPersonController, animated: true)
    }
    
    lazy var sectionQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Section Processing Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
        }()
    
    var currentActionSheetContact: SwiftAddressBookPerson?
    var filteredContacts = [SwiftAddressBookPerson]()
    var searchBarScope: String?
    
    var contacts = Array<SwiftAddressBookPerson>()
    var contactsHUD:MBProgressHUD?
    
    
    override func onContactsChange(newContacts: [SwiftAddressBookPerson]) {
        if !defaultTableView.editing {
            self.contacts = newContacts
            if contactsHUD == nil {
                contactsHUD = MBProgressHUD(view: view)
                contactsHUD!.labelText = NSLocalizedString("Loading Contacts", comment: "")
            }
            if !contactsHUD!.isDescendantOfView(view) {
                self.view.addSubview(contactsHUD!)
            }
            contactsHUD?.show(true)
            updateSections()
        }
    }
    var tableFooterLabel: UILabel?
    
    func focusOnSearch() {
        self.searchDisplayController!.setActive(true, animated: false)
    }
    func removeFocusOnSearch() {
        self.searchDisplayController!.setActive(false, animated: false)
    }
    
    func updateSections() {
        startTime = NSDate()
        var sections = [Section]()
        // we need to make sure first threadSafeFullName is called
        
        SCABManager.addressBookBlock({
            for contact in self.contacts {
                if contact.linkedTo != nil {
                    continue
                }
                let section = self.collation.sectionForObject(contact, collationStringSelector: "threadUnSafeFullName")
                while sections.count <= section {
                    sections.append(Section())
                }
                sections[section].addContact(contact)
            }
            }, completion: {
                self.sectionQueue.cancelAllOperations()
                self.sectionQueue.addOperationWithBlock({
                    logDebug("// create users from the name list: \(NSDate().timeIntervalSinceDate(self.startTime!))")
                    // sort each section
                    for section in sections {
                        section.contacts = self.collation.sortedArrayFromArray(section.contacts, collationStringSelector: "getFullName") as [SwiftAddressBookPerson]
                    }
                    logDebug("// sort each section: \(NSDate().timeIntervalSinceDate(self.startTime!))")
                    self.sections = sections
                    NSOperationQueue.mainQueue().addOperationWithBlock({
                        if self.defaultTableView != nil {
                            self.hideWaitIndicator()
                            self.defaultTableView.reloadData()
                            self.contactsHUD?.hide(true)
                            
                            if self.tableFooterLabel == nil {
                                let tableWidth = self.defaultTableView.frame.width
                                let footerView = UIView(frame: CGRectMake(0, 0, tableWidth, 24))
                                footerView.setTranslatesAutoresizingMaskIntoConstraints(false)
                                var label = UILabel(frame: CGRectMake(0, 0, 200, 24))
                                label.center = footerView.center
                                label.textAlignment = NSTextAlignment.Center
                                let contactsCount = self.contacts.count
                                label.text = String(format: NSLocalizedString("%d Contacts", comment: "Label text indicating number of contacts at the bottom of contacts table view"), contactsCount)
                                self.tableFooterLabel = label
                                footerView.addSubview(label)
                                self.defaultTableView.tableFooterView = footerView
                            } else {
                                let contactsCount = self.contacts.count
                                self.tableFooterLabel!.text = String(format: NSLocalizedString("%d Contacts", comment: "Label text indicating number of contacts at the bottom of contacts table view"), contactsCount)
                            }
                        }
                    })
                })
        })
    }
    
    var sections = [Section]()
    
    let collation = UILocalizedIndexedCollation.currentCollation()
        as UILocalizedIndexedCollation
    
    var searchHUD: MBProgressHUD?
    var lastSearchtext = ""
    var lastScope = ""
    
    var timer:NSTimer?
    
    func showSearchLoader(timer: NSTimer){
        searchHUD?.show(true)
    }
    
    func filterContentForSearchText(searchText: String, scope: String = kSearchDefaultScopeLabel) {
        if searchText == "" {
            self.filteredContacts = []
            let searchController = self.searchDisplayController!
            self.searchHUD?.hide(true)
            self.lastSearchtext = searchText
            self.lastScope = scope
            searchController.searchResultsTableView.reloadData()
            return
        }
        self.searchBarScope = scope
        let view = self.searchDisplayController!.searchResultsTableView!
        if searchHUD == nil {
            searchHUD = MBProgressHUD(view: view)
            searchHUD!.labelText = NSLocalizedString("Searching Records", comment: "")
            searchHUD!.userInteractionEnabled = false
        }
        if !searchHUD!.isDescendantOfView(view) {
            self.view.addSubview(searchHUD!)
        }
        
        
        timer?.invalidate()
        
        timer = NSTimer.scheduledTimerWithTimeInterval( 0.1, target: self, selector: "showSearchLoader:", userInfo: nil, repeats: false)
        
        var contactsToFilter = self.contacts
        if lastSearchtext != "" && startsWith(searchText, lastSearchtext) && scope == self.lastScope {
            contactsToFilter = self.filteredContacts
        }
        SCABManager.sharedInstance.filterContacts(contactsToFilter, text: searchText, scope: scope) {
            (results: [SwiftAddressBookPerson]) in
            self.filteredContacts = results
            let searchController = self.searchDisplayController!
            self.timer?.invalidate()
            self.searchHUD?.hide(true)
            self.lastSearchtext = searchText
            self.lastScope = scope
            searchController.searchResultsTableView.reloadData()
        }
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        let scopes = self.searchDisplayController!.searchBar.scopeButtonTitles as [String]
        let selectedScope = scopes[self.searchDisplayController!.searchBar.selectedScopeButtonIndex] as String
        self.filterContentForSearchText(searchString, scope: selectedScope)
        return false
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        let scope = self.searchDisplayController!.searchBar.scopeButtonTitles as [String]
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text, scope: scope[searchOption])
        return false
    }
    
    func searchDisplayController(controller: UISearchDisplayController, willShowSearchResultsTableView tableView: UITableView) {
        tableView.contentInset = UIEdgeInsetsZero
        //        tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, CGRectGetHeight(self.tabBarController!.tabBar.frame), 0.0)
        tableView.scrollIndicatorInsets = UIEdgeInsetsZero
    }
    
    class ContactWrapper {
        let person: SwiftAddressBookPerson
        var section: Int?
        init(person: SwiftAddressBookPerson) {
            self.person = person
        }
    }
    
    class Section {
        var contacts: [SwiftAddressBookPerson] = []
        func addContact(contact: SwiftAddressBookPerson) {
            self.contacts.append(contact)
        }
    }
    
    var startTime: NSDate?
    override func viewDidLoad() {
        super.viewDidLoad()
        SCABManager.sharedInstance.registerContactsChangeHandler(self)
        self.edgesForExtendedLayout = UIRectEdge.All
        //        self.extendedLayoutIncludesOpaqueBars = false
        self.automaticallyAdjustsScrollViewInsets = false
        self.defaultTableView.registerNib(UINib(nibName: "ContactsTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ContactsTableCell")
        self.searchDisplayController!.searchResultsTableView.registerNib(UINib(nibName: "ContactsTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "ContactsTableCell")
        
        
        
        //Add Save Search Button
        let view = NSBundle.mainBundle().loadNibNamed("SaveSearchView", owner: self, options: nil)[0] as UIView
        for childView in view.subviews {
            if childView is UIButton {
                childView.addTarget(self, action: "saveSearchButtonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
                childView.layer.borderColor = kDefaultTint.CGColor
            }
        }
        self.searchDisplayController!.searchResultsTableView.tableHeaderView = view
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return 1
        } else {
            return self.sections.count
        }
    }
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]! {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return nil
        } else {
            let currentCollation = UILocalizedIndexedCollation.currentCollation() as UILocalizedIndexedCollation
            return currentCollation.sectionIndexTitles as NSArray
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return nil
        } else {
            if self.sections[section].contacts.count == 0 {
                return nil;
            }
            let currentCollation = UILocalizedIndexedCollation.currentCollation() as UILocalizedIndexedCollation
            let sectionTitles = currentCollation.sectionTitles as NSArray
            return (sectionTitles.objectAtIndex(section) as? String)
        }
    }
    
    
    func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return 0
        }
        let currentCollation = UILocalizedIndexedCollation.currentCollation() as UILocalizedIndexedCollation
        return currentCollation.sectionForSectionIndexTitleAtIndex(index)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return self.filteredContacts.count
        } else {
            return self.sections[section].contacts.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var contact: SwiftAddressBookPerson!
        if tableView == self.searchDisplayController!.searchResultsTableView {
            contact = self.filteredContacts[indexPath.row]
        } else {
            contact = self.sections[indexPath.section].contacts[indexPath.row]
        }
        
        var cell: ContactsTableCell! = tableView.dequeueReusableCellWithIdentifier("ContactsTableCell") as? ContactsTableCell
        if cell == nil {
            logError("ContactsTableCell still nil after dequeing from tableView. Directly loading bundle")
            cell = NSBundle.mainBundle().loadNibNamed("ContactsTableCell", owner: tableView, options: nil)[0] as? ContactsTableCell
        }
        
        if let thumbnail = contact.imageDataWithFormat(SwiftAddressBookPersonImageFormat.thumbnail) {
            cell.picture.image = thumbnail
        } else {
            cell.picture.image = kDefaultAvatarImage
        }
        
        var status = ""
        if let jobTitle = contact.jobTitle {
            status = jobTitle
        }
        if let department = contact.department {
            if status != "" {
                status += ", "
            }
            status += department
        }
        if let organization = contact.organization {
            if status != "" {
                status += ", "
            }
            status += organization
        }
        
        var fullName = contact.getFullName()
        if fullName == "" {
            if let emails = contact.emails {
                fullName = emails[0].value
            } else if let phoneNumbers = contact.phoneNumbers {
                fullName = phoneNumbers[0].value
            } else {
                fullName = NSLocalizedString("No Name", comment: "")
            }
        }
        if status == "" {
            cell.fullNameCentered.hidden = false
            cell.fullNameCentered.text = fullName
            cell.fullName.text = ""
            cell.status.text = ""
        } else {
            cell.fullNameCentered.text = ""
            cell.fullName.text = fullName
            cell.status.text = status
        }
        
        
        if defaultCallButtonColor == nil {
            defaultCallButtonColor = cell.callButton.tintColor
        }
        if defaultCallButtonImage == nil {
            defaultCallButtonImage = cell.callButton.imageView!.image
        }
        //
        cell.callButton.tintColor = kSkyBlueColor
        if contact.phoneNumbers != nil {
            cell.callButton.tag = 10
            cell.callButton.setImage(defaultCallButtonImage, forState: UIControlState.Normal)
            
            cell.callButton.hidden = false
        } else if contact.emails != nil {
            cell.callButton.tag = 20
            cell.callButton.setImage(UIImage(named: IconSet.EmailFill48), forState: UIControlState.Normal)
        } else {
            cell.callButton.tag = 30
            cell.callButton.hidden = true
        }
        if tableView == self.searchDisplayController!.searchResultsTableView {
            cell.callButton.tag += 100
        } else {
            cell.callButton.tag += 200
        }
        cell.callButton.addTarget(self, action: "callButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        return cell!
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return 50
        } else {
            return 50
        }
    }
    
    func saveSearchButtonClicked(sender: AnyObject) {
        let searchDisplayController = self.searchDisplayController!
        let searchText = searchDisplayController.searchBar.text
        let searchScope = self.searchBarScope!
        var searchTitleSuggestion = String(format: NSLocalizedString("Search Contacts for \"%@\"", comment: ""), searchText)
        if searchScope == "Address" {
            searchTitleSuggestion = String(format: NSLocalizedString("Contacts in %@", comment: ""), searchText)
        } else if searchScope == "Name" {
            searchTitleSuggestion = String(format: NSLocalizedString("Contacts named %@", comment: ""), searchText)
        } else if searchScope == "Phone" {
            searchTitleSuggestion = String(format: NSLocalizedString("With number %@", comment: ""), searchText)
        } else if searchScope == "Email" {
            searchTitleSuggestion = String(format: NSLocalizedString("With email %@", comment: ""), searchText)
        } else if searchScope == "Social" {
            searchTitleSuggestion = String(format: NSLocalizedString("Contacts in %@", comment: ""), searchText)
        }
        self.getTextInput(NSLocalizedString("Saved Search Title", comment: ""),
            message: NSLocalizedString("Set an appropriate title of the Search",
                comment: "This will be displayed in the saved search list"),
            defaultValue: searchTitleSuggestion) {
                (value: String!) in
                SCSearch.addEntry(searchText, title: value, scope: searchScope)
                let title = NSLocalizedString("Search Saved", comment: "")
                let message = NSLocalizedString("Search has been saved. To use this search, go to Groups Tab", comment: "")
                self.showAlert(title, message: message)
                //            self.showSearchSavedAlert()
        }
        return
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var person: SwiftAddressBookPerson!
        if tableView == self.searchDisplayController!.searchResultsTableView {
            person = self.filteredContacts[indexPath.row]
        } else {
            person = self.sections[indexPath.section].contacts[indexPath.row]
        }
        let personController = ABPersonViewController()
        person.performRecordBlock({
            (internalRecord: ABRecord) in
            personController.displayedPerson = internalRecord
        })
        personController.shouldShowLinkedPeople = true
        self.navigationController!.pushViewController(personController, animated: true)
    }
    
    func showSearchSavedAlert() {
        let title = NSLocalizedString("Search Saved", comment: "")
        let message = NSLocalizedString("The Search has been saved. Would you like to Add another Search?", comment: "")
        let cancelButtonTitle = NSLocalizedString("NO", comment: "")
        let otherButtonTitle = NSLocalizedString("YES", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
            //            self.searchDisplayController!.setActive(false, animated: true)
        }
        let otherAction = UIAlertAction(title: otherButtonTitle, style: .Default) {
            action in
            //            self.searchDisplayController!.searchBar.text = ""
        }
        alertController.addAction(cancelAction)
        alertController.addAction(otherAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func newPersonViewController(newPersonView: ABNewPersonViewController!, didCompleteWithNewPerson person: ABRecord!) {
        newPersonView.navigationController!.popViewControllerAnimated(true)
        if person != nil {
            //No need for calling refreshContacts as it is called by external notification
            //SCABManager.sharedInstance.refreshContacts() this is no longer required with external call
        }
    }
    //Set default number
    func actionSheet(myActionSheet: UIActionSheet!, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            
            return
        }
        let numberIndex = buttonIndex - 1
        let newMultiValueEntry = MultivalueEntry(value: self.currentActionSheetContact!.phoneNumbers![numberIndex].value, label: kDefaultLabel, id: -1)
        self.currentActionSheetContact!.phoneNumbers!.append(newMultiValueEntry)
        SCABManager.sharedInstance.saveAddressBook()
        let numberToCall = newMultiValueEntry.value.replace("[\\s]", template: "-")
        UIApplication.sharedApplication().openURL(telURL(numberToCall))
        self.currentActionSheetContact = nil // release contact
    }
    
    func callButtonTapped(sender: UIButton) {
        //Small Hack here: Need to revisit
        let cell = sender.superview!.superview as UITableViewCell
        var contactOptional: SwiftAddressBookPerson?
        if sender.tag < 200 {
            let indexPath = self.searchDisplayController!.searchResultsTableView.indexPathForCell(cell)
            contactOptional = self.filteredContacts[indexPath!.row]
        } else {
            let indexPath = self.defaultTableView.indexPathForCell(cell)
            contactOptional = self.sections[indexPath!.section].contacts[indexPath!.row]
        }
        let contact = contactOptional!
        if contains([220, 120], sender.tag) {
            if let emails = contact.emails {
                let email = emails[0].value
                self.configuredMailComposeViewController(email)
            } else {
                logInfo("Call Button Clicked for contact with No number")
            }
        } else {
            if let phoneNumbers = contact.phoneNumbers {
                if phoneNumbers.count > 1 {
                    var defaultNumber: String?
                    var defaultLabel: String?
                    let preCallActionSheet = UIActionSheet(title: NSLocalizedString("Choose Default Number. Will be remembered", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("Cancel", comment: ""), destructiveButtonTitle: nil)
                    for phoneNumber in phoneNumbers {
                        if phoneNumber.label == kDefaultLabel {
                            defaultNumber = phoneNumber.value
                            defaultLabel = phoneNumber.label
                        } else {
                            preCallActionSheet.addButtonWithTitle(phoneNumber.value)
                        }
                    }
                    if defaultNumber == nil {
                        preCallActionSheet.cancelButtonIndex = 0
                        self.currentActionSheetContact = contact
                        preCallActionSheet.showInView(self.view)
                    } else {
                        ContactLog.addEntry(defaultNumber!, label: defaultLabel!, abRecordID: contact.getRecordID())
                        let numberToCall = defaultNumber!.replace("[\\s]", template: "-")
                        UIApplication.sharedApplication().openURL(telURL(numberToCall))
                    }
                } else {
                    ContactLog.addEntry(phoneNumbers[0].value, label: phoneNumbers[0].label, abRecordID: contact.getRecordID())
                    let numberToCall = phoneNumbers[0].value.replace("[\\s]", template: "-")
                    UIApplication.sharedApplication().openURL(telURL(numberToCall))
                }
            } else {
                logInfo("Call Button Clicked for contact with No number")
            }
        }
        
    }
    
    func configuredMailComposeViewController(email: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        mailComposerVC.setToRecipients([email])
        self.presentViewController(mailComposerVC, animated: true, completion: nil)
        return mailComposerVC
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func searchBarBookmarkButtonClicked(searchBar: UISearchBar) {
        self.performSegueWithIdentifier("savedSearchesSegue", sender: self)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "savedSearchesSegue" {
            let dest = segue.destinationViewController as SavedSearchesController
            dest.delegate = self
            SCSearch.fetchRecentlyUsedEntries() {
                (results: [SCSearch]?) -> Void in
                if let savedSearches = results {
                    dest.savedSearches = savedSearches
                }
            }
            
        }
    }
    
    @IBAction func reset(segue: UIStoryboardSegue) {
        //do stuff
    }
    
    func didSelectSearch(search: SCSearch) {
        let searchBar = self.searchDisplayController!.searchBar
        let scopeTitles = searchBar.scopeButtonTitles as [String]
        let searchScopeIndex = find(scopeTitles, search.scope)
        if searchScopeIndex != nil {
            searchBar.selectedScopeButtonIndex = searchScopeIndex!
        } else {
            searchBar.selectedScopeButtonIndex = 0
        }
        self.searchDisplayController!.setActive(true, animated: false)
        searchBar.text = search.text
        self.filterContentForSearchText(search.text, scope: search.scope)
    }
}

