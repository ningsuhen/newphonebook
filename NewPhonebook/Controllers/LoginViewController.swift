//
//  SCRootViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/21/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: BaseUIViewController, FBLoginViewDelegate {
    
    @IBOutlet var loginView: FBLoginView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginView.readPermissions = kFBPermissions
        self.loginView.delegate = self
        //        self.loginView.backgroundColor = UIColor.whiteColor()
        self.setNeedsStatusBarAppearanceUpdate()
    }
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginViewShowingLoggedInUser(loginView: FBLoginView) {
        logDebug("loginViewShowingLoggedInUser")
    }
    
    func loginViewFetchedUserInfo(loginView: FBLoginView?, user: FBGraphUser) {
        logDebug("loginViewFetchedUserInfo: \(user.first_name) \(user.last_name)")
        Preferences.sharedInstance.facebookAccount = "\(user.first_name) \(user.last_name)"
        if let userInfo = Preferences.sharedInstance.userInfo {
            logDebug("updating userInfo")
            Preferences.sharedInstance.userInfo = userInfo.update(user)
        } else {
            logDebug("setting userInfo")
            Preferences.sharedInstance.userInfo = SCUserInfo(fbGraphUser: user)
        }
        SCABManager.sharedInstance.facebookLoginCallback()
        self.dismissViewControllerAnimated(true,completion:nil)
    }
    
    func loginViewShowingLoggedOutUser(loginView: FBLoginView?) {
        logDebug("loginViewShowingLoggedOutUser")
        Preferences.sharedInstance.facebookAccount = kUnlinkedAccount
    }
    
    func loginView(loginView:FBLoginView,handleError error:NSError){
        logError("error with Facebook LoginView: \(error)")
    }
    
    
}