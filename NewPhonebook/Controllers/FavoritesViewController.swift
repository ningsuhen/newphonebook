//
//  FavoritesViewController.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/26/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import AddressBook
import AddressBookUI
import UIKit

class FavoritesViewController: BaseUIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, ABPeoplePickerNavigationControllerDelegate {
    let kAllSegmentControl = 0
    let kFavoritesSegmentControl = 1
    let kSmartSegmentControl = 2
    var currentActionSheetPerson: SwiftAddressBookPerson?
    
    //View Connections
    @IBOutlet var defaultTableView: UITableView!
    
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBAction func segmentControlChanged(sender: UISegmentedControl) {
        self.defaultTableView.reloadData()
    }
    
    @IBAction func editFavorites(sender: UIBarButtonItem) {
        if defaultTableView.editing {
            defaultTableView.setEditing(false, animated: true)
            sender.style = UIBarButtonItemStyle.Plain
            sender.title = NSLocalizedString("Edit", comment: "")
        } else {
            defaultTableView.setEditing(true, animated: true)
            
            sender.style = UIBarButtonItemStyle.Done
            sender.title = NSLocalizedString("Done", comment: "")
        }
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            // Update data source array here, something like [array removeObjectAtIndex:indexPath.row];
            var contactEntry: Favorite!
            if self.segmentControl.selectedSegmentIndex == kAllSegmentControl {
                contactEntry = favoriteEntries[indexPath.row]
                self.favoriteEntries.removeAtIndex(indexPath.row)
            } else if self.segmentControl.selectedSegmentIndex == kFavoritesSegmentControl {
                contactEntry = sections[0][indexPath.row]
                self.sections[0].removeAtIndex(indexPath.row)
                if let index = find(self.favoriteEntries, contactEntry) {
                    self.favoriteEntries.removeAtIndex(index)
                }
                
            } else if self.segmentControl.selectedSegmentIndex == kSmartSegmentControl {
                contactEntry = sections[1][indexPath.row]
                self.sections[1].removeAtIndex(indexPath.row)
                if let index = find(self.favoriteEntries, contactEntry) {
                    self.favoriteEntries.removeAtIndex(index)
                }
                
            }
            Favorite.deleteEntry(contactEntry)
            
            tableView.deleteRowsAtIndexPaths(NSArray(object: indexPath), withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    var searchController: UISearchController!
    
    @IBAction func addContactButtonTapped(sender: UIBarButtonItem) {
        let abPersonPickerController = ABPeoplePickerNavigationController()
        SCABManager.swiftAddressBook?.performAddressBookBlock({
            (internalAddressBook: ABAddressBook) in
            abPersonPickerController.addressBook = internalAddressBook
        })
        abPersonPickerController.peoplePickerDelegate = self
        presentViewController(abPersonPickerController, animated: true,completion:nil)
    }
    
    func peoplePickerNavigationController(peoplePicker: ABPeoplePickerNavigationController!, didSelectPerson person: ABRecord!) {
        if let contact = SCABManager.swiftAddressBook?.personFromRecord(person) {
            if let phoneNumbers = contact.phoneNumbers {
                if phoneNumbers.count > 1 {
                    var defaultNumber: String?
                    let preCallActionSheet = UIActionSheet(title: NSLocalizedString("Add to Favorites", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("Cancel", comment: ""), destructiveButtonTitle: nil)
                    for phoneNumber in phoneNumbers {
                        let label = ABAddressBookCopyLocalizedLabel(phoneNumber.label).takeRetainedValue() as NSString
                        let buttonTitle = String(format: NSLocalizedString("%@ : %@", comment: "[label] : [phoneNumber]"), label, phoneNumber.value)
                        logDebug(buttonTitle)
                        preCallActionSheet.addButtonWithTitle(buttonTitle)
                    }
                    preCallActionSheet.cancelButtonIndex = 0
                    currentActionSheetPerson = contact
                    preCallActionSheet.showInView(self.view)
                } else {
                    Favorite.addEntry(phoneNumbers[0].value, abRecordID: contact.getRecordID(), label: phoneNumbers[0].label, reason: FavoriteReasons.Added)
                }
            } else {
                peoplePicker.dismissViewControllerAnimated(true) {
                    self.showAlert(NSLocalizedString("No Phone Number", comment: ""), message: NSLocalizedString("Selected Contact doesn't have a phone number", comment: ""))
                }
            }
        } else {
            //person expected to be found but not available
            logError("selected person not found in swiftAddressBook: \(person)")
        }
        
    }
    //Set default number
    func actionSheet(myActionSheet: UIActionSheet!, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            return
        }
        let numberIndex = buttonIndex - 1
        let title = myActionSheet.buttonTitleAtIndex(buttonIndex)
        let titleArray = title.componentsSeparatedByString(" : ")
        let label = titleArray[0]
        let value = titleArray[1]
        var recordID: ABRecordID?
        if currentActionSheetPerson != nil {
            recordID = currentActionSheetPerson!.getRecordID()
        }
        Favorite.addEntry(value, abRecordID: recordID, label: label, reason: FavoriteReasons.Added)
        refreshFavorites()
    }
    
    @IBAction func searchButtonClicked(button: UIBarButtonItem) {
        // Create the search results view controller and use it for the UISearchController.
        let searchResultsController = storyboard!.instantiateViewControllerWithIdentifier(SearchResultsViewController.StoryboardConstants.identifier) as SearchResultsViewController
        
        // Create the search controller and make it perform the results updating.
        searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchResultsUpdater = searchResultsController
        searchController.searchBar.placeholder = NSLocalizedString("Search All Contact", comment: "")
        searchController.hidesNavigationBarDuringPresentation = false
        
        // Present the view controller.
        presentViewController(searchController, animated: true, completion: nil)
    }
    
    //Constants and one time initlializations
    let collation = UILocalizedIndexedCollation.currentCollation()
        as UILocalizedIndexedCollation
    
    //Variables
    var favoriteEntries = [Favorite]()
    var contacts = Array<SwiftAddressBookPerson>()
    
    
    var sections = [[Favorite]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SCABManager.sharedInstance.registerContactsChangeHandler(self)
        CDModelManager.sharedInstance.registerCoreDataChangeHandler(self, entityName: "Favorite")
        //        self.refreshFavorites()
        self.defaultTableView.registerNib(UINib(nibName: "FavoriteTableCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: "FavoriteTableCell")
        
        
    }
    var skipCDNotification = false
    override func onCoreDataChange(entityName: String!) {
        logDebug("onCoreDataChange called")
        if skipCDNotification{
            skipCDNotification = false
            return
        }
        refreshFavorites()
    }
    
    
    func refreshFavorites() {
        if !defaultTableView.editing {
            //            NSOperationQueue().addOperationWithBlock({
            Favorite.fetchAllEntries() {
                (results: [AnyObject]?) -> Void in
                let favoriteEntries = results as [Favorite]
                self.updateSections(favoriteEntries) {
                    (activeFavorites:[Favorite]) in
                    self.favoriteEntries = activeFavorites
                    NSOperationQueue.mainQueue().addOperationWithBlock({
                        self.defaultTableView.reloadData()
                    })
                }
            }
            //            })
        }
    }
    
    func updateSections(favoriteEntries: [Favorite], completion: ((activeFavorites:[Favorite]) -> Void)) {
        
        var sections = [[Favorite]]()
        sections.append([Favorite]())  //added
        sections.append([Favorite]())  //smart
        
        
        var activeFavorites = [Favorite]()
        for favoriteEntry in favoriteEntries {
            if favoriteEntry.isContact && SCABManager.swiftAddressBook?.personFromRecordId(ABRecordID(favoriteEntry.abRecordID.integerValue)) == nil {
                Favorite.deleteEntry(favoriteEntry)
                continue
            }
            activeFavorites.append(favoriteEntry)
            if favoriteEntry.reason == FavoriteReasons.Added {
                sections[0].append(favoriteEntry) //Added
            } else {
                sections[1].append(favoriteEntry) //Smart
            }
        }
        
        self.sections = sections
        completion(activeFavorites:activeFavorites)
    }
    
    func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        return 0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: FavoriteTableCell! = tableView.dequeueReusableCellWithIdentifier("FavoriteTableCell") as? FavoriteTableCell
        if cell == nil {
            cell = NSBundle.mainBundle().loadNibNamed("FavoriteTableCell", owner: self, options: nil)[0] as? FavoriteTableCell
        }
        
        var favoriteEntry: Favorite!
        if self.segmentControl.selectedSegmentIndex == kAllSegmentControl {
            favoriteEntry = favoriteEntries[indexPath.row]
        } else if self.segmentControl.selectedSegmentIndex == kFavoritesSegmentControl {
            favoriteEntry = sections[0][indexPath.row]
        } else if self.segmentControl.selectedSegmentIndex == kSmartSegmentControl {
            favoriteEntry = sections[1][indexPath.row]
        }
        
        
        if favoriteEntry.isContact {
            if favoriteEntry.contact == nil {
                let recordId = ABRecordID(favoriteEntry.abRecordID.integerValue)
                if let tempContact = SCABManager.swiftAddressBook?.personFromRecordId(recordId) {
                    favoriteEntry.contact = tempContact
                }
            }
            if let contact = favoriteEntry.contact {
                if contact.image != nil {
                    cell.picture.image = contact.image
                } else {
                    cell.picture.image = kDefaultAvatarImage
                }
                cell.fullName.text = contact.getFullName()
                
                cell.status.text = favoriteEntry.value
                cell.subStatus.text = favoriteEntry.localizedLabel.capitalizedString
            }
        } else {
            //favoriteEntry is not a contact. No Need to handle for now
        }
        return cell
    }
    
    
    func newPersonViewController(newPersonView: ABNewPersonViewController!, didCompleteWithNewPerson person: ABRecord!) {
        newPersonView.navigationController!.popViewControllerAnimated(true)
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segmentControl.selectedSegmentIndex == kAllSegmentControl {
            return favoriteEntries.count
        } else if self.segmentControl.selectedSegmentIndex == kFavoritesSegmentControl {
            return sections[0].count
        } else if self.segmentControl.selectedSegmentIndex == kSmartSegmentControl {
            return sections[1].count
        }
        return 0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let favoriteEntry = self.favoriteEntries[indexPath.row]
        let recordId = ABRecordID(favoriteEntry.abRecordID.integerValue)
        
        if let contact = SCABManager.swiftAddressBook?.personFromRecordId(recordId){
            if favoriteEntry.valueType == CLConstants.kTypePhoneNumber {
                self.skipCDNotification = true
                ContactLog.addEntry(favoriteEntry.value, label: favoriteEntry.label, abRecordID: contact.getRecordID())
                let numberToCall = favoriteEntry.value.replace("[\\s]", template: "-")
                UIApplication.sharedApplication().openURL(telURL(numberToCall))
            }
        }
        else{
            showAlert(NSLocalizedString("Contact doesn't exist",comment:""), message:NSLocalizedString("Selected Contact has been deleted in Address Book, removing item from favorites",comment:""))
            if self.segmentControl.selectedSegmentIndex == kAllSegmentControl {
                self.favoriteEntries.removeAtIndex(indexPath.row)
            } else if self.segmentControl.selectedSegmentIndex == kFavoritesSegmentControl {
                self.sections[0].removeAtIndex(indexPath.row)
                if let index = find(self.favoriteEntries, favoriteEntry) {
                    self.favoriteEntries.removeAtIndex(index)
                }
                
            } else if self.segmentControl.selectedSegmentIndex == kSmartSegmentControl {
                self.sections[1].removeAtIndex(indexPath.row)
                if let index = find(self.favoriteEntries, favoriteEntry) {
                    self.favoriteEntries.removeAtIndex(index)
                }
            }
            tableView.deleteRowsAtIndexPaths(NSArray(object: indexPath), withRowAnimation: UITableViewRowAnimation.Fade)
            Favorite.deleteEntry(favoriteEntry)
        }
        
        
    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        var favoriteEntry:Favorite!
        if self.segmentControl.selectedSegmentIndex == kAllSegmentControl {
            favoriteEntry = favoriteEntries[indexPath.row]
        } else if self.segmentControl.selectedSegmentIndex == kFavoritesSegmentControl {
            favoriteEntry = sections[0][indexPath.row]
        } else if self.segmentControl.selectedSegmentIndex == kSmartSegmentControl {
            favoriteEntry = sections[1][indexPath.row]
        } else{
            logError("Favorites accessoryButtonTapped with incorrect segment control index")
            return
        }
        let recordId = ABRecordID(favoriteEntry.abRecordID.integerValue)
        
        if let person = SCABManager.swiftAddressBook?.personFromRecordId(recordId){
            let personController = ABPersonViewController()
            person.performRecordBlock({
                (internalRecord: ABRecord) in
                personController.displayedPerson = internalRecord
            })
            personController.shouldShowLinkedPeople = true
            self.navigationController!.pushViewController(personController, animated: true)
        }
        else{
            showAlert(NSLocalizedString("Contact doesn't exist",comment:""), message:NSLocalizedString("Selected Contact has been deleted in Address Book, removing item from favorites",comment:""))
            if self.segmentControl.selectedSegmentIndex == kAllSegmentControl {
                self.favoriteEntries.removeAtIndex(indexPath.row)
            } else if self.segmentControl.selectedSegmentIndex == kFavoritesSegmentControl {
                self.sections[0].removeAtIndex(indexPath.row)
                if let index = find(self.favoriteEntries, favoriteEntry) {
                    self.favoriteEntries.removeAtIndex(index)
                }
                
            } else if self.segmentControl.selectedSegmentIndex == kSmartSegmentControl {
                self.sections[1].removeAtIndex(indexPath.row)
                if let index = find(self.favoriteEntries, favoriteEntry) {
                    self.favoriteEntries.removeAtIndex(index)
                }
            }
            
            tableView.deleteRowsAtIndexPaths(NSArray(object: indexPath), withRowAnimation: UITableViewRowAnimation.Fade)
            Favorite.deleteEntry(favoriteEntry)
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
}
