//
//  AppDelegate.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/17/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: NSString?, annotation: AnyObject) -> Bool {
        var wasHandled = false
        if url.scheme! == kFacebookScheme {
            wasHandled = FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication)
        } else if url.scheme! == kDropboxScheme {
            if DBSession.sharedSession().handleOpenURL(url) {
                if DBSession.sharedSession().isLinked() {
                    if SCABManager.sharedInstance.dropboxAccessCompletionBlock != nil {
                        SCABManager.sharedInstance.dropboxAccessCompletionBlock!()
                    } else {
                        logError("Dropbox accessCompletionBlock is nil")
                    }
                } else {
                    logError("Dropbox handleOpenURL called but not linked")
                }
                wasHandled = true
            }
        }
        return wasHandled
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: NSDictionary) -> Bool {
        // Whenever a person opens the app, check for a cached session
        if (FBSession.activeSession().state == FBSessionState.CreatedTokenLoaded) {
            // If there's one, just open the session silently, without showing the user the login UI
            FBSession.openActiveSessionWithReadPermissions(kFBPermissions, allowLoginUI: false, completionHandler: {
                (session: FBSession!, state: FBSessionState, error: NSError!) in
                // Handler for session state changes
                // This method will be called EACH time the session state changes,
                // also for intermediate states and NOT just when the session open
                //self.sessionStateChanged(session, state: state, error: error)
                if let gotError = error {
                    logError("Error in opening Facebook Active Session- \(gotError)")
                } else {
                    //Login Successful, just call facebookLoginCallback for safety
                    SCABManager.sharedInstance.facebookLoginCallback()
                }
            })
        } else {
            var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            var viewController: LoginViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as LoginViewController
            self.window!.makeKeyAndVisible()
            self.window!.rootViewController!.presentViewController(viewController, animated: false, completion: nil)
        }
        Appirater.setAppId(kAppID)
        Appirater.setDaysUntilPrompt(1)
        Appirater.setUsesUntilPrompt(3)
        Appirater.setSignificantEventsUntilPrompt(-1)
        Appirater.setTimeBeforeReminding(2)
        Appirater.setDebug(false)
        
        //Setup CocoaLumberjack
        DDLog.addLogger(DDASLLogger.sharedInstance())
        DDLog.addLogger(DDTTYLogger.sharedInstance())
        DDTTYLogger.sharedInstance().colorsEnabled = true
        DDLog.logLevel = .Warning
        //Listen for settings changes
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "settingsDidChange:", name: kIASKAppSettingChanged, object: nil)
        return true
    }
    /**
    utility function to show alert when there's no access to Address Book or there's an error
    */
    func showAddressBookPermissionAlert() {
        let cancelButtonTitle = NSLocalizedString("OK", comment: "")
        let title = NSLocalizedString("AddressBook Access is required", comment: "")
        let message = NSLocalizedString("You must enable Address Book access in your settings.", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        // Create the action.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) {
            action in
        }
        alertController.addAction(cancelAction)
        self.window!.rootViewController!.presentViewController(alertController, animated: true, completion: nil)
    }
    /**
    callback for getting notifications of settings changes
    
    :param: notification NSNotification object
    */
    func settingsDidChange(notification: NSNotification) {
        if let key = notification.object as? String {
            if startsWith(key, StringConstants.MergeSettingsKey) {
                Preferences.sharedInstance.refreshMergeSettings()
            } else if startsWith(key, StringConstants.SearchSettingsKey) {
                Preferences.sharedInstance.refreshSearchSettings()
            }
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBAppEvents.activateApp()
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationLibraryDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.waikhom.NewPhonebook" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.LibraryDirectory, inDomains: .UserDomainMask)
        return urls[urls.count - 1] as NSURL
        }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("NewPhonebook", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
        }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationLibraryDirectory.URLByAppendingPathComponent("NewPhonebook.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            let dict = NSMutableDictionary()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            logError("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
        }()
    
    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
        }()
    
    // MARK: - Core Data Saving support
    
    func saveContext() {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                logError("Unresolved error \(error), \(error!.userInfo)")
                abort()
            }
        }
    }
    
    
    
    
}

