//
//  ModelExtensions.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/26/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import AddressBook
import CoreData

typealias ContactLogType = NSNumber
typealias ContactLogValueType = NSNumber


struct CLConstants {
    static let kLogTypePhoneCall = 0
    static let kLogTypeMessage = 1
    static let kLogTypeEmail = 2
    
    static let kTypePhoneNumber = 0
    static let kTypeEmail = 1
    
    
}

struct FavoriteReasons {
    static let Added = NSNumber(int: 0)
    static let Frequent = NSNumber(int: 1)
    static let Contents = NSNumber(int: 2)
    static let Recent = NSNumber(int: 3)
}

struct ActivityTypes {
    static let DUPLICATE_SCAN = NSNumber(int: 0)
    static let MERGE_DUPLICATES = NSNumber(int: 1)
    static let DUMMIES_SCAN = NSNumber(int: 2)
    static let CLEAN_DUMMIES = NSNumber(int: 3)
}

struct ActivityStatuses {
    static let NA = NSNumber(int: 0)
}

struct MergeItemStatuses {
    static let DUPLICATE = NSNumber(int: 0)
    static let MERGED = NSNumber(int: 1)
}


extension ContactLog {
    
    /**
    utility method to compose unique string for contact log
    
    :returns: unique string which is a combination of the value, label and record ID
    */
    func uniqueString() -> String {
        return "\(value)-\(label)-\(abRecordID)"
    }
    
    /**
    Adds new entry to ContactLog Core Data Model
    
    :param: value      value of contact - e.g. phone number value
    :param: label      label of the value - e.g. Home
    :param: abRecordID Address Book record Id
    :param: valueType  whether value is a phone numer or email id or twitter id
    :param: logType    whether it's a phone call or email or twitter message
    */
    class func addEntry(value: String, label: String = "", abRecordID: ABRecordID? = nil, valueType: ContactLogValueType? = nil, logType: ContactLogType? = nil) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let item = NSEntityDescription.insertNewObjectForEntityForName("ContactLog", inManagedObjectContext: CDModelManager.context) as ContactLog
            item.startTime = NSDate()
            item.value = value
            item.label = label
            item.isContact = false
            if abRecordID != nil {
                item.abRecordID = NSNumber(int: abRecordID!)
                item.isContact = true
            }
            if logType != nil {
                item.logType = logType!
            } else {
                item.logType = CLConstants.kLogTypePhoneCall
            }
            if valueType != nil {
                item.valueType = valueType!
            } else {
                item.valueType = CLConstants.kTypePhoneNumber
            }
            
            if let favoriteItem = Favorite.entryForValue(value, label: label, abRecordID: abRecordID) {
                favoriteItem.addCallLogItem(item)
                if favoriteItem.callLogsCount.integerValue > 10 && favoriteItem.reason == FavoriteReasons.Recent {
                    favoriteItem.reason = FavoriteReasons.Frequent
                }
            } else if let favoriteItem = Favorite.addEntry(value, abRecordID: abRecordID, label: label, reason: FavoriteReasons.Recent, save: false) {
                favoriteItem.addCallLogItem(item)
            }
            var e: NSError?
            if !CDModelManager.context.save(&e) {
                logError("insert error: \(e!.localizedDescription)")
                abort()
            }
            CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("ContactLog")
            CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("Favorite")
        })
    }
    
    class func clean() {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("ContactLog", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [ContactLog] {
                for item in fetchedResults {
                    CDModelManager.context.deleteObject(item)
                }
                var e: NSError?
                if !CDModelManager.context.save(&e) {
                    logError("insert error: \(e!.localizedDescription)")
                    abort()
                }
                CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("ContactLog")
            }
        })
    }
    
    class func fetchRecentEntries(completion: ([ContactLog]?) -> Void, limit: Int = 50) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("ContactLog", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            fetchRequest.fetchLimit = limit
            let sortDescriptor = NSSortDescriptor(key: "startTime", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = [sortDescriptor]
            if let fetchResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [ContactLog] {
                completion(fetchResults)
                return
            }
            completion(nil)
        })
    }
}


extension SCSearch {
    
    class func addEntry(text: String, title: String, scope: String, desc: String? = nil) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let item = NSEntityDescription.insertNewObjectForEntityForName("SCSearch", inManagedObjectContext: CDModelManager.context) as SCSearch
            item.text = text
            item.title = title
            item.scope = scope
            if desc != nil {
                item.desc = desc!
            } else {
                item.desc = ""
            }
            item.creationDate = NSDate()
            item.lastUsed = NSDate()
            var e: NSError?
            if !CDModelManager.context.save(&e) {
                logError("insert error: \(e!.localizedDescription)")
                abort()
            }
            CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("SCSearch")
        })
    }
    
    class func deleteEntry(entry: SCSearch) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            CDModelManager.context.deleteObject(entry)
            var e: NSError?
            if !CDModelManager.context.save(&e) {
                logError("insert error: \(e!.localizedDescription)")
                abort()
            }
            CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("SCSearch")
        })
        
    }
    
    class func fetchRecentlyUsedEntries(completion: ([SCSearch]?) -> Void, limit: Int = 50) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("SCSearch", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            fetchRequest.fetchLimit = limit
            let sortDescriptor = NSSortDescriptor(key: "lastUsed", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = [sortDescriptor]
            if let fetchResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [SCSearch] {
                completion(fetchResults)
                return
            }
            completion(nil)
        })
    }
    
    class func fetchRecentlyCreatedEntries(completion: ([AnyObject]?) -> Void, limit: Int = 50) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("SCSearch", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            fetchRequest.fetchLimit = limit
            let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = [sortDescriptor]
            if let fetchResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [SCSearch] {
                completion(fetchResults)
                return
            }
            completion(nil)
        })
    }
    
    
}


extension Activity {
    
    class func addEntry(title: String, type: NSNumber, data: AnyObject? = nil, desc: String = "", startTime: NSDate = NSDate(), endTime: NSDate = NSDate(), status: NSNumber = ActivityStatuses.NA, save:Bool = true) -> Activity? {
        var item: Activity?
        CDModelManager.getQueue().waitForOperationWithBlock({
            item = NSEntityDescription.insertNewObjectForEntityForName("Activity", inManagedObjectContext: CDModelManager.context) as? Activity
            item!.title = title
            item!.type = type
            item!.desc = desc
            item!.startTime = startTime
            item!.endTime = endTime
            item!.status = status
            item!.data = data
            if save{
                var e: NSError?
                if !CDModelManager.context.save(&e) {
                    logError("insert error: \(e!.localizedDescription)")
                    abort()
                }
                CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("Activity")
            }
            else{
                CDModelManager.sharedInstance.addPendingEntityToSave("Activity")
            }
            
        })
        return item
    }
    
    class func clean() {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("Activity", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [Activity] {
                for item in fetchedResults {
                    CDModelManager.context.deleteObject(item)
                }
                var e: NSError?
                if !CDModelManager.context.save(&e) {
                    logError("insert error: \(e!.localizedDescription)")
                    abort()
                }
                CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("Activity")
            }
        })
    }
    
    
    class func fetchRecentEntries(completion: ([AnyObject]?) -> Void, limit: Int = 50) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("Activity", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            fetchRequest.fetchLimit = limit
            let sortDescriptor = NSSortDescriptor(key: "endTime", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = [sortDescriptor]
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [Activity] {
                completion(fetchedResults)
                return
            }
            completion(nil)
        })
    }
    
    class func fetchRecentMergeEntries(completion: ([AnyObject]?) -> Void, limit: Int = 50) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("Activity", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            fetchRequest.fetchLimit = limit
            let args = [ActivityTypes.DUPLICATE_SCAN, ActivityTypes.MERGE_DUPLICATES] as NSArray
            let predicate = NSPredicate(format: "type IN %@", args)
            fetchRequest.predicate = predicate
            
            let sortDescriptor = NSSortDescriptor(key: "endTime", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = [sortDescriptor]
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [Activity] {
                completion(fetchedResults)
                return
            }
            completion(nil)
        })
    }
    
    class func fetchRecentDummyEntries(completion: ([AnyObject]?) -> Void, limit: Int = 50) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("Activity", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            fetchRequest.fetchLimit = limit
            let args = [ActivityTypes.DUMMIES_SCAN, ActivityTypes.CLEAN_DUMMIES] as NSArray
            let predicate = NSPredicate(format: "type IN %@", args)
            fetchRequest.predicate = predicate
            
            let sortDescriptor = NSSortDescriptor(key: "endTime", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = [sortDescriptor]
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [Activity] {
                completion(fetchedResults)
                return
            }
            completion(nil)
        })
    }
}

extension CleanItem {
    class func addEntry(name: String, abRecordID: ABRecordID, vCardPath: String, parentActivity: Activity,
        cleanDate: NSDate, imagePath: String, save:Bool = true) {
            //        var item:CleanItem?
            CDModelManager.getQueue().waitForOperationWithBlock({
                let item = NSEntityDescription.insertNewObjectForEntityForName("CleanItem", inManagedObjectContext: CDModelManager.context) as CleanItem
                item.name = name
                item.recordID = NSNumber(int: abRecordID)
                item.vCardPath = vCardPath
                item.parentActivity = parentActivity
                item.cleanDate = cleanDate
                item.imagePath = imagePath
                if save{
                    var e: NSError?
                    if !CDModelManager.context.save(&e) {
                        logError("insert error: \(e!.localizedDescription)")
                        abort()
                    }
                    CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("CleanItem")
                }else {
                    CDModelManager.sharedInstance.addPendingEntityToSave("CleanItem")
                }
            })
            //        return item
    }
    
    class func clean() {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("CleanItem", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [CleanItem] {
                for item in fetchedResults {
                    CDModelManager.context.deleteObject(item)
                }
                var e: NSError?
                if !CDModelManager.context.save(&e) {
                    logError("insert error: \(e!.localizedDescription)")
                    abort()
                }
                CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("CleanItem")
            }
        })
    }
    
}

extension MergeItem {
    
    class func addEntry(parentActivity: Activity, mergeDate: NSDate, dcName: String, dcRecordID: ABRecordID, dcVCardPath: String, dcImagePath: String, tcName: String, tcRecordID: ABRecordID, tcVCardPath: String, tcImagePath: String, save:Bool = true) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let item = NSEntityDescription.insertNewObjectForEntityForName("MergeItem", inManagedObjectContext: CDModelManager.context) as MergeItem
            item.parentActivity = parentActivity
            item.mergeDate = mergeDate
            
            item.dcName = dcName
            item.dcRecordID = NSNumber(int: dcRecordID)
            item.dcVCardPath = dcVCardPath
            item.dcImagePath = dcImagePath
            
            item.tcName = tcName
            item.tcRecordID = NSNumber(int: tcRecordID)
            item.tcVCardPath = tcVCardPath
            item.tcImagePath = tcImagePath
            
            if save{
                var e: NSError?
                if !CDModelManager.context.save(&e) {
                    logError("insert error: \(e!.localizedDescription)")
                    abort()
                }
                CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("MergeItem")
            }
            else{
                CDModelManager.sharedInstance.addPendingEntityToSave("MergeItem")
            }
            
        })
    }
    
    class func clean() {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("MergeItem", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [MergeItem] {
                for item in fetchedResults {
                    CDModelManager.context.deleteObject(item)
                }
                var e: NSError?
                if !CDModelManager.context.save(&e) {
                    logError("insert error: \(e!.localizedDescription)")
                    abort()
                }
                CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("MergeItem")
            }
        })
    }
    
}

typealias FavoriteValueType = NSNumber
typealias FavoriteReasonType = NSNumber
typealias FavoriteOrderType = Int16

extension Favorite {
    //    override func didChangeValueForKey(key: String){
    //        logDebug("didChange \(key)")
    //        if key == "lastUpdated"{
    //            logDebug("lastUpdated changed")
    //            self.callLogsCount = self.callLogs.count
    //        }
    //    }
    func addCallLogItem(callLog: ContactLog) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            self.callLogsCount = self.callLogs.count + 1
            callLog.favoriteItem = self
        })
    }
    
    var localizedLabel: String {
        return ABAddressBookCopyLocalizedLabel(self.label).takeRetainedValue() as NSString
    }
    
    class func entryForValue(value: String, label: String, abRecordID: ABRecordID? = nil) -> Favorite? {
        var result: Favorite?
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("Favorite", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            if abRecordID == nil {
                fetchRequest.predicate = NSPredicate(format: "(value = %@) AND (label = %@)  ", value, label)
            } else {
                fetchRequest.predicate = NSPredicate(format: "(value = %@) AND (label = %@) AND (abRecordID = %d)", value, label, abRecordID!)
            }
            
            fetchRequest.fetchLimit = 1
            let sortDescriptor = NSSortDescriptor(key: "priority", ascending: false)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = [sortDescriptor]
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [Favorite] {
                if fetchedResults.count > 0 {
                    result = fetchedResults[0]
                }
            }
        })
        return result
        
    }
    
    class func addEntry(value: String, abRecordID: ABRecordID? = nil, label: String = kABOtherLabel, reason: FavoriteReasonType? = nil,
        priority: Int64 = 0, valueType: FavoriteValueType? = nil, order: FavoriteOrderType? = nil, save: Bool = true) -> Favorite? {
            var item: Favorite!
            CDModelManager.getQueue().waitForOperationWithBlock({
                if let favorite = Favorite.entryForValue(value, label: label, abRecordID: abRecordID) {
                    item = favorite
                    if reason != nil && reason == FavoriteReasons.Added {
                        item.reason = reason!
                    }
                    if valueType != nil {
                        item.valueType = valueType!
                    }
                    
                } else {
                    item = NSEntityDescription.insertNewObjectForEntityForName("Favorite", inManagedObjectContext: CDModelManager.context) as Favorite
                    item.isContact = false
                    if reason != nil {
                        item.reason = reason!
                    } else {
                        item.reason = FavoriteReasons.Added
                    }
                    item.creationDate = NSDate()
                    if valueType != nil {
                        item.valueType = valueType!
                    } else {
                        item.valueType = CLConstants.kTypePhoneNumber
                    }
                }
                
                item.value = value
                if abRecordID != nil {
                    item.abRecordID = NSNumber(int: abRecordID!)
                    item.isContact = true
                }
                
                if priority != 0 {
                    item.priority = NSNumber(longLong: priority)
                } else {
                    //Calculate Priority
                }
                
                item.label = label
                
                if order != nil {
                    item.order = NSNumber(short: order!)
                }
                item.lastUpdated = NSDate()
                if save {
                    var e: NSError?
                    if !CDModelManager.context.save(&e) {
                        logError("insert error: \(e!.localizedDescription)")
                        abort()
                    }
                    CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("Favorite")
                } else {
                    CDModelManager.sharedInstance.addPendingEntityToSave("Favorite")
                }
            })
            return item
    }
    
    class func deleteEntry(entry: Favorite) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            CDModelManager.context.deleteObject(entry)
            var e: NSError?
            if !CDModelManager.context.save(&e) {
                logError("insert error: \(e!.localizedDescription)")
                abort()
            }
            CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("Favorite")
        })
        
    }
    
    class func clean() {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("Favorite", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            let predicate = NSPredicate(format: "not reason in %@", [FavoriteReasons.Added, FavoriteReasons.Contents])
            fetchRequest.predicate = predicate
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [Favorite] {
                for favorite in fetchedResults {
                    logDebug("deleting \(favorite.description)")
                    CDModelManager.context.deleteObject(favorite)
                }
                var e: NSError?
                if !CDModelManager.context.save(&e) {
                    logError("insert error: \(e!.localizedDescription)")
                    abort()
                }
                CDModelManager.sharedInstance.notifyCoreDataChangeForEnity("Favorite")
            }
        })
    }
    
    class func fetchAllEntries(completion: ([AnyObject]?) -> Void, limit: Int = 20) {
        CDModelManager.getQueue().waitForOperationWithBlock({
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("Favorite", inManagedObjectContext: CDModelManager.context)
            fetchRequest.entity = entity
            fetchRequest.fetchLimit = limit
            let predicate = NSPredicate(format: "reason != %@", FavoriteReasons.Recent)
            fetchRequest.predicate = predicate
            let sortDescriptor1 = NSSortDescriptor(key: "reason", ascending: true)
            let sortDescriptor2 = NSSortDescriptor(key: "callLogsCount", ascending: false)
            let sortDescriptor3 = NSSortDescriptor(key: "priority", ascending: false)
            let sortDescriptor4 = NSSortDescriptor(key: "lastUpdated", ascending: false)
            
            fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2, sortDescriptor3, sortDescriptor4]//1st one has highest priority
            if let fetchedResults = CDModelManager.context.executeFetchRequest(fetchRequest, error: nil) as? [Favorite] {
                completion(fetchedResults)
                return
            }
            completion(nil)
        })
    }
}
