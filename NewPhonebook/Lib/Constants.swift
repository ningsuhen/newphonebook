/** AppStore App ID used by Appirater **/
let kAppID = "962339241"

let kDefaultLabel = NSLocalizedString("default", comment: "Default label in Address Book")
let kCallPhoneNumberScheme = "tel://"
let kFBPermissions = ["public_profile", "email", "user_friends","user_location"]
let kScanDuplicateName = true

/*
Account Details
*/

//Facebook
let kFacebookScheme = "fb791423160905369"
//Dropbox
let kDropboxScheme = "db-npyxsdtx8l4cqnq"
//Google
let kKeychainItemName = "NewPhonebook Google Drive";
let kClientID = "824046695738-p8ia3ojfqjeo7fjl0tgfohr5hppos2ej.apps.googleusercontent.com";
let kClientSecret = "vXfDFix_0FVFBC2dr2LDvF3S";


let kSearchDefaultScopeLabel = "Default"
let kVCardMimeType = "text/vcard"
let kUnlinkedAccount = "Not Linked"
let kDefaultTint = UIColor(red: 0, green: (122 / 255), blue: 1, alpha: 1)
let kSkyBlueColor = UIColor(red: (0 / 255), green: (191 / 255), blue: (255 / 255), alpha: 1)

struct SCGroupTypes {
    static let RECENT: SCGroupType = 0
    static let TOP: SCGroupType = 1
    static let SEARCH: SCGroupType = 2
}

typealias SCGroupType = Int

struct ScanButtonStates {
    static let NORMAL = 0
    static let STARTED = 1
    static let STOPPED = 2
    static let FINISHED = 3
}

struct ContactsProcessingStatus {
    var total: Int
    var completed: Int
}

let kDefaultAvatarImage = UIImage(named: IconSet.DefaultAvatar)?.imageTintedWithColor(kSkyBlueColor)

struct StringConstants {
    static let MergeSettingsKey = "mergeSettings"
    static let SearchSettingsKey = "searchSettings"
    static let EmailKey = "email"
}

struct LocalizedSearchScopes {
    static let Default = NSLocalizedString("Default", comment: "")
    static let Name = NSLocalizedString("Name", comment: "")
    static let Phone = NSLocalizedString("Phone", comment: "")
    static let Job = NSLocalizedString("Job", comment: "")
    static let Address = NSLocalizedString("Address", comment: "")
    static let Social = NSLocalizedString("Social", comment: "")
}

struct IconSet {
    static let EmailFill48 = "email-fill-48"
    static let DefaultAvatar = "avatar-fill-48"
    static let EmptyContact = "empty-contact.png"
    static let Contacts = "bb-contacts-source"
    static let MergeLeft = "bb-merge-left"
    static let MergeRight = "bb-merge-right"
    static let SavedSearch = "bb-search"
    static let DuplicateGloss = "bb-duplicate"
    static let MergeGloss = "bb-merge"
    static let MergeGloss64 = "merge-64"
    static let EmptyGloss = "bb-empty"
    static let CleanGloss = "bb-clean"
    static let RemoveGloss = "bb-trash"
    static let ScanGloss64 = "scan-64"
    static let RemoveGloss64 = "remove-64"
    static let CleanGloss64 = "clean-64"
    static let CheckboxTicked = "bb-checkbox-checked"
    static let Checkbox = "bb-checkbox"
}


struct RestoreTypes {
    static let PHONE = 0
    static let URL = 1
    static let ICLOUD = 2
    static let DROPBOX = 3
    static let GOOGLEDRIVE = 4
}

struct BackupOptions {
    static let PHONE = 0
    static let EMAIL = 1
    static let ICLOUD = 2
    static let DROPBOX = 3
    static let GOOGLEDRIVE = 4
}

struct RestoreChoice {
    var title: String
    var description: String
    var type: Int
    var data: AnyObject?
}

