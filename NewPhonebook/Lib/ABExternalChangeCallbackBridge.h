#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface ABExternalChangeCallbackBridge : NSObject {
    ABAddressBookRef addressBook;
}

+ (ABExternalChangeCallbackBridge *)sharedInstance;


void MyAddressBookExternalChangeCallback(
        ABAddressBookRef addressBook,
        CFDictionaryRef info,
        void *context
);

@end