//
//  CustomQueue.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/17/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation

class CustomQueue: NSObject {
    var label: String!

    lazy var internalQueue: dispatch_queue_t = {
        return dispatch_queue_create(self.label, DISPATCH_QUEUE_SERIAL)
    }()
    init(_ label: String!) {
        self.label = label

//        super.init()
    }

    func waitForOperationWithBlock(block: () -> Void) {
        if dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL) == dispatch_queue_get_label(self.internalQueue) {
            //Since we are executing in the current thread, we don't need autorelease
            block()
        } else {
            //first block of the the thread should be autorelease
            let wrappedBlock = {
                autoreleasepool({
                    block()
                })
            }
            dispatch_sync(self.internalQueue, wrappedBlock)
        }
    }

    func addOperation(operation: NSOperation) {
        var wrappedBlock = {
            autoreleasepool({
                operation.main()
                if operation.completionBlock != nil {
                    dispatch_async(dispatch_get_main_queue(), operation.completionBlock!)
                }
            })
        }
        dispatch_async(internalQueue, wrappedBlock)
    }


    func addOperationWithBlock(block: () -> Void) {
        dispatch_async(internalQueue, block)
    }

    func addOperationWithBlock(block: () -> Void, completion: (() -> Void)? = nil) {
        var wrappedBlock = {
            autoreleasepool({
                block()
                if completion != nil {
                    dispatch_async(dispatch_get_main_queue(), completion!)
                }
            })
        }
        dispatch_async(internalQueue, wrappedBlock)
    }
}