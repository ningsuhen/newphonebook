import Foundation
import AddressBook
import CoreData

class VCardDownloader: NSObject {
    var data: NSMutableData?
    var downloadSize: Int64 = 0
    var progressBlock: ((progress:Float) -> Void)?
    var completionBlock: ((data:NSData?) -> Void)?
    var restoreUrl: NSURL?
    
    func main() {
        autoreleasepool({
            self.startDownload()
        })
    }
    
    func startDownload() {
        data = NSMutableData()
        if restoreUrl != nil {
            var request: NSURLRequest = NSURLRequest(URL: restoreUrl!)
            var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: true)!
            connection.start()
            
        } else {
            logError("invalid restore url")
        }
    }
    
    func connection(connection: NSURLConnection!, didReceiveResponse response: NSHTTPURLResponse!) {
        let statusCode_ = response.statusCode
        if statusCode_ == 200 {
            downloadSize = response.expectedContentLength
        } else {
            if completionBlock != nil {
                completionBlock!(data: nil)
            }
            logError("status code not 200 \(statusCode_)")
        }
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data?.appendData(data)
        if progressBlock != nil {
            if downloadSize != 0 {
                let progress = Float(Double(self.data!.length) / Double(downloadSize))
                progressBlock!(progress: progress)
            }
            
        }
    }
    func connection(connection: NSURLConnection, didFailWithError error: NSError){
        if completionBlock != nil {
            completionBlock!(data: nil)
        }
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        if completionBlock != nil {
            completionBlock!(data: data!)
        }
    }
}

class SmartFavoritesOperation: NSOperation {
    var contacts: [SwiftAddressBookPerson]!
    var sortedContacts: [SwiftAddressBookPerson]!
    override func main() {
        autoreleasepool({
            self.iterateContacts()
            logDebug("SmartFavoritesOperation completed")
            self.contacts = nil
            self.sortedContacts = nil
        })
    }
    
    deinit {
        logDebug("SmartFavoritesOperation deinit")
    }
    override init() {
        super.init()
        logDebug("SmartFavoritesOperation init")
    }
    
    func iterateContacts() {
        autoreleasepool({
            logDebug("Entered iterateContacts")
            //iterate through contacts - once every 7 days
            if self.contacts == nil {
                return
            }
            
            let userLocations = Preferences.sharedInstance.userInfo?.getLocations()
            
            for var index = 0; index < self.contacts.count; index++ {
                let person = self.contacts[index]
                if let firstName = person.firstName {
                    if firstName == "Identified As Spam" || person.phoneNumbers?.count > 20 {
                        //truecaller spam contact. Skip
                        continue
                    }
                }
                var infoCount = 0
                let personRecordID = person.getRecordID()
                
                if person.hasImage {
                    infoCount += 50
                }
                
                //Don't consider name as useful information.
                person.checkStringProperty(kABPersonFirstNameProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonLastNameProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonMiddleNameProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonPrefixProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonSuffixProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonNicknameProperty) {
                    infoCount += 5
                }
                person.checkStringProperty(kABPersonFirstNamePhoneticProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonLastNamePhoneticProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonMiddleNamePhoneticProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonOrganizationProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonJobTitleProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonDepartmentProperty) {
                    infoCount += 2
                }
                person.checkMultiStringProperty(kABPersonEmailProperty) {
                    infoCount += 2
                }
                person.checkMultiDateTimeProperty(kABPersonBirthdayProperty) {
                    infoCount += 5
                }
                person.checkStringProperty(kABPersonNoteProperty) {
                    infoCount += 2
                }
                
                //                scanMultiDateTime(kABPersonCreationDateProperty)
                //                scanMultiDateTime(kABPersonModificationDateProperty)
                if userLocations == nil {
                    person.checkMultiDictionaryProperty(kABPersonAddressProperty) {
                        infoCount += 3
                    }
                } else {
                    person.checkMultiDictionaryProperty(kABPersonAddressProperty, callback: {
                        },
                        filterCallback: {
                            (label: String, value: NSDictionary?) in
                            if let addressDictionary = value {
                                if let street = addressDictionary.objectForKey(kABPersonAddressStreetKey) as? String {
                                    for location in userLocations! {
                                        if street.rangeOfString(location, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
                                            infoCount += 50
                                        }
                                    }
                                }
                                if let city = addressDictionary.objectForKey(kABPersonAddressCityKey) as? String {
                                    for location in userLocations! {
                                        if city.rangeOfString(location, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
                                            infoCount += 40
                                        }
                                    }
                                }
                                if let state = addressDictionary.objectForKey(kABPersonAddressStateKey) as? String {
                                    for location in userLocations! {
                                        if state.rangeOfString(location, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
                                            infoCount += 20
                                        }
                                    }
                                }
                                if let country = addressDictionary.objectForKey(kABPersonAddressCountryKey) as? String {
                                    for location in userLocations! {
                                        if country.rangeOfString(location, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
                                            infoCount += 10
                                        }
                                    }
                                }
                            }
                        }
                    )
                }
                
                person.checkMultiDateTimeProperty(kABPersonDateProperty) {
                    infoCount += 1
                }
                //                scanPersonalInfo(kABPersonAnniversaryLabel)
                
                person.checkMultiStringProperty(kABPersonPhoneProperty) {
                    infoCount += 10
                }
                person.checkMultiDictionaryProperty(kABPersonInstantMessageProperty) {
                    infoCount += 3
                }
                person.checkMultiDictionaryProperty(kABPersonSocialProfileProperty, callback: {
                    },
                    filterCallback: {
                        (label: String, value: NSDictionary?) in
                        if label != "widgets" {
                            if value != nil {
                                let v: Dictionary = value! as Dictionary
                                if let service = v[kABPersonSocialProfileServiceKey] as? String {
                                    if service != "widgets" {
                                        if v[kABPersonSocialProfileURLKey] != nil || v[kABPersonSocialProfileUsernameKey] != nil || v[kABPersonSocialProfileUserIdentifierKey] != nil {
                                            infoCount += 5
                                        }
                                    }
                                }
                            }
                            
                        }
                })
                person.checkMultiStringProperty(kABPersonURLProperty) {
                    infoCount += 5
                }
                person.checkMultiStringProperty(kABPersonRelatedNamesProperty) {
                    infoCount += 1
                }
                
                person.checkPropertyExists(kABPersonAlternateBirthdayProperty) {
                    infoCount += 1
                }
                person.infoCount = infoCount
                person.clearCache()
                if self.cancelled {
                    break
                }
            }
            autoreleasepool({
                if !self.cancelled {
                    if self.sortedContacts == nil {
                        self.sortedContacts = sorted(self.contacts, {
                            (p1: SwiftAddressBookPerson, p2: SwiftAddressBookPerson) -> Bool in
                            return p1.infoCount > p2.infoCount
                        })
                    }
                    
                    var endIndex = self.sortedContacts.count - 1
                    if endIndex < 0 {
                        return //if array is empty
                    }
                    if endIndex > 20 {
                        endIndex = 20
                    }
                    
                    let top10 = self.sortedContacts[0 ... endIndex]
                    for contact in top10 {
                        if let phoneNumbers: [MultivalueEntry] = contact.phoneNumbers {
                            var value = phoneNumbers[0].value
                            var label = phoneNumbers[0].label
                            if phoneNumbers.count > 1 {
                                for phoneNumber in phoneNumbers {
                                    if phoneNumber.label == kDefaultLabel {
                                        value = phoneNumber.value
                                        label = phoneNumber.label
                                        break
                                    }
                                }
                            }
                            Favorite.addEntry(value, abRecordID: contact.getRecordID(), label: label, reason: FavoriteReasons.Contents, priority: Int64(contact.infoCount), save: false)
                        }
                        if self.cancelled {
                            break
                        }
                    }
                    if !self.cancelled {
                        CDModelManager.sharedInstance.commit()
                    }
                    //iterate through call logs - everytime
                }
                logDebug("exiting iterateContacts")
            })
            
        })
    }
}