//SwiftAddressBook - A strong-typed Swift Wrapper for ABAddressBook
//Copyright (C) 2014  Socialbit GmbH
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see http://www.gnu.org/licenses/ .
//If you would to like license this software for non-free commercial use,
//please write us at kontakt@socialbit.de .

import UIKit
import AddressBook

extension NSOperationQueue {
    func waitForOperationWithBlock(block: () -> Void) {
        if NSOperationQueue.currentQueue() == self {
            block()
        } else {
            let wrappedBlock = {
                autoreleasepool({
                    block()
                })
            }
            addOperations([NSBlockOperation(wrappedBlock)], waitUntilFinished: true)
        }
    }
}

let phoneNumberRegex = NSRegularExpression(pattern: "[-\\s\\(\\)]", options: NSRegularExpressionOptions(0), error: nil)
let socialProfileUrlRegex = NSRegularExpression(pattern: "((profiles|plus|www)\\.)?((google|facebook|klout|twitter|gravatar|foursquare|linkedin)\\.com)(\\/(profiles|user))?\\/[^\\/]+", options: NSRegularExpressionOptions(0), error: nil)

//MARK: Address Book





let queue = CustomQueue("Swift Address Book Queue")


class SwiftAddressBook {
    private var internalAddressBook: ABAddressBook!
    
    var linkedToIgnore: Dictionary<ABRecordID, ABRecordID>!
    var recordMap: Dictionary<ABRecordID, SwiftAddressBookPerson>?
    var swiftRecords = [SwiftAddressBookPerson]()
    
    init?() {
        queue.waitForOperationWithBlock({
            var err: Unmanaged<CFError>? = nil
            let ab = ABAddressBookCreateWithOptions(nil, &err)
            
            if err == nil {
                self.internalAddressBook = ab.takeRetainedValue()
            } else {
                
                logDebug("error: \(err?.takeRetainedValue())")
                logDebug("error creating addressbook")
            }
        })
        if internalAddressBook == nil {
            return nil
        }
    }
    
    func addBlock(block: () -> Void, waitUntilFinished: Bool = false, completion: (() -> Void)? = nil) {
        if waitUntilFinished {
            queue.waitForOperationWithBlock(block)
            if completion != nil {
                completion!()
            }
        } else {
            queue.addOperationWithBlock(block, completion: completion)
        }
    }
    
    func performAddressBookBlock(block: (internalAddressBook:ABAddressBook) -> Void) {
        queue.waitForOperationWithBlock({
            block(internalAddressBook: self.internalAddressBook)
        })
    }
    
    
    class func authorizationStatus() -> ABAuthorizationStatus {
        return ABAddressBookGetAuthorizationStatus()
    }
    
    func requestAccessWithCompletion(completion: (Bool, CFError?) -> Void) {
        queue.waitForOperationWithBlock({
            ABAddressBookRequestAccessWithCompletion(self.internalAddressBook) {
                (let b: Bool, c: CFError!) -> Void in completion(b, c)
            }
        })
    }
    
    func hasUnsavedChanges() -> Bool {
        var hasChanges: Bool = false
        queue.waitForOperationWithBlock({
            hasChanges = ABAddressBookHasUnsavedChanges(self.internalAddressBook)
        })
        return hasChanges
    }
    
    func save() -> CFError? {
        var result: CFError?
        queue.waitForOperationWithBlock({
            result = errorIfNoSuccess {
                ABAddressBookSave(self.internalAddressBook, $0)
            }
        })
        return result
    }
    
    func revert() {
        queue.waitForOperationWithBlock({
            ABAddressBookRevert(self.internalAddressBook)
        })
    }
    
    func addRecord(record: SwiftAddressBookRecord) -> CFError? {
        var result: CFError?
        queue.waitForOperationWithBlock({
            result = errorIfNoSuccess {
                ABAddressBookAddRecord(self.internalAddressBook, record.internalRecord, $0)
            }
        })
        return result
    }
    
    func removeRecord(record: SwiftAddressBookRecord) -> CFError? {
        var result: CFError?
        queue.waitForOperationWithBlock({
            result = errorIfNoSuccess {
                ABAddressBookRemoveRecord(self.internalAddressBook, record.internalRecord, $0)
            }
        })
        return result
    }
    
    //    //This function does not yet work
    //    func registerExternalChangeCallback(callback: (AnyObject) -> Void) {
    //        //call some objective C function (c function pointer does not work in swift)
    //    }
    //
    //    //This function does not yet work
    //    func unregisterExternalChangeCallback(callback: (AnyObject) -> Void) {
    //        //call some objective C function (c function pointer does not work in swift)
    //    }
    
    
    //MARK: person records
    
    var personCount: Int {
        get {
            var count: Int = 0
            queue.waitForOperationWithBlock({
                count = ABAddressBookGetPersonCount(self.internalAddressBook)
            })
            return count
        }
    }
    
    func personWithRecordId(recordId: ABRecordID) -> SwiftAddressBookPerson? {
        var person: SwiftAddressBookPerson?
        queue.waitForOperationWithBlock({
            if let record: ABRecord = ABAddressBookGetPersonWithRecordID(self.internalAddressBook, recordId)?.takeUnretainedValue() {
                person = SwiftAddressBookRecord(record: record).convertToPerson()
            }
            
        })
        return person
    }
    
    func personFromRecord(record: ABRecord) -> SwiftAddressBookPerson? {
        var recordId: ABRecordID!
        queue.waitForOperationWithBlock({
            recordId = ABRecordGetRecordID(record)
        })
        return personFromRecordId(recordId)
    }
    
    func personFromRecordId(recordId: ABRecordID) -> SwiftAddressBookPerson? {
        if let person = recordMap?[recordId] {
            return person
        }
        return personWithRecordId(recordId)
    }
    
    var allPeople: [SwiftAddressBookPerson]? {
        get {
            var people: [SwiftAddressBookPerson]?
            queue.waitForOperationWithBlock({
                people = convertRecordsToPersons(ABAddressBookCopyArrayOfAllPeople(self.internalAddressBook).takeRetainedValue())
            })
            return people
        }
    }
    lazy var abDispatchQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Swift Address Book Dispatch Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
        }()
    /**
    *  Simple operation which is used for dispatching people operations
    */
    
    class PeopleOperation: NSOperation {
        weak var swiftAddressBook: SwiftAddressBook!
        override func main() {
            if self.cancelled {
                logDebug("cancelling PeopleOperation")
                return
            }
            autoreleasepool({
                queue.waitForOperationWithBlock({
                    var swiftRecords = [SwiftAddressBookPerson]()
                    var recordMap = Dictionary<ABRecordID, SwiftAddressBookPerson>()
                    var linkedToIgnore = Dictionary<ABRecordID, ABRecordID>()
                    
                    for record in ABAddressBookCopyArrayOfAllPeople(self.swiftAddressBook.internalAddressBook).takeRetainedValue() as [ABRecord] {
                        if self.cancelled {
                            logDebug("cancelling PeopleOperation")
                            return
                        }
                        let recordID = ABRecordGetRecordID(record)
                        let person = SwiftAddressBookRecord(record: record).convertToPerson()! as SwiftAddressBookPerson
                        
                        if let targetLink = linkedToIgnore[recordID] {
                            person.linkedTo = targetLink
                        } else {
                            if let linkedPeople = person.allLinkedPeople {
                                person.linkedPeople = linkedPeople
                                for linkedPerson in person.linkedPeople! {
                                    linkedToIgnore[linkedPerson.getRecordID()] = recordID
                                }
                            }
                        }
                        recordMap[recordID] = person
                        swiftRecords.append(person)
                    }
                    self.swiftAddressBook.linkedToIgnore = linkedToIgnore
                    self.swiftAddressBook.recordMap = recordMap
                    self.swiftAddressBook.swiftRecords = swiftRecords
                })
            })
        }
    }
    
    func getAllUniquePersons(completion: ((persons:[SwiftAddressBookPerson]) -> Void)?) {
        abDispatchQueue.cancelAllOperations()
        abDispatchQueue.waitUntilAllOperationsAreFinished()
        let op = PeopleOperation()
        op.swiftAddressBook = self
        if completion != nil {
            op.completionBlock = {
                [weak op] in
                if !op!.cancelled {
                    completion!(persons: self.swiftRecords)
                }
            }
        }
        abDispatchQueue.addOperation(op)
    }
    var allUniquePeople: [SwiftAddressBookPerson]? {
        abDispatchQueue.cancelAllOperations()
        abDispatchQueue.waitUntilAllOperationsAreFinished()
        let op = PeopleOperation()
        abDispatchQueue.addOperations([op], waitUntilFinished: true)
        return self.swiftRecords
    }
    var allRecords: [ABRecord]? {
        get {
            var records: [ABRecord]?
            queue.waitForOperationWithBlock({
                autoreleasepool {
                    records = ABAddressBookCopyArrayOfAllPeople(self.internalAddressBook).takeRetainedValue()
                }
            })
            return records
        }
    }
    
    func allPeopleInSource(source: SwiftAddressBookSource) -> [SwiftAddressBookPerson]? {
        var persons: [SwiftAddressBookPerson]?
        queue.waitForOperationWithBlock({
            persons = convertRecordsToPersons(ABAddressBookCopyArrayOfAllPeopleInSource(self.internalAddressBook, source.internalRecord).takeRetainedValue())
        })
        return persons
    }
    
    func allPeopleInSourceWithSortOrdering(source: SwiftAddressBookSource, ordering: SwiftAddressBookOrdering) -> [SwiftAddressBookPerson]? {
        var persons: [SwiftAddressBookPerson]?
        queue.waitForOperationWithBlock({
            persons = convertRecordsToPersons(ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(self.internalAddressBook, source.internalRecord, ordering.abPersonSortOrderingValue).takeRetainedValue())
        })
        return persons
    }
    
    func peopleWithName(name: String) -> [SwiftAddressBookPerson]? {
        let string: CFString = name as CFString
        var persons: [SwiftAddressBookPerson]?
        queue.waitForOperationWithBlock({
            persons = convertRecordsToPersons(ABAddressBookCopyPeopleWithName(self.internalAddressBook, string).takeRetainedValue())
        })
        return persons
    }
    
    
    //MARK: group records
    
    func groupWithRecordId(recordId: Int32) -> SwiftAddressBookGroup? {
        var group: SwiftAddressBookGroup?
        queue.waitForOperationWithBlock({
            group = SwiftAddressBookRecord(record: ABAddressBookGetGroupWithRecordID(self.internalAddressBook, recordId).takeUnretainedValue()).convertToGroup()
        })
        return group
    }
    
    var groupCount: Int {
        get {
            var count: Int = 0
            queue.waitForOperationWithBlock({
                count = ABAddressBookGetGroupCount(self.internalAddressBook)
            })
            return count
        }
    }
    
    var arrayOfAllGroups: [SwiftAddressBookGroup]? {
        get {
            var groups: [SwiftAddressBookGroup]?
            queue.waitForOperationWithBlock({
                groups = convertRecordsToGroups(ABAddressBookCopyArrayOfAllGroups(self.internalAddressBook).takeRetainedValue())
            })
            return groups
        }
    }
    
    func allGroupsInSource(source: SwiftAddressBookSource) -> [SwiftAddressBookGroup]? {
        var groups: [SwiftAddressBookGroup]?
        queue.waitForOperationWithBlock({
            groups = convertRecordsToGroups(ABAddressBookCopyArrayOfAllGroupsInSource(self.internalAddressBook, source.internalRecord).takeRetainedValue())
        })
        return groups
    }
    
    
    //MARK: sources
    
    var defaultSource: SwiftAddressBookSource? {
        get {
            var source: SwiftAddressBookSource?
            queue.waitForOperationWithBlock({
                source = SwiftAddressBookSource(record: ABAddressBookCopyDefaultSource(self.internalAddressBook).takeRetainedValue())
            })
            return source
        }
    }
    
    func sourceWithRecordId(sourceId: Int32) -> SwiftAddressBookSource? {
        var source: SwiftAddressBookSource?
        queue.waitForOperationWithBlock({
            source = SwiftAddressBookSource(record: ABAddressBookGetSourceWithRecordID(self.internalAddressBook, sourceId).takeUnretainedValue())
        })
        return source
    }
    
    var allSources: [SwiftAddressBookSource]? {
        get {
            var sources: [SwiftAddressBookSource]?
            queue.waitForOperationWithBlock({
                sources = convertRecordsToSources(ABAddressBookCopyArrayOfAllSources(self.internalAddressBook).takeRetainedValue())
            })
            return sources
        }
    }
    
}


//MARK: Wrapper for ABAddressBookRecord

class SwiftAddressBookRecord: NSObject {
    
    private var internalRecord: ABRecord
    
    init(record: ABRecord) {
        internalRecord = record
    }
    
    func performRecordBlock(block: (internalRecord:ABRecord) -> Void) {
        queue.waitForOperationWithBlock({
            block(internalRecord: self.internalRecord)
        })
    }
    
    func getRecordID() -> ABRecordID {
        if recordID != nil {
            return recordID!
        }
        queue.waitForOperationWithBlock({
            self.recordID = ABRecordGetRecordID(self.internalRecord)
        })
        return recordID!
    }
    var recordID: ABRecordID?
    
    
    func getVCard() -> NSData {
        var value: NSData!
        queue.waitForOperationWithBlock({
            value = ABPersonCreateVCardRepresentationWithPeople([self.internalRecord]).takeRetainedValue() as NSData
        })
        return value
    }
    
    private func convertToSource() -> SwiftAddressBookSource? {
        
        if ABRecordGetRecordType(self.internalRecord) == UInt32(kABSourceType) {
            let source = SwiftAddressBookSource(record: internalRecord)
            return source
        } else {
            return nil
        }
    }
    
    private func convertToGroup() -> SwiftAddressBookGroup? {
        if ABRecordGetRecordType(self.internalRecord) == UInt32(kABGroupType) {
            let group = SwiftAddressBookGroup(record: internalRecord)
            return group
        } else {
            return nil
        }
    }
    
    private func convertToPerson() -> SwiftAddressBookPerson? {
        if ABRecordGetRecordType(self.internalRecord) == UInt32(kABPersonType) {
            let person = SwiftAddressBookPerson(record: internalRecord)
            return person
        } else {
            return nil
        }
    }
}


//MARK: Wrapper for ABAddressBookRecord of type ABSource

class SwiftAddressBookSource: SwiftAddressBookRecord {
    
    var sourceType: SwiftAddressBookSourceType {
        get {
            var value: SwiftAddressBookSourceType!
            queue.waitForOperationWithBlock({
                let sourceType: CFNumber = ABRecordCopyValue(self.internalRecord, kABSourceTypeProperty).takeRetainedValue() as CFNumber
                var rawSourceType: Int32? = nil
                CFNumberGetValue(sourceType, CFNumberGetType(sourceType), &rawSourceType)
                value = SwiftAddressBookSourceType(abSourceType: rawSourceType!)
            })
            return value
        }
    }
    
    var searchable: Bool {
        get {
            var result: Bool!
            queue.waitForOperationWithBlock({
                let sourceType: CFNumber = ABRecordCopyValue(self.internalRecord, kABSourceTypeProperty).takeRetainedValue() as CFNumber
                var rawSourceType: Int32? = nil
                CFNumberGetValue(sourceType, CFNumberGetType(sourceType), &rawSourceType)
                let andResult = kABSourceTypeSearchableMask & rawSourceType!
                result = andResult != 0
            })
            return result
        }
    }
    
    var sourceName: String {
        var sourceName: String!
        queue.waitForOperationWithBlock({
            sourceName = ABRecordCopyValue(self.internalRecord, kABSourceNameProperty).takeRetainedValue() as CFString
        })
        return sourceName
    }
}


//MARK: Wrapper for ABAddressBookRecord of type ABGroup

class SwiftAddressBookGroup: SwiftAddressBookRecord {
    
    var name: String {
        get {
            var name: String!
            queue.waitForOperationWithBlock({
                name = ABRecordCopyValue(self.internalRecord, kABGroupNameProperty).takeRetainedValue() as CFString
            })
            return name
        }
    }
    
    class func create() -> SwiftAddressBookGroup {
        return SwiftAddressBookGroup(record: ABGroupCreate().takeRetainedValue())
    }
    
    class func createInSource(source: SwiftAddressBookSource) -> SwiftAddressBookGroup {
        var group: SwiftAddressBookGroup!
        queue.waitForOperationWithBlock({
            group = SwiftAddressBookGroup(record: ABGroupCreateInSource(source.internalRecord).takeRetainedValue())
        })
        return group
    }
    
    var allMembers: [SwiftAddressBookPerson]? {
        get {
            var members: [SwiftAddressBookPerson]?
            queue.waitForOperationWithBlock({
                members = convertRecordsToPersons(ABGroupCopyArrayOfAllMembers(self.internalRecord).takeRetainedValue())
            })
            return members
        }
    }
    
    func allMembersWithSortOrdering(ordering: SwiftAddressBookOrdering) -> [SwiftAddressBookPerson]? {
        var members: [SwiftAddressBookPerson]?
        queue.waitForOperationWithBlock({
            members = convertRecordsToPersons(ABGroupCopyArrayOfAllMembersWithSortOrdering(self.internalRecord, ordering.abPersonSortOrderingValue).takeRetainedValue())
        })
        return members
    }
    
    func addMember(person: SwiftAddressBookPerson) -> CFError? {
        var error: CFError?
        queue.waitForOperationWithBlock({
            error = errorIfNoSuccess {
                ABGroupAddMember(self.internalRecord, person.internalRecord, $0)
            }
        })
        return error
    }
    
    func removeMember(person: SwiftAddressBookPerson) -> CFError? {
        var error: CFError?
        queue.waitForOperationWithBlock({
            error = errorIfNoSuccess {
                ABGroupRemoveMember(self.internalRecord, person.internalRecord, $0)
            }
        })
        return error
    }
    
    var source: SwiftAddressBookSource {
        get {
            var source: SwiftAddressBookSource!
            queue.waitForOperationWithBlock({
                source = SwiftAddressBookSource(record: ABGroupCopySource(self.internalRecord).takeRetainedValue())
            })
            return source
        }
    }
}


//MARK: Wrapper for ABAddressBookRecord of type ABPerson

class SwiftAddressBookPerson: SwiftAddressBookRecord {
    var linkedTo: ABRecordID?
    var linkedPeople: [SwiftAddressBookPerson]?
    var infoCount: Int = 0
    var status = ""
    var cachedData: Dictionary<String, Any>?
    let enableCache = true
    func clearCache() {
        self.cachedData = nil
        self._image = nil
        self._fullName = nil
        self._hasImage = nil
        self._jobTitle = nil
        self._thumbnail = nil
        self._department = nil
        self._organization = nil
    }
    
    deinit {
        //        logDebug("SwiftAddressBookPerson deinit called")
    }
    
    class func create() -> SwiftAddressBookPerson {
        return SwiftAddressBookPerson(record: ABPersonCreate().takeRetainedValue())
    }
    
    class func createInSource(source: SwiftAddressBookSource) -> SwiftAddressBookPerson {
        var person: SwiftAddressBookPerson!
        queue.waitForOperationWithBlock({
            person = SwiftAddressBookPerson(record: ABPersonCreateInSource(source.internalRecord).takeRetainedValue())
        })
        return person
    }
    
    class func createInSourceWithVCard(source: SwiftAddressBookSource, vCard: String) -> [SwiftAddressBookPerson]? {
        var swiftPersons = [SwiftAddressBookPerson]()
        queue.waitForOperationWithBlock({
            let data: NSData? = vCard.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
            let abPersons: NSArray? = ABPersonCreatePeopleInSourceWithVCardRepresentation(source.internalRecord, data).takeRetainedValue()
            if let persons = abPersons {
                for person: ABRecord in persons {
                    let swiftPerson = SwiftAddressBookPerson(record: person)
                    swiftPersons.append(swiftPerson)
                }
            }
        })
        if swiftPersons.count != 0 {
            return swiftPersons
        } else {
            return nil
        }
    }
    
    class func createInSourceWithVCard(source: SwiftAddressBookSource, vCard: NSData) -> [SwiftAddressBookPerson]? {
        var swiftPersons = [SwiftAddressBookPerson]()
        queue.waitForOperationWithBlock({
            
            let abPersons: NSArray? = ABPersonCreatePeopleInSourceWithVCardRepresentation(source.internalRecord, vCard).takeRetainedValue()
            if let persons = abPersons {
                for person: ABRecord in persons {
                    let swiftPerson = SwiftAddressBookPerson(record: person)
                    swiftPersons.append(swiftPerson)
                }
            }
        })
        if swiftPersons.count != 0 {
            return swiftPersons
        } else {
            return nil
        }
    }
    
    class func createVCard(people: [SwiftAddressBookPerson]) -> String {
        var data: NSData!
        queue.waitForOperationWithBlock({
            let peopleArray: NSArray = people.map {
                $0.internalRecord
            }
            data = ABPersonCreateVCardRepresentationWithPeople(peopleArray).takeRetainedValue()
        })
        return NSString(data: data, encoding: NSUTF8StringEncoding)!
    }
    
    class func ordering() -> SwiftAddressBookOrdering {
        return SwiftAddressBookOrdering(ordering: ABPersonGetSortOrdering())
    }
    
    class func comparePeopleByName(person1: SwiftAddressBookPerson, person2: SwiftAddressBookPerson, ordering: SwiftAddressBookOrdering) -> CFComparisonResult {
        return ABPersonComparePeopleByName(person1, person2, ordering.abPersonSortOrderingValue)
    }
    
    
    
    
    //MARK: Personal Information
    
    
    func setImage(image: UIImage) -> CFError? {
        let imageData: NSData = UIImagePNGRepresentation(image)
        var error: CFError?
        queue.waitForOperationWithBlock({
            error = errorIfNoSuccess {
                ABPersonSetImageData(self.internalRecord, CFDataCreate(nil, UnsafePointer(imageData.bytes), imageData.length), $0)
            }
        })
        return error
    }
    
    
    private func cachedValue(key: String) -> Any? {
        if enableCache {
            return self.cachedData?[key]
        } else {
            return nil
        }
    }
    
    
    
    class Dummy {
        
    }
    
    private func storeInCache(key: String, _ value: Any?) -> Any? {
        if enableCache {
            objc_sync_enter(self)
            if self.cachedData == nil {
                self.cachedData = Dictionary<String, Any>()
            }
            if value != nil {
                self.cachedData![key] = value!
            } else {
                self.cachedData![key] = Dummy()
            }
            objc_sync_exit(self)
        }
        return nil
    }
    
    var _image: UIImage?
    var image: UIImage? {
        get {
            if let cached: Any = cachedValue("image") {
                return cached as? UIImage
            }
            var image: UIImage?
            queue.waitForOperationWithBlock({
                if let data = ABPersonCopyImageData(self.internalRecord) {
                    image = UIImage(data: data.takeRetainedValue())
                }
            })
            storeInCache("image", image)
            return image
        }
    }
    
    
    let cacheThumbnail = true
    var _thumbnail: UIImage?
    var thumbnail: UIImage? {
        get {
            if let cached: Any = cachedValue("thumbnail") {
                return cached as? UIImage
            }
            if let thumbnail = imageDataWithFormat(SwiftAddressBookPersonImageFormat.thumbnail) {
                storeInCache("thumbnail", thumbnail)
                return thumbnail
            }
            return nil
        }
    }
    
    var _hasImage: Bool?
    weak var weakQueue = queue
    var hasImage: Bool {
        if _hasImage != nil {
            return _hasImage!
        }
        self.performRecordBlock({
            (internalRecord: ABRecord) in
            self._hasImage = ABPersonHasImageData(internalRecord)
        })
        return _hasImage!
    }
    
    func hasMultiValueProperty(propertyName: ABPropertyID) -> Bool {
        var hasProperty = false
        queue.waitForOperationWithBlock({
            if let multivalue: ABMultiValue! = self.extractProperty(propertyName) {
                if ABMultiValueGetCount(multivalue) > 0 {
                    hasProperty = true
                }
            }
        })
        return hasProperty
    }
    
    func hasPhoneNumber() -> Bool {
        if let cached = cachedValue("hasPhoneNumber") {
            return cached as Bool
        }
        var hasPhoneNumber = hasMultiValueProperty(kABPersonPhoneProperty)
        storeInCache("hasPhoneNumber", hasPhoneNumber)
        return hasPhoneNumber
    }
    
    func imageDataWithFormat(format: SwiftAddressBookPersonImageFormat) -> UIImage? {
        var image: UIImage?
        queue.waitForOperationWithBlock({
            if let imageData = ABPersonCopyImageDataWithFormat(self.internalRecord, format.abPersonImageFormat)?.takeRetainedValue(){
                image = UIImage(data: imageData)
            }
        })
        return image
    }
    
    func hasImageData() -> Bool {
        var hasImageData: Bool = false
        queue.waitForOperationWithBlock({
            hasImageData = ABPersonHasImageData(self.internalRecord)
        })
        return hasImageData
    }
    
    func removeImage() -> CFError? {
        var error: CFError?
        queue.waitForOperationWithBlock({
            error = errorIfNoSuccess {
                ABPersonRemoveImageData(self.internalRecord, $0)
            }
        })
        return error
    }
    
    var allLinkedPeople: [SwiftAddressBookPerson]? {
        get {
            var linkedPeople: [SwiftAddressBookPerson]?
            queue.waitForOperationWithBlock({
                linkedPeople = convertRecordsToPersons(ABPersonCopyArrayOfAllLinkedPeople(self.internalRecord).takeRetainedValue() as CFArray)
            })
            return linkedPeople
        }
    }
    
    var source: SwiftAddressBookSource {
        get {
            var source: SwiftAddressBookSource!
            queue.waitForOperationWithBlock({
                source = SwiftAddressBookSource(record: ABPersonCopySource(self.internalRecord).takeRetainedValue())
            })
            return source
        }
    }
    
    var compositeNameDelimiterForRecord: String {
        get {
            var delimiter: String!
            queue.waitForOperationWithBlock({
                delimiter = ABPersonCopyCompositeNameDelimiterForRecord(self.internalRecord).takeRetainedValue()
            })
            return delimiter
        }
    }
    
    var compositeNameFormat: SwiftAddressBookCompositeNameFormat {
        get {
            var compositeNameFormat: SwiftAddressBookCompositeNameFormat!
            queue.waitForOperationWithBlock({
                compositeNameFormat = SwiftAddressBookCompositeNameFormat(format: ABPersonGetCompositeNameFormatForRecord(self.internalRecord))
            })
            return compositeNameFormat
        }
    }
    
    var firstName: String? {
        get {
            if let cached = cachedValue("firstName") {
                return cached as? String
            }
            var firstName: String?
            queue.waitForOperationWithBlock({
                firstName = self.extractProperty(kABPersonFirstNameProperty)
            })
            storeInCache("firstName", firstName)
            return firstName
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonFirstNameProperty, NSString(string: newValue))
            })
        }
    }
    
    var lastName: String? {
        get {
            if let cached = cachedValue("lastName") {
                return cached as? String
            }
            var lastName: String?
            queue.waitForOperationWithBlock({
                lastName = self.extractProperty(kABPersonLastNameProperty)
            })
            return lastName
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonLastNameProperty, NSString(string: newValue))
            })
        }
    }
    
    func getName() -> String {
        return getFullName()
    }
    var _fullName: String?
    func getFullName() -> String {
        if _fullName != nil {
            return _fullName!
        }
        queue.waitForOperationWithBlock({
            if let compositeName = ABRecordCopyCompositeName(self.internalRecord) {
                self._fullName = compositeName.takeRetainedValue()
            } else {
                self._fullName = ""
            }
        })
        //        storeInCache("fullName", fullName)
        
        return _fullName!
    }
    
    func threadUnSafeFullName() -> String {
        if _fullName != nil {
            return _fullName!
        }
        var fullName: String!
        if let compositeName = ABRecordCopyCompositeName(self.internalRecord) {
            self._fullName = compositeName.takeRetainedValue()
        } else {
            self._fullName = ""
        }
        //        storeInCache("fullName", fullName)
        return _fullName!
    }
    
    
    var fullNameForSearch: String {
        get {
            if _fullName != nil {
                return _fullName!
            }
            objc_sync_enter(self)
            queue.waitForOperationWithBlock({
                if let compositeName = ABRecordCopyCompositeName(self.internalRecord) {
                    self._fullName = compositeName.takeRetainedValue()
                } else {
                    self._fullName = ""
                }
            })
            objc_sync_exit(self)
            return _fullName!
        }
    }
    
    var middleName: String? {
        get {
            var middleName: String?
            queue.waitForOperationWithBlock({
                middleName = self.extractProperty(kABPersonMiddleNameProperty)
            })
            return middleName
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonMiddleNameProperty, NSString(string: newValue))
            })
        }
    }
    
    var prefix: String? {
        get {
            var prefix: String?
            queue.waitForOperationWithBlock({
                prefix = self.extractProperty(kABPersonPrefixProperty)
            })
            return prefix
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonPrefixProperty, NSString(string: newValue))
            })
        }
    }
    
    var suffix: String? {
        get {
            var suffix: String?
            queue.waitForOperationWithBlock({
                suffix = self.extractProperty(kABPersonSuffixProperty)
            })
            return suffix
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonSuffixProperty, NSString(string: newValue))
            })
        }
    }
    
    var nickname: String? {
        get {
            var nickName: String?
            queue.waitForOperationWithBlock({
                nickName = self.extractProperty(kABPersonNicknameProperty)
            })
            return nickName
        }
        set {
            
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonNicknameProperty, NSString(string: newValue))
            })
            
        }
    }
    
    var firstNamePhonetic: String? {
        get {
            var value: String?
            queue.waitForOperationWithBlock({
                value = self.extractProperty(kABPersonFirstNamePhoneticProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonFirstNamePhoneticProperty, NSString(string: newValue))
            })
        }
    }
    
    var lastNamePhonetic: String? {
        get {
            var value: String?
            queue.waitForOperationWithBlock({
                value = self.extractProperty(kABPersonLastNamePhoneticProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonLastNamePhoneticProperty, NSString(string: newValue))
            })
        }
    }
    
    var middleNamePhonetic: String? {
        get {
            var value: String?
            queue.waitForOperationWithBlock({
                value = self.extractProperty(kABPersonMiddleNamePhoneticProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonMiddleNamePhoneticProperty, NSString(string: newValue))
            })
        }
    }
    
    var _organization: String?
    var organization: String? {
        get {
            if _organization != nil {
                return _organization!
            }
            
            queue.waitForOperationWithBlock({
                self._organization = self.extractProperty(kABPersonOrganizationProperty)
            })
            return _organization
        }
        set {
            setSingleValueProperty(kABPersonOrganizationProperty, NSString(string: newValue))
        }
    }
    
    var _jobTitle: String?
    var jobTitle: String? {
        get {
            if _jobTitle != nil {
                return _jobTitle!
            }
            queue.waitForOperationWithBlock({
                self._jobTitle = self.extractProperty(kABPersonJobTitleProperty)
            })
            return _jobTitle
        }
        set {
            setSingleValueProperty(kABPersonJobTitleProperty, NSString(string: newValue))
        }
    }
    var _department: String?
    var department: String? {
        get {
            if _department != nil {
                return _department!
            }
            queue.waitForOperationWithBlock({
                self._department = self.extractProperty(kABPersonDepartmentProperty)
            })
            return _department
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonDepartmentProperty, NSString(string: newValue))
            })
        }
    }
    
    var emails: Array<MultivalueEntry<String>>? {
        get {
            if let cached = cachedValue("emails") {
                return cached as? Array<MultivalueEntry<String>>
            }
            var emails: Array<MultivalueEntry<String>>?
            queue.waitForOperationWithBlock({
                emails = self.extractMultivalueProperty(kABPersonEmailProperty)
            })
            storeInCache("emails", emails)
            return emails
        }
        set {
            queue.waitForOperationWithBlock({
                self.setMultivalueProperty(kABPersonEmailProperty, self.convertMultivalueEntries(newValue, converter: { NSString(string: $0) }))
            })
        }
    }
    
    var birthday: NSDate? {
        get {
            var value: NSDate?
            queue.waitForOperationWithBlock({
                value = self.extractProperty(kABPersonBirthdayProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonBirthdayProperty, newValue)
            })
        }
    }
    
    var note: String? {
        get {
            var value: String?
            queue.waitForOperationWithBlock({
                value = self.extractProperty(kABPersonNoteProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonNoteProperty, NSString(string: newValue))
            })
        }
    }
    
    var creationDate: NSDate? {
        get {
            var value: NSDate?
            queue.waitForOperationWithBlock({
                value = self.extractProperty(kABPersonCreationDateProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonCreationDateProperty, newValue)
            })
        }
    }
    
    var modificationDate: NSDate? {
        get {
            var value: NSDate?
            queue.waitForOperationWithBlock({
                value = self.extractProperty(kABPersonModificationDateProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonModificationDateProperty, newValue)
            })
        }
    }
    
    var addresses: Array<MultivalueEntry<Dictionary<SwiftAddressBookAddressProperty, AnyObject>>>? {
        get {
            if let cached = cachedValue("addresses") {
                return cached as? Array<MultivalueEntry<Dictionary<SwiftAddressBookAddressProperty, AnyObject>>>
            }
            var addresses: Array<MultivalueEntry<Dictionary<SwiftAddressBookAddressProperty, AnyObject>>>?
            queue.waitForOperationWithBlock({
                addresses = self.extractMultivalueDictionaryProperty(kABPersonAddressProperty)
            })
            storeInCache("addresses", addresses)
            return addresses
        }
        set {
            queue.waitForOperationWithBlock({
                self.setMultivalueDictionaryProperty(kABPersonAddressProperty, newValue, { NSString(string: $0.abAddressProperty) }, { $0 })
            })
        }
    }
    
    var dates: Array<MultivalueEntry<NSDate>>? {
        get {
            var value: Array<MultivalueEntry<NSDate>>?
            queue.waitForOperationWithBlock({
                value = self.extractMultivalueProperty(kABPersonDateProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setMultivalueProperty(kABPersonDateProperty, newValue)
            })
        }
    }
    
    var type: SwiftAddressBookPersonType? {
        get {
            var value: SwiftAddressBookPersonType?
            queue.waitForOperationWithBlock({
                value = SwiftAddressBookPersonType(type: self.extractProperty(kABPersonMiddleNameProperty))
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonMiddleNameProperty, newValue?.abPersonType)
            })
        }
    }
    
    var phoneNumbers: Array<MultivalueEntry<String>>? {
        get {
            if let cached = cachedValue("phoneNumbers") {
                return cached as? Array<MultivalueEntry<String>>
            }
            var phoneNumbers: Array<MultivalueEntry<String>>?
            queue.waitForOperationWithBlock({
                phoneNumbers = self.extractMultivalueProperty(kABPersonPhoneProperty)
            })
            return phoneNumbers
        }
        set {
            queue.waitForOperationWithBlock({
                self.setMultivalueProperty(kABPersonPhoneProperty, self.convertMultivalueEntries(newValue, converter: { NSString(string: $0) }))
            })
        }
    }
    
    var skipScan: Bool = false
    var isMergeTarget: Bool = false
    
    var phoneNumberUniqueValues: [String]? {
        if let cached = cachedValue("phoneNumbersUniqueValues") {
            return cached as? [String]
        }
        var uniqueValues: [String]?
        if let multivalue = cachedValue("phoneNumbers") as? Array<MultivalueEntry<String>> {
            let set = NSMutableSet()
            for entry in multivalue {
                let value = entry.value as String
                let range: NSRange = NSMakeRange(0, countElements(value))
                let newValue = phoneNumberRegex!.stringByReplacingMatchesInString(value, options: NSMatchingOptions(0), range: range, withTemplate: "")
                set.addObject(newValue)
            }
            uniqueValues = set.allObjects as? [String]
        } else {
            queue.waitForOperationWithBlock({
                if let multivalue: ABMultiValue = ABRecordCopyValue(self.internalRecord, kABPersonPhoneProperty)?.takeRetainedValue() {
                    if let values = ABMultiValueCopyArrayOfAllValues(multivalue)?.takeRetainedValue() as? [String] {
                        let set = NSMutableSet()
                        for value in values {
                            let range: NSRange = NSMakeRange(0, countElements(value))
                            let newValue = phoneNumberRegex!.stringByReplacingMatchesInString(value, options: NSMatchingOptions(0), range: range, withTemplate: "")
                            set.addObject(newValue)
                        }
                        uniqueValues = set.allObjects as? [String]
                    }
                }
            })
        }
        storeInCache("phoneNumbersUniqueValues", uniqueValues)
        return uniqueValues
    }
    
    var emailUniqueValues: [String]? {
        get {
            if let cached = cachedValue("emailUniqueValues") {
                return cached as? [String]
            }
            var emailUniqueValues: [String]?
            queue.waitForOperationWithBlock({
                if let multivalue = ABRecordCopyValue(self.internalRecord, kABPersonEmailProperty) {
                    if let arrayCopy = ABMultiValueCopyArrayOfAllValues(multivalue.takeRetainedValue()) {
                        let values = arrayCopy.takeRetainedValue() as Array<String>
                        let set = NSMutableSet()
                        for value in values {
                            set.addObject(value.lowercaseString)
                        }
                        emailUniqueValues = set.allObjects as? [String]
                        
                    }
                }
            })
            storeInCache("emailUniqueValues", emailUniqueValues)
            return emailUniqueValues
        }
    }
    var socialProfileUniqueValues: [String]? {
        get {
            if let cached = cachedValue("socialProfileUniqueValues") {
                return cached as? [String]
            }
            var socialProfileUniqueValues: [String]?
            queue.waitForOperationWithBlock({
                let set = NSMutableSet()
                if let multivalue = ABRecordCopyValue(self.internalRecord, kABPersonSocialProfileProperty) {
                    if let arrayCopy = ABMultiValueCopyArrayOfAllValues(multivalue.takeRetainedValue()) {
                        for nsDict in arrayCopy.takeRetainedValue() as Array<NSDictionary> {
                            let url = nsDict.objectForKey(kABPersonSocialProfileURLKey) as String
                            let label = nsDict.objectForKey(kABPersonSocialProfileServiceKey) as? String
                            if label != nil && label == "widgets" {
                                //                                logDebug("skipping widgets in social profile")
                            } else {
                                let range: NSRange = NSMakeRange(0, countElements(url))
                                if socialProfileUrlRegex!.matchesInString(url, options: NSMatchingOptions(0), range: range).count > 0 {
                                    set.addObject(url)
                                }
                            }
                        }
                    }
                }
                socialProfileUniqueValues = set.allObjects as? [String]
            })
            storeInCache("socialProfileUniqueValues", socialProfileUniqueValues)
            return socialProfileUniqueValues
        }
    }
    
    
    var urlProfileUniqueValues: [String]? {
        if let cached = cachedValue("urlProfileUniqueValues") {
            return cached as? [String]
        }
        var urlProfileUniqueValues: [String]?
        queue.waitForOperationWithBlock({
            if let multivalue: ABMultiValue = ABRecordCopyValue(self.internalRecord, kABPersonURLProperty)?.takeRetainedValue() {
                if let arrayCopy = ABMultiValueCopyArrayOfAllValues(multivalue)?.takeRetainedValue() as? [String] {
                    let set = NSMutableSet()
                    for url in arrayCopy {
                        let range: NSRange = NSMakeRange(0, countElements(url))
                        if socialProfileUrlRegex!.matchesInString(url, options: NSMatchingOptions(0), range: range).count > 0 {
                            set.addObject(url)
                        }
                    }
                    urlProfileUniqueValues = set.allObjects as? [String]
                }
            }
        })
        storeInCache("urlProfileUniqueValues", urlProfileUniqueValues)
        return urlProfileUniqueValues
    }
    
    
    var addressUniqueValues: [String]? {
        get {
            
            if let cached = cachedValue("addressUniqueValues") {
                return cached as? [String]
            }
            var addressUniqueValues: [String]?
            queue.waitForOperationWithBlock({
                let set = NSMutableSet()
                if let multivalue = ABRecordCopyValue(self.internalRecord, kABPersonAddressProperty) {
                    if let arrayCopy = ABMultiValueCopyArrayOfAllValues(multivalue.takeRetainedValue()) {
                        for nsDict in arrayCopy.takeRetainedValue() as Array<NSDictionary> {
                            var addressString = ""
                            for (key, value) in nsDict {
                                addressString += (value as String).lowercaseString
                            }
                            set.addObject(addressString)
                        }
                    }
                }
                addressUniqueValues = set.allObjects as? [String]
            })
            storeInCache("addressUniqueValues", addressUniqueValues)
            return addressUniqueValues
            
        }
    }
    
    
    var instantMessage: Array<MultivalueEntry<Dictionary<SwiftAddressBookInstantMessagingProperty, String>>>? {
        get {
            var value: Array<MultivalueEntry<Dictionary<SwiftAddressBookInstantMessagingProperty, String>>>?
            queue.waitForOperationWithBlock({
                value = self.extractMultivalueProperty(kABPersonInstantMessageProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setMultivalueDictionaryProperty(kABPersonInstantMessageProperty, newValue, keyConverter: { NSString(string: $0.abInstantMessageProperty) }, valueConverter: { NSString(string: $0) })
            })
        }
    }
    
    var instantMessageUniqueValues: [String]? {
        if let cached = cachedValue("instantMessageUniqueValues") {
            return cached as? [String]
        }
        var instantMessageUniqueValues: [String]?
        queue.waitForOperationWithBlock({
            let set = NSMutableSet()
            if let multivalue: ABMultiValue = ABRecordCopyValue(self.internalRecord, kABPersonInstantMessageProperty)?.takeRetainedValue() {
                if let arrayCopy = ABMultiValueCopyArrayOfAllValues(multivalue) {
                    for nsDict in arrayCopy.takeRetainedValue() as Array<NSDictionary> {
                        let service = nsDict.objectForKey(kABPersonInstantMessageServiceKey) as String
                        let username = nsDict.objectForKey(kABPersonInstantMessageUsernameKey) as String
                        set.addObject("\(service):\(username)")
                    }
                }
            }
            instantMessageUniqueValues = set.allObjects as? [String]
        })
        storeInCache("instantMessageUniqueValues", instantMessageUniqueValues)
        return instantMessageUniqueValues
    }
    
    var socialProfiles: Array<MultivalueEntry<Dictionary<SwiftAddressBookSocialProfileProperty, String>>>? {
        get {
            if let cached = cachedValue("socialProfiles") {
                return cached as? Array<MultivalueEntry<Dictionary<SwiftAddressBookSocialProfileProperty, String>>>
            }
            var socialProfiles: Array<MultivalueEntry<Dictionary<SwiftAddressBookSocialProfileProperty, String>>>?
            queue.waitForOperationWithBlock({
                socialProfiles = self.extractMultivalueDictionaryProperty(kABPersonSocialProfileProperty)
            })
            storeInCache("socialProfiles", socialProfiles)
            return socialProfiles
        }
        set {
            queue.waitForOperationWithBlock({
                self.setMultivalueDictionaryProperty(kABPersonSocialProfileProperty, newValue, keyConverter: { NSString(string: $0.abSocialProfileProperty) }, valueConverter: { NSString(string: $0) })
            })
        }
    }
    
    
    var urls: Array<MultivalueEntry<String>>? {
        get {
            if let cached = cachedValue("urls") {
                return cached as? Array<MultivalueEntry<String>>
            }
            var urls: Array<MultivalueEntry<String>>?
            queue.waitForOperationWithBlock({
                urls = self.extractMultivalueProperty(kABPersonURLProperty)
            })
            storeInCache("urls", urls)
            return urls
        }
        set {
            queue.waitForOperationWithBlock({
                self.setMultivalueProperty(kABPersonURLProperty, self.convertMultivalueEntries(newValue, converter: { NSString(string: $0) }))
            })
        }
    }
    
    var relatedNames: Array<MultivalueEntry<String>>? {
        get {
            var value: Array<MultivalueEntry<String>>?
            queue.waitForOperationWithBlock({
                value = self.extractMultivalueProperty(kABPersonRelatedNamesProperty)
            })
            return value
        }
        set {
            queue.waitForOperationWithBlock({
                self.setMultivalueProperty(kABPersonRelatedNamesProperty, self.convertMultivalueEntries(newValue, converter: { NSString(string: $0) }))
            })
        }
    }
    
    var alternateBirthday: Dictionary<String, AnyObject>? {
        get {
            var value: Dictionary<String, AnyObject>?
            queue.waitForOperationWithBlock({
                value = self.extractProperty(kABPersonAlternateBirthdayProperty)
            })
            return value
        }
        set {
            let dict: NSDictionary? = newValue
            queue.waitForOperationWithBlock({
                self.setSingleValueProperty(kABPersonAlternateBirthdayProperty, dict)
            })
        }
    }
    
    //    var customFields = Dictionary<String, AnyObject>()
    
    
    //MARK: generic methods to set and get person properties
    
    private func extractProperty<T>(propertyName: ABPropertyID) -> T? {
        
        if let propertyValue = ABRecordCopyValue(self.internalRecord, propertyName) {
            return propertyValue.takeRetainedValue() as? T
        }
        return nil
    }
    
    private func setSingleValueProperty<T:AnyObject>(key: ABPropertyID, _ value: T?) {
        ABRecordSetValue(self.internalRecord, key, value, nil)
    }
    
    private func extractMultivalueProperty<T>(propertyName: ABPropertyID) -> Array<MultivalueEntry<T>>? {
        var array = Array<MultivalueEntry<T>>()
        
        let multivalue: ABMultiValue? = extractProperty(propertyName)
        for i: Int in 0 ..< (ABMultiValueGetCount(multivalue)) {
            let value: T? = ABMultiValueCopyValueAtIndex(multivalue, i).takeRetainedValue() as? T
            if let v: T = value {
                let id: Int = Int(ABMultiValueGetIdentifierAtIndex(multivalue, i))
                var label: String = kABOtherLabel
                if let copy = ABMultiValueCopyLabelAtIndex(multivalue, i) {
                    label = copy.takeRetainedValue()
                }
                array.append(MultivalueEntry(value: v, label: label, id: id))
            }
        }
        if array.count > 0 {
            return array
        } else {
            return nil
        }
    }
    
    private func extractMultivalueDictionaryProperty<K, V>(propertyName: ABPropertyID) -> Array<MultivalueEntry<Dictionary<K, V>>>? {
        var array = Array<MultivalueEntry<Dictionary<K, V>>>()
        let multivalue: ABMultiValue? = extractProperty(propertyName)
        for i: Int in 0 ..< (ABMultiValueGetCount(multivalue)) {
            if propertyName == kABPersonSocialProfileProperty {
                let value: NSDictionary? = ABMultiValueCopyValueAtIndex(multivalue, i).takeRetainedValue() as? NSDictionary
                let v: Dictionary = value! as Dictionary
                var dict: Dictionary<K, V> = [:]
                for (key, val) in v {
                    var newKey: K = SwiftAddressBookSocialProfileProperty(property: key as NSString) as K
                    dict[newKey] = val as? V
                }
                let id: Int = Int(ABMultiValueGetIdentifierAtIndex(multivalue, i))
                if let copy = ABMultiValueCopyLabelAtIndex(multivalue, i) {
                    let label: String = copy.takeRetainedValue()
                    array.append(MultivalueEntry(value: dict, label: label, id: id))
                }
            } else if propertyName == kABPersonAddressProperty {
                let value: NSDictionary? = ABMultiValueCopyValueAtIndex(multivalue, i).takeRetainedValue() as? NSDictionary
                let v: Dictionary = value! as Dictionary
                var dict: Dictionary<K, V> = [:]
                for (key, val) in v {
                    var newKey: K = SwiftAddressBookAddressProperty(property: key as NSString) as K
                    dict[newKey] = val as? V
                }
                let id: Int = Int(ABMultiValueGetIdentifierAtIndex(multivalue, i))
                if let copy = ABMultiValueCopyLabelAtIndex(multivalue, i) {
                    let label: String = copy.takeRetainedValue()
                    array.append(MultivalueEntry(value: dict, label: label, id: id))
                }
                
            } else {
                logDebug("extractMultivalueDictionaryProperty called with unhandled property type")
            }
            
            
        }
        if array.count > 0 {
            return array
        } else {
            return nil
        }
    }
    
    private func convertDictionary<T, U, V:AnyObject, W:AnyObject where V:Hashable>(d: Dictionary<T, U>?, keyConverter: (T) -> V, valueConverter: (U) -> W) -> NSDictionary? {
        if let d2 = d {
            var dict = Dictionary<V, W>()
            for key in d2.keys {
                dict[keyConverter(key)] = valueConverter(d2[key]!)
            }
            return dict as NSDictionary
        } else {
            return nil
        }
    }
    
    private func convertMultivalueEntries<T, U:AnyObject>(multivalue: Array<MultivalueEntry<T>>?, converter: (T) -> U) -> Array<MultivalueEntry<U>>? {
        if let array = multivalue{
            var result = Array<MultivalueEntry<U>>()
            for m in array{
                result.append(MultivalueEntry(value: converter(m.value) as U, label: m.label, id: m.id))
            }
            return result
        }
        return nil
    }
    private func convertMultivalueDictionaryEntries<T,U, X:AnyObject>(multivalue: Array<MultivalueEntry<Dictionary<T,U>>>?, converter: (Dictionary<T,U>) -> X) -> Array<MultivalueEntry<X>>? {
        return multivalue?.map {
            m -> MultivalueEntry<X> in
            return MultivalueEntry(value: converter(m.value) as X, label: m.label, id: m.id)
        }
    }
    
    private func setMultivalueProperty<T:AnyObject>(key: ABPropertyID, _ multivalue: Array<MultivalueEntry<T>>?) {
        if (multivalue == nil) {
            ABRecordSetValue(self.internalRecord, key, ABMultiValueCreateMutable(ABPersonGetTypeOfProperty(key)).takeRetainedValue(), nil)
        }
        
        var abMultivalue: ABMutableMultiValue
        if let origMultiValue: ABMultiValue = extractProperty(key) {
            abMultivalue = ABMultiValueCreateMutableCopy(origMultiValue).takeRetainedValue()
        } else {
            abMultivalue = ABMultiValueCreateMutable(ABPersonGetTypeOfProperty(key)).takeRetainedValue()
        }
        
        
        var identifiers = Array<Int>()
        
        for i: Int in 0 ..< (ABMultiValueGetCount(abMultivalue)) {
            identifiers.append(Int(ABMultiValueGetIdentifierAtIndex(abMultivalue, i)))
        }
        
        for m: MultivalueEntry in multivalue! {
            if contains(identifiers, m.id) {
                let index = ABMultiValueGetIndexForIdentifier(abMultivalue, Int32(m.id))
                ABMultiValueReplaceValueAtIndex(abMultivalue, m.value, index)
                ABMultiValueReplaceLabelAtIndex(abMultivalue, m.label, index)
                identifiers.removeAtIndex(find(identifiers, m.id)!)
            } else {
                ABMultiValueAddValueAndLabel(abMultivalue, m.value, m.label, nil)
            }
        }
        
        for i in identifiers {
            ABMultiValueRemoveValueAndLabelAtIndex(abMultivalue, ABMultiValueGetIndexForIdentifier(abMultivalue, Int32(i)))
        }
        
        ABRecordSetValue(self.internalRecord, key, abMultivalue, nil)
    }
    
    private func replaceMultivalueProperty<T:AnyObject>(key: ABPropertyID, _ multivalue: Array<MultivalueEntry<T>>?) {
        if (multivalue == nil) {
            ABRecordSetValue(self.internalRecord, key, ABMultiValueCreateMutable(ABMultiValueGetPropertyType(extractProperty(key))).takeRetainedValue(), nil)
        }
        
        if let multivalue: ABMultiValue = extractProperty(key) {
            logDebug("isMultivalue")
        }
        
        let propertyType = ABPersonGetTypeOfProperty(key)
        var abMultivalue: ABMutableMultiValue = ABMultiValueCreateMutable(propertyType).takeRetainedValue()
        
        for m: MultivalueEntry in multivalue! {
            ABMultiValueAddValueAndLabel(abMultivalue, m.value, m.label, nil)
        }
        ABRecordSetValue(self.internalRecord, key, abMultivalue, nil)
    }
    
    private func setMultivalueDictionaryProperty<T, U, V:AnyObject, W:AnyObject where V:Hashable >(key: ABPropertyID, _ multivalue: Array<MultivalueEntry<Dictionary<T, U>>>?, keyConverter: (T) -> V, valueConverter: (U) -> W) {
        
        let array = convertMultivalueEntries(multivalue, converter: {
            d -> NSDictionary in
            return self.convertDictionary(d, keyConverter: keyConverter, valueConverter: valueConverter)!
        })
        
        setMultivalueProperty(key, array)
    }
    
}

//MARK: swift structs for convenience

enum SwiftAddressBookOrdering {
    
    case lastName, firstName
    
    init(ordering: ABPersonSortOrdering) {
        switch Int(ordering) {
        case kABPersonSortByLastName:
            self = .lastName
        case kABPersonSortByFirstName:
            self = .firstName
        default:
            self = .firstName
        }
    }
    
    var abPersonSortOrderingValue: UInt32 {
        get {
            switch self {
            case .lastName:
                return UInt32(kABPersonSortByLastName)
            case .firstName:
                return UInt32(kABPersonSortByFirstName)
            }
        }
    }
}

enum SwiftAddressBookCompositeNameFormat {
    case firstNameFirst, lastNameFirst
    
    init(format: ABPersonCompositeNameFormat) {
        switch Int(format) {
        case kABPersonCompositeNameFormatFirstNameFirst:
            self = .firstNameFirst
        case kABPersonCompositeNameFormatLastNameFirst:
            self = .lastNameFirst
        default:
            self = .firstNameFirst
        }
    }
}

enum SwiftAddressBookSourceType {
    case local, exchange, exchangeGAL, mobileMe, LDAP, cardDAV, cardDAVSearch
    
    init(abSourceType: ABSourceType) {
        switch Int(abSourceType) {
        case kABSourceTypeLocal:
            self = .local
        case kABSourceTypeExchange:
            self = .exchange
        case kABSourceTypeExchangeGAL:
            self = .exchangeGAL
        case kABSourceTypeMobileMe:
            self = .mobileMe
        case kABSourceTypeLDAP:
            self = .LDAP
        case kABSourceTypeCardDAV:
            self = .cardDAV
        case kABSourceTypeCardDAVSearch:
            self = .cardDAVSearch
        default:
            self = .local
        }
    }
}

enum SwiftAddressBookPersonImageFormat {
    case thumbnail
    case originalSize
    
    var abPersonImageFormat: ABPersonImageFormat {
        switch self {
        case .thumbnail:
            return kABPersonImageFormatThumbnail
        case .originalSize:
            return kABPersonImageFormatOriginalSize
        }
    }
}


enum SwiftAddressBookSocialProfileProperty {
    case url, service, username, userIdentifier
    
    init(property: String) {
        switch property {
        case kABPersonSocialProfileURLKey:
            self = .url
        case kABPersonSocialProfileServiceKey:
            self = .service
        case kABPersonSocialProfileUsernameKey:
            self = .username
        case kABPersonSocialProfileUserIdentifierKey:
            self = .userIdentifier
        default:
            self = .url
        }
    }
    
    var abSocialProfileProperty: String {
        switch self {
        case .url:
            return kABPersonSocialProfileURLKey
        case .service:
            return kABPersonSocialProfileServiceKey
        case .username:
            return kABPersonSocialProfileUsernameKey
        case .userIdentifier:
            return kABPersonSocialProfileUserIdentifierKey
        }
    }
}

enum SwiftAddressBookInstantMessagingProperty {
    case service, username
    
    init(property: String) {
        switch property {
        case kABPersonInstantMessageServiceKey:
            self = .service
        case kABPersonInstantMessageUsernameKey:
            self = .username
        default:
            self = .service
        }
    }
    
    var abInstantMessageProperty: String {
        switch self {
        case .service:
            return kABPersonInstantMessageServiceKey
        case .username:
            return kABPersonInstantMessageUsernameKey
        }
    }
}

enum SwiftAddressBookPersonType {
    
    case person, organization
    
    init(type: CFNumber?) {
        if CFNumberCompare(type, kABPersonKindPerson, nil) == CFComparisonResult.CompareEqualTo {
            self = .person
        } else if CFNumberCompare(type, kABPersonKindOrganization, nil) == CFComparisonResult.CompareEqualTo {
            self = .organization
        } else {
            self = .person
        }
    }
    
    var abPersonType: CFNumber {
        get {
            switch self {
            case .person:
                return kABPersonKindPerson
            case .organization:
                return kABPersonKindOrganization
            }
        }
    }
}

enum SwiftAddressBookAddressProperty {
    case street, city, state, zip, country, countryCode
    
    init(property: String) {
        switch property {
        case kABPersonAddressStreetKey:
            self = .street
        case kABPersonAddressCityKey:
            self = .city
        case kABPersonAddressStateKey:
            self = .state
        case kABPersonAddressZIPKey:
            self = .zip
        case kABPersonAddressCountryKey:
            self = .country
        case kABPersonAddressCountryCodeKey:
            self = .countryCode
        default:
            self = .street
        }
    }
    
    var abAddressProperty: String {
        get {
            switch self {
            case .street:
                return kABPersonAddressStreetKey
            case .city:
                return kABPersonAddressCityKey
            case .state:
                return kABPersonAddressStateKey
            case .zip:
                return kABPersonAddressZIPKey
            case .country:
                return kABPersonAddressCountryKey
            case .countryCode:
                return kABPersonAddressCountryCodeKey
            default:
                return kABPersonAddressStreetKey
            }
        }
    }
}

struct MultivalueEntry<T> {
    var value: T
    var label: String
    let id: Int
}


//MARK: methods to convert arrays of ABRecords

private func convertRecordsToSources(records: [ABRecord]?) -> [SwiftAddressBookSource]? {
    let swiftRecords = records?.map {
        (record: ABRecord) -> SwiftAddressBookSource in return SwiftAddressBookRecord(record: record).convertToSource()!
    }
    return swiftRecords
}

private func convertRecordsToGroups(records: [ABRecord]?) -> [SwiftAddressBookGroup]? {
    let swiftRecords = records?.map {
        (record: ABRecord) -> SwiftAddressBookGroup in return SwiftAddressBookRecord(record: record).convertToGroup()!
    }
    return swiftRecords
}

private func convertRecordsToPersons(records: [ABRecord]?) -> [SwiftAddressBookPerson]? {
    let swiftRecords = records?.map {
        (record: ABRecord) -> SwiftAddressBookPerson in return SwiftAddressBookRecord(record: record).convertToPerson()!
    }
    return swiftRecords
}



//MARK: some more handy methods

extension NSString {
    convenience init?(string: String?) {
        if string == nil {
            self.init()
            return nil
        }
        self.init(string: string!)
    }
}

func errorIfNoSuccess(call: (UnsafeMutablePointer<Unmanaged<CFError>?>) -> Bool) -> CFError? {
    var err: Unmanaged<CFError>? = nil
    let success: Bool = call(&err)
    if success {
        return nil
    } else {
        return err?.takeRetainedValue()
    }
}


extension SwiftAddressBookPerson {
    //iterate name properties - check if there are useful info
    func checkStringProperty(propertyName: ABPropertyID, callback: (() -> Void)) {
        queue.waitForOperationWithBlock({
            if let propertyValue = ABRecordCopyValue(self.internalRecord, propertyName) {
                if let valueString = propertyValue.takeRetainedValue() as? String {
                    if valueString != "" {
                        callback()
                    }
                }
            }
        })
    }
    
    func checkMultiStringProperty(propertyName: ABPropertyID, callback: (() -> Void)) {
        queue.waitForOperationWithBlock({
            if let propertyValue = ABRecordCopyValue(self.internalRecord, propertyName) {
                let multivalue: ABMultiValue? = propertyValue.takeRetainedValue() as ABMultiValue
                for i: Int in 0 ..< (ABMultiValueGetCount(multivalue)) {
                    let value: String? = ABMultiValueCopyValueAtIndex(multivalue, i).takeRetainedValue() as? String
                    if let v: String = value {
                        let id: Int = Int(ABMultiValueGetIdentifierAtIndex(multivalue, i))
                        if let copy = ABMultiValueCopyLabelAtIndex(multivalue, i) {
                            let label: String = copy.takeRetainedValue()
                            if v != "" && label != "" {
                                callback()
                            }
                        }
                    }
                }
            }
        })
    }
    
    
    func checkMultiDictionaryProperty(propertyName: ABPropertyID, callback: (() -> Void), filterCallback: ((label:String, value:NSDictionary?) -> Void)? = nil) {
        queue.waitForOperationWithBlock({
            if let propertyValue = ABRecordCopyValue(self.internalRecord, propertyName) {
                let multivalue: ABMultiValue? = propertyValue.takeRetainedValue() as ABMultiValue
                for i: Int in 0 ..< (ABMultiValueGetCount(multivalue)) {
                    let value: NSDictionary? = ABMultiValueCopyValueAtIndex(multivalue, i).takeRetainedValue() as? NSDictionary
                    let v: Dictionary = value! as Dictionary
                    let id: Int = Int(ABMultiValueGetIdentifierAtIndex(multivalue, i))
                    if let copy = ABMultiValueCopyLabelAtIndex(multivalue, i) {
                        let label: String = copy.takeRetainedValue()
                        if label != "" && v.count > 0 {
                            if filterCallback == nil {
                                callback()
                            } else {
                                filterCallback!(label: label, value: value)
                            }
                            
                        }
                    }
                }
            }
        })
    }
    
    func checkMultiDateTimeProperty(propertyName: ABPropertyID, callback: (() -> Void)) {
        queue.waitForOperationWithBlock({
            if let propertyValue = ABRecordCopyValue(self.internalRecord, propertyName) {
                if let multivalue = propertyValue.takeRetainedValue() as? NSDate {
                    callback()
                }
                
            }
        })
    }
    
    func checkPropertyExists(propertyName: ABPropertyID, callback: (() -> Void)) {
        queue.waitForOperationWithBlock({
            if let propertyValue = ABRecordCopyValue(self.internalRecord, propertyName) {
                if let valueString = propertyValue.takeRetainedValue() as? String {
                    if valueString != "" {
                        callback()
                    }
                }
            }
        })
    }
    
    
    func mergeContact(duplicatePerson: SwiftAddressBookPerson) {
        
        duplicatePerson.performRecordBlock({
            [unowned self](internalRecord: ABRecord) in
            
            //Merge phone Numbers
            if let duplicatePhoneNumbers = duplicatePerson.phoneNumbers {
                for duplicateNumber in duplicatePhoneNumbers {
                    var duplicateNumberString = String(duplicateNumber.value).replace("[-\\s\\(\\)]", template: "")
                    if let targetPhoneNumbers = self.phoneNumbers {
                        var foundInTarget = false
                        for targetNumber in targetPhoneNumbers {
                            var targetNumberString = String(targetNumber.value).replace("[-\\s\\(\\)]", template: "")
                            if duplicateNumberString.rangeOfString(targetNumberString) != nil || targetNumberString.rangeOfString(duplicateNumberString) != nil {
                                //skip
                                foundInTarget = true
                            }
                        }
                        if foundInTarget {
                            logDebug("duplicate email skipped-------------")
                        } else {
                            logInfo("merging email \(duplicateNumberString) >> \(self.getName())")
                            self.phoneNumbers!.append(duplicateNumber)
                        }
                    } else {
                        logInfo("merging phone number \(duplicateNumber) >> \(self.getName())")
                    }
                }
            }
            //Merge emails
            if let duplicateEmails = duplicatePerson.emails {
                for duplicateEmail in duplicateEmails {
                    var duplicateEmailString = duplicateEmail.value.lowercaseString
                    if let targetEmails = self.emails {
                        var foundInTarget = false
                        for targetEmail in targetEmails {
                            var targetEmailString = targetEmail.value.lowercaseString
                            if duplicateEmailString == targetEmailString {
                                //skip
                                foundInTarget = true
                                
                            }
                        }
                        if foundInTarget {
                            logDebug("duplicate email skipped-------------")
                        } else {
                            logInfo("merging email \(duplicateEmailString) >> \(self.getName())")
                            self.emails!.append(duplicateEmail)
                        }
                    } else {
                        logInfo("merging email \(duplicateEmailString) >> \(self.getName())")
                    }
                }
            }
            
            //Merge Social Profiles
            if let duplicateSocialProfiles = duplicatePerson.socialProfiles {
                for duplicateSocialProfile in duplicateSocialProfiles {
                    if let targetSocialProfiles = self.socialProfiles {
                        var foundInTarget = false
                        for targetSocialProfile in targetSocialProfiles {
                            if targetSocialProfile.value[SwiftAddressBookSocialProfileProperty.url] == duplicateSocialProfile.value[SwiftAddressBookSocialProfileProperty.url] &&
                                targetSocialProfile.value[SwiftAddressBookSocialProfileProperty.service] == duplicateSocialProfile.value[SwiftAddressBookSocialProfileProperty.service] {
                                    foundInTarget = true
                            }
                        }
                        if foundInTarget {
                            logDebug("duplicate social profile skipped-------------")
                        } else {
                            logInfo("merging profile \(duplicateSocialProfile) >> \(self.getName())")
                            self.socialProfiles!.append(duplicateSocialProfile)
                        }
                    } else {
                        logInfo("merging profile \(duplicateSocialProfile) >> \(self.getName())")
                    }
                }
            }
            
            
            //Merge remaining properties
            func mergeStringProperty(propertyID: ABPropertyID) {
                if let property = ABRecordCopyValue(internalRecord, propertyID) {
                    let propertyValue = property.takeRetainedValue() as String
                    if ABRecordCopyValue(self.internalRecord, propertyID) == nil {
                        var e: NSError?
                        if !ABRecordSetValue(self.internalRecord, propertyID, propertyValue, nil) {
                            logError("Error merging property")
                        }
                    }
                }
            }
            
            
            
            //Merge remaining properties
            func mergeMultiStringProperty(propertyID: ABPropertyID) {
                if let property = ABRecordCopyValue(internalRecord, propertyID) {
                    let propertyValue: ABMultiValue? = property.takeRetainedValue() as ABMultiValue
                    if let targetProperty = ABRecordCopyValue(self.internalRecord, propertyID) {
                        let targetPropertyValue: ABMultiValue = targetProperty.takeRetainedValue() as ABMultiValue
                        var newMultiValue: ABMultiValue = ABMultiValueCreateMutableCopy(targetPropertyValue).takeRetainedValue() as ABMultiValue
                        for i: Int in 0 ..< (ABMultiValueGetCount(propertyValue)) {
                            var foundInTarget = false
                            let value: String? = ABMultiValueCopyValueAtIndex(propertyValue, i).takeRetainedValue() as? String
                            if let v: String = value {
                                if let copy = ABMultiValueCopyLabelAtIndex(propertyValue, i) {
                                    let label: String = copy.takeRetainedValue()
                                    //we have got the label and value of duplicate property
                                    //Now loop target
                                    
                                    
                                    
                                    for i: Int in 0 ..< (ABMultiValueGetCount(targetPropertyValue)) {
                                        let targetValue: String? = ABMultiValueCopyValueAtIndex(targetPropertyValue, i).takeRetainedValue() as? String
                                        if let tv: String = targetValue {
                                            if let tcopy = ABMultiValueCopyLabelAtIndex(targetPropertyValue, i) {
                                                let tlabel: String = tcopy.takeRetainedValue()
                                                //we have got the label and value of duplicate property
                                                if tlabel == label && tv == v {
                                                    foundInTarget = true
                                                }
                                            }
                                        }
                                    }
                                    
                                    if foundInTarget {
                                        logDebug("duplicate property skipped-------------")
                                    } else {
                                        logInfo("merging value \(value) >> \(self.getName())")
                                        //                                            self.socialProfiles!.append(duplicateSocialProfile)
                                        ABMultiValueAddValueAndLabel(newMultiValue, value, label, nil)
                                    }
                                }
                            }
                        }
                        var e: NSError?
                        if !ABRecordSetValue(self.internalRecord, propertyID, newMultiValue, nil) {
                            logError("Error merging property")
                        }
                    } else {
                        var e: NSError?
                        if !ABRecordSetValue(self.internalRecord, propertyID, propertyValue, nil) {
                            logError("Error merging property")
                        }
                    }
                }
            }
            
            
            func mergeMultiDateTimeProperty(propertyID: ABPropertyID) {
                if let property = ABRecordCopyValue(internalRecord, propertyID) {
                    let propertyValue = property.takeRetainedValue() as NSDate
                    if ABRecordCopyValue(self.internalRecord, propertyID) == nil {
                        
                        if !ABRecordSetValue(self.internalRecord, propertyID, propertyValue, nil) {
                            logError("error merging property")
                        }
                    }
                }
            }
            
            func mergeMultiDictionaryProperty(propertyID: ABPropertyID) {
                if let property = ABRecordCopyValue(internalRecord, propertyID) {
                    let propertyValue: ABMultiValue = property.takeRetainedValue() as ABMultiValue
                    if let targetProperty = ABRecordCopyValue(self.internalRecord, propertyID) {
                        let targetPropertyValue: ABMultiValue = targetProperty.takeRetainedValue() as ABMultiValue
                        var newMultiValue: ABMultiValue = ABMultiValueCreateMutableCopy(targetPropertyValue).takeRetainedValue() as ABMultiValue
                        for i: Int in 0 ..< (ABMultiValueGetCount(propertyValue)) {
                            
                            let value: NSDictionary? = ABMultiValueCopyValueAtIndex(propertyValue, i).takeRetainedValue() as? NSDictionary
                            //                                        let v: Dictionary = value! as Dictionary
                            
                            if let copy = ABMultiValueCopyLabelAtIndex(propertyValue, i) {
                                let label: String = copy.takeRetainedValue()
                                //we have got the label and value of duplicate property
                                var foundInTarget = false
                                //Now loop target
                                for i: Int in 0 ..< (ABMultiValueGetCount(targetPropertyValue)) {
                                    let targetValue: NSDictionary? = ABMultiValueCopyValueAtIndex(targetPropertyValue, i).takeRetainedValue() as? NSDictionary
                                    //                                                let tv: Dictionary = targetValue! as Dictionary
                                    if let tcopy = ABMultiValueCopyLabelAtIndex(targetPropertyValue, i) {
                                        let tlabel: String = tcopy.takeRetainedValue()
                                        //we have got the label and value of duplicate property
                                        for (key, val) in value! {
                                            if targetValue!.objectForKey(key) != nil {
                                                if targetValue!.objectForKey(key)!.isEqual(val) {
                                                    foundInTarget = true
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                if foundInTarget {
                                    logDebug("duplicate property skipped-------------")
                                } else {
                                    logInfo("merging value \(value) >> \(self.getName())")
                                    ABMultiValueAddValueAndLabel(newMultiValue, value, label, nil)
                                }
                                
                            }
                        }
                        var e: NSError?
                        if !ABRecordSetValue(self.internalRecord, propertyID, newMultiValue, nil) {
                            logError("error merging property")
                        }
                    } else {
                        var e: NSError?
                        if !ABRecordSetValue(self.internalRecord, propertyID, propertyValue, nil) {
                            logError("error merging property")
                        }
                    }
                }
            }
            
            mergeStringProperty(kABPersonFirstNameProperty)
            mergeStringProperty(kABPersonLastNameProperty)
            mergeStringProperty(kABPersonMiddleNameProperty)
            mergeStringProperty(kABPersonPrefixProperty)
            mergeStringProperty(kABPersonSuffixProperty)
            mergeStringProperty(kABPersonNicknameProperty)
            mergeStringProperty(kABPersonFirstNamePhoneticProperty)
            mergeStringProperty(kABPersonLastNamePhoneticProperty)
            mergeStringProperty(kABPersonMiddleNamePhoneticProperty)
            mergeStringProperty(kABPersonOrganizationProperty)
            mergeStringProperty(kABPersonJobTitleProperty)
            mergeStringProperty(kABPersonDepartmentProperty)
            //                        mergeMultiStringProperty(kABPersonEmailProperty)
            mergeMultiDateTimeProperty(kABPersonBirthdayProperty)
            mergeStringProperty(kABPersonNoteProperty)
            mergeMultiDictionaryProperty(kABPersonAddressProperty)
            //                        mergeMultiDateTimeProperty(kABPersonDateProperty)
            //                        mergeMultiStringProperty(kABPersonPhoneProperty)
            mergeMultiDictionaryProperty(kABPersonInstantMessageProperty)
            //                        mergeMultiDictionaryProperty(kABPersonSocialProfileProperty)
            mergeMultiStringProperty(kABPersonURLProperty)
            mergeMultiStringProperty(kABPersonRelatedNamesProperty)
            //                        mergeProperty(kABPersonAlternateBirthdayProperty)
            logInfo("removing record: \(duplicatePerson.getFullName())")
        })
    }
    
    
}