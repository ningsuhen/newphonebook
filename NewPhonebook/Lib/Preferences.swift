//
//  Preferences.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/18/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation

private var _sharedPreferences = Preferences()

struct MergeSettings {
    var partialNumber: Bool
    var emailMatch: Bool
    var socialProfile: Bool
    var urlProfile: Bool
    var instantMessage: Bool
    var nameAndAddress: Bool
    var nameAndBirthDay: Bool
    var nameAndJobTitle: Bool
    var nameAndOrganization: Bool
    var nameAndDepartment: Bool
    var nameAndNickname: Bool
    
    
}

struct SearchSettings {
    var defaultSearchName: Bool
    var defaultSearchJobTitle: Bool
    var defaultSearchDepartment: Bool
    var defaultSearchOrganization: Bool
    var defaultSearchEmails: Bool
    var defaultSearchPhoneNumbers: Bool
    var defaultSearchAddress: Bool
    var defaultSearchSocialProfiles: Bool
    var defaultSearchUrlProfiles: Bool
    var defaultSearchInstantMessage: Bool
}

struct AccountKeys {
    static let Dropbox = "dropboxAccount"
    static let GoogleDrive = "googleDriveAccount"
    static let Facebook = "facebookAccount"
}

class SCUserInfo: NSObject {
    var username: String?
    var link: String?
    var name: String?
    var email: String?
    var first_name: String?
    var middle_name: String?
    var last_name: String?
    var birthday: String?
    var location: String?
    var street: String?
    var city: String?
    var state: String?
    var country: String?
    var zip: String?
    var latitude: Int?
    var longitude: Int?
    init(dict: NSDictionary) {
        if let username = dict["username"] as? String {
            self.username = username
        } else {
            logError("username key absent on SCUserInfo init dictionary")
        }
        if let link = dict["link"] as? String {
            self.link = link
        } else {
            logError("link key absent on SCUserInfo init dictionary")
        }
        self.name = dict["name"] as? String
        self.email = dict["email"] as? String
        self.first_name = dict["first_name"] as? String
        self.middle_name = dict["middle_name"] as? String
        self.last_name = dict["last_name"] as? String
        self.birthday = dict["birthday"] as? String
        self.location = dict["location"] as? String
        self.street = dict["street"] as? String
        self.city = dict["city"] as? String
        self.state = dict["state"] as? String
        self.country = dict["country"] as? String
        self.zip = dict["zip"] as? String
        let latitude = dict["latitude"] as? NSNumber
        self.latitude = latitude?.integerValue
        let longitude = dict["longitude"] as? NSNumber
        self.longitude = longitude?.integerValue
    }
    init(fbGraphUser: FBGraphUser) {
        super.init()
        self.update(fbGraphUser)
    }
    init(graphObject: FBGraphObject) {
        super.init()
        self.update(graphObject)
    }
    
    func update(fbGraphUser: FBGraphUser) -> SCUserInfo {
        if let username = fbGraphUser.username {
            self.username = username
        } else {
            logError("username key absent on SCUserInfo init FBGraphUser")
        }
        if let link = fbGraphUser.link {
            self.link = link
        } else {
            logError("link key absent on SCUserInfo init dictionary")
        }
        self.name = fbGraphUser.name
        self.email = fbGraphUser.objectForKey("email") as? String
        self.first_name = fbGraphUser.first_name
        self.middle_name = fbGraphUser.middle_name
        self.last_name = fbGraphUser.last_name
        
        self.birthday = fbGraphUser.birthday
        
        let location = fbGraphUser.location?.location
        self.location = fbGraphUser.location?.name
        self.street = location?.street
        self.city = location?.city
        self.state = location?.state
        self.country = location?.country
        self.zip = location?.zip
        self.latitude = location?.latitude?.integerValue
        self.longitude = location?.longitude?.integerValue
        return self
    }
    func update(graphObject: FBGraphObject) -> SCUserInfo {
        if let username = graphObject["user_name"] as? String{
            self.username = username
        } else {
            logError("username key absent on SCUserInfo init FBGraphObject")
        }
        if let link = graphObject["link"] as? String{
            self.link = link
        } else {
            logError("link key absent on SCUserInfo init dictionary")
        }
        self.name = graphObject["name"] as? String
        self.email = graphObject["email as? String"] as? String
        self.first_name = graphObject["first_name"] as? String
        self.middle_name = graphObject["middle_name"] as? String
        self.last_name = graphObject["last_name"] as? String
        
        self.birthday = graphObject["birthday"] as? String
        self.location = graphObject["location"]?["name"] as? String

        /* self.street = location?.street
        self.city = location?.city
        self.state = location?.state
        self.country = location?.country
        self.zip = location?.zip
        self.latitude = location?.latitude?.integerValue
        self.longitude = location?.longitude?.integerValue */
        return self
    }
    
    func getLocations() -> [String]? {
        if location != nil {
            return split(location!, { $0 == "," || $0 == " " }, allowEmptySlices: false)
        }
        return nil
    }
    
    func getName() -> String! {
        var result = ""
        if let value = self.name {
            return value
        }
        if let value = self.first_name {
            result = result + value
        }
        if let value = self.middle_name {
            result = result + value
        }
        if let value = self.last_name {
            result = result + value
        }
        return result
    }
    
    func convertToDictionary() -> NSDictionary {
        logDebug("convertToDictionary called")
        var dict = Dictionary<String, AnyObject>()
        dict["username"] = username
        dict["link"] = link
        
        dict["name"] = name
        dict["email"] = email
        dict["first_name"] = first_name
        dict["last_name"] = last_name
        dict["middle_name"] = middle_name
        
        dict["birthday"] = birthday
        dict["location"] = location
        dict["street"] = street
        dict["city"] = city
        dict["state"] = state
        dict["country"] = country
        dict["zip"] = zip
        dict["latitude"] = latitude
        dict["longitude"] = longitude
        return dict as NSDictionary
    }
}


class Preferences: NSObject {
    
    class var sharedInstance: Preferences {
        return _sharedPreferences
    }
    func createMergeSettingsFromUserDefaults() -> MergeSettings {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let partialNumber = userDefaults.objectForKey("mergeSettings.partialNumber") as? Bool ?? false
        let emailMatch = userDefaults.objectForKey("mergeSettings.emailMatch") as? Bool ?? true
        let socialProfile = userDefaults.objectForKey("mergeSettings.socialProfile") as? Bool ?? true
        let urlProfile = userDefaults.objectForKey("mergeSettings.urlProfile") as? Bool ?? true
        let instantMessage = userDefaults.objectForKey("mergeSettings.instantMessage") as? Bool ?? true
        let nameAndAddress = userDefaults.objectForKey("mergeSettings.nameAndAddress") as? Bool ?? true
        let nameAndBirthDay = userDefaults.objectForKey("mergeSettings.nameAndBirthDay") as? Bool ?? true
        let nameAndJobTitle = userDefaults.objectForKey("mergeSettings.nameAndJobTitle") as? Bool ?? true
        let nameAndOrganization = userDefaults.objectForKey("mergeSettings.nameAndOrganization") as? Bool ?? true
        let nameAndDepartment = userDefaults.objectForKey("mergeSettings.nameAndDepartment") as? Bool ?? true
        let nameAndNickname = userDefaults.objectForKey("mergeSettings.nameAndNickname") as? Bool ?? true
        return MergeSettings(partialNumber: partialNumber, emailMatch: emailMatch, socialProfile: socialProfile, urlProfile: urlProfile, instantMessage: instantMessage, nameAndAddress: nameAndAddress, nameAndBirthDay: nameAndBirthDay, nameAndJobTitle: nameAndJobTitle, nameAndOrganization: nameAndOrganization, nameAndDepartment: nameAndDepartment, nameAndNickname: nameAndNickname)
    }
    
    func createSearchSettingsFromUserDefaults() -> SearchSettings {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let defaultSearchName = userDefaults.objectForKey("searchSettings.defaultSearchName") as? Bool ?? true
        let defaultSearchJobTitle = userDefaults.objectForKey("searchSettings.defaultSearchJobTitle") as? Bool ?? false
        let defaultSearchDepartment = userDefaults.objectForKey("searchSettings.defaultSearchDepartment") as? Bool ?? false
        let defaultSearchOrganization = userDefaults.objectForKey("searchSettings.defaultSearchOrganization") as? Bool ?? false
        let defaultSearchEmails = userDefaults.objectForKey("searchSettings.defaultSearchEmails") as? Bool ?? false
        let defaultSearchPhoneNumbers = userDefaults.objectForKey("searchSettings.defaultSearchPhoneNumbers") as? Bool ?? true
        let defaultSearchAddress = userDefaults.objectForKey("searchSettings.defaultSearchAddress") as? Bool ?? false
        let defaultSearchSocialProfiles = userDefaults.objectForKey("searchSettings.defaultSearchSocialProfiles") as? Bool ?? false
        let defaultSearchUrlProfiles = userDefaults.objectForKey("searchSettings.defaultSearchUrlProfiles") as? Bool ?? false
        let defaultSearchInstantMessage = userDefaults.objectForKey("searchSettings.defaultSearchInstantMessage") as? Bool ?? false
        
        return SearchSettings(defaultSearchName: defaultSearchName, defaultSearchJobTitle: defaultSearchJobTitle, defaultSearchDepartment: defaultSearchDepartment, defaultSearchOrganization: defaultSearchOrganization, defaultSearchEmails: defaultSearchEmails, defaultSearchPhoneNumbers: defaultSearchPhoneNumbers, defaultSearchAddress: defaultSearchAddress, defaultSearchSocialProfiles: defaultSearchSocialProfiles, defaultSearchUrlProfiles: defaultSearchUrlProfiles, defaultSearchInstantMessage: defaultSearchInstantMessage)
    }
    
    var _mergeSettings: MergeSettings?
    var mergeSettings: MergeSettings {
        if _mergeSettings != nil {
            return _mergeSettings!
        }
        _mergeSettings = createMergeSettingsFromUserDefaults()
        return _mergeSettings!
    }
    func refreshMergeSettings() {
        _mergeSettings = nil
    }
    var _searchSettings: SearchSettings?
    var searchSettings: SearchSettings {
        if _searchSettings != nil {
            return _searchSettings!
        }
        _searchSettings = createSearchSettingsFromUserDefaults()
        return _searchSettings!
    }
    func refreshSearchSettings() {
        _searchSettings = nil
    }
    
    
    var dropboxAccount: String! {
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey(AccountKeys.Dropbox) as? String ?? kUnlinkedAccount
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: AccountKeys.Dropbox)
        }
    }
    var googleDriveAccount: String! {
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey(AccountKeys.GoogleDrive) as? String ?? kUnlinkedAccount
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: AccountKeys.GoogleDrive)
        }
    }
    var facebookAccount: String! {
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey(AccountKeys.Facebook) as? String ?? kUnlinkedAccount
        }
        set {
            NSUserDefaults.standardUserDefaults().setValue(newValue, forKey: AccountKeys.Facebook)
        }
    }
    var _userInfo: SCUserInfo?
    var userInfo: SCUserInfo? {
        get {
            if _userInfo != nil {
                return _userInfo!
            }
            if let dict = NSUserDefaults.standardUserDefaults().objectForKey("userInfo") as? NSDictionary {
                _userInfo = SCUserInfo(dict: dict)
            }
            return _userInfo
        }
        set {
            if newValue != nil {
                NSUserDefaults.standardUserDefaults().setValue(newValue!.convertToDictionary(), forKey: "userInfo")
                _userInfo = newValue!
            } else {
                logError("nil passed to Preferences.userInfo Setter")
            }
        }
    }
    
    var smartFavoritesGenerated: Bool! {
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey("smartFavoritesGenerated") as? Bool ?? false
        }
        set {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "smartFavoritesGenerated")
        }
    }
    var smartGroupsGenerated: Bool! {
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey("smartGroupsGenerated") as? Bool ?? false
        }
        set {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "smartGroupsGenerated")
        }
    }
    var walkThroughDone: Bool {
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey("walkThroughDone") as? Bool ?? false
        }
        set {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "walkThroughDone")
        }
    }
}