//
//  DropboxOperation.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/18/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBook
import CoreData

class DropboxOperation: NSObject, DBRestClientDelegate {
    var progressBlock: ((progress:Float) -> Void)?
    var completionBlock: ((success:Bool) -> Void)?
    var listFilesCompletionBlock: ((results:[DBMetadata]?) -> Void)?
    var restClient: DBRestClient?
    var userInfo: DBAccountInfo?
    
    override init() {
        super.init()
        if self.restClient == nil {
            self.restClient = DBRestClient(session: DBSession.sharedSession())
            self.restClient!.delegate = self
        }
    }
    
    func uploadFile(fileName: String!, path: String!) {
        self.restClient!.uploadFile(fileName, toPath: "/", withParentRev: nil, fromPath: path)
    }
    
    func listFiles() {
        self.restClient!.loadMetadata("/")
    }
    
    func downloadFile(dropboxPath: String!, localPath: String!) {
        self.restClient!.loadFile(dropboxPath, intoPath: localPath)
    }
    
    func getUserInfo() {
        self.restClient!.loadAccountInfo()
    }
    
    func restClient(client: DBRestClient, loadedAccountInfo info: DBAccountInfo) {
        //logDebug(@"UserID: %@ %@", [info displayName], [info userId]);
        self.userInfo = info
        if completionBlock != nil {
            completionBlock!(success:true)
        }
    }
    
    
    func restClient(client: DBRestClient, uploadedFile: String!, from: String!, metadata: DBMetadata) {
        if completionBlock != nil {
            completionBlock!(success:true)
        }
    }
    
    func restClient(client: DBRestClient, loadedFile: String!, contentType: String!, metadata: DBMetadata) {
        if completionBlock != nil {
            completionBlock!(success:true)
        }
    }
    
    func restClient(client: DBRestClient, loadFileFailedWithError error: NSError) {
        logError(String(format: "File download failed with error: %@", error))
        if completionBlock != nil {
            completionBlock!(success:false)
        }
    }
    
    func restClient(client: DBRestClient, loadProgress progress: CGFloat, forFile destPath: String!) {
        if progressBlock != nil {
            progressBlock!(progress: Float(progress))
        }
    }
    
    func restClient(client: DBRestClient, uploadProgress: CGFloat, forFile: String!, from: String!) {
        if progressBlock != nil {
            progressBlock!(progress: Float(uploadProgress))
        }
    }
    
    func restClient(client: DBRestClient, uploadFileFailedWithError error: NSError) {
        logError(String(format: "File upload failed with error: %@", error))
        if completionBlock != nil {
            completionBlock!(success:false)
        }
    }
    
    
    func restClient(client: DBRestClient, loadedMetadata: DBMetadata) {
        if loadedMetadata.isDirectory {
            if let results = loadedMetadata.contents as? [DBMetadata] {
                if listFilesCompletionBlock != nil {
                    listFilesCompletionBlock!(results: results)
                }
                
            }
            
        }
    }
    
    func restClient(client: DBRestClient, loadMetadataFailedWithError error: NSError) {
        logError(String(format: "Error loading metadata: %@", error))
        if listFilesCompletionBlock != nil {
            listFilesCompletionBlock!(results: nil)
        }
        
    }
    
}
