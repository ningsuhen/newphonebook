import Foundation
import AddressBook
import CoreData

class VCard2Downloader: NSObject {
    var data: NSMutableData?
    var downloadSize: Int64 = 0
    var progressBlock: ((progress:Float) -> Void)?
    var completionBlock: ((data:NSData) -> Void)?
    var restoreUrl: String?

    func main() {
        autoreleasepool({
            self.startDownload()
        })
    }

    func startDownload() {
        data = NSMutableData()
        if restoreUrl != nil {
            if let url = NSURL(string: restoreUrl!) {
                var request: NSURLRequest = NSURLRequest(URL: url)
                var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: true)!
                connection.start()
            } else {
                logError("Unable to convert restore url to NSURL")
            }
        } else {
            logError("invalid restore url")
        }
    }

    func connection(connection: NSURLConnection!, didReceiveResponse response: NSHTTPURLResponse!) {
        let statusCode_ = response.statusCode
        if statusCode_ == 200 {
            downloadSize = response.expectedContentLength
        } else {
            logError("status code not 200 \(statusCode_)")
        }
    }

    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data?.appendData(data)
        if progressBlock != nil {
            if downloadSize != 0 {
                let progress = Float(Double(self.data!.length) / Double(downloadSize))
                progressBlock!(progress: progress)
            }

        }
    }

    func connectionDidFinishLoading(connection: NSURLConnection!) {
        if completionBlock != nil {
            completionBlock!(data: data!)
        }
    }
}


class SmartFavorites2Operation: NSOperation {
    var contacts: [SwiftAddressBookPerson]!
    var sortedContacts: [SwiftAddressBookPerson]!
    override func main() {
        autoreleasepool({
            self.iterateContacts()
            logDebug("SmartFavoritesOperation completed")
            self.contacts = nil
            self.sortedContacts = nil
        })
    }

    deinit {
        logDebug("SmartFavoritesOperation deinit")
    }
    override init() {
        super.init()
        logDebug("SmartFavoritesOperation init")
    }

    func iterateContacts() {
        autoreleasepool({
            logDebug("Entered iterateContacts")
            //iterate through contacts - once every 7 days
            if self.contacts == nil {
                return
            }
            for var index = 0; index < self.contacts.count; index++ {
                let person = self.contacts[index]
                if let firstName = person.firstName {
                    if firstName == NSLocalizedString("Identified As Spam", comment: "") || person.phoneNumbers?.count > 20 {
                        //truecaller spam contact. Skip
                        continue
                    }
                }
                var infoCount = 0
                let personRecordID = person.getRecordID()

                if person.hasImage {
                    infoCount += 50
                }

                //Don't consider name as useful information.
                person.checkStringProperty(kABPersonFirstNameProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonLastNameProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonMiddleNameProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonPrefixProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonSuffixProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonNicknameProperty) {
                    infoCount += 5
                }
                person.checkStringProperty(kABPersonFirstNamePhoneticProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonLastNamePhoneticProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonMiddleNamePhoneticProperty) {
                    infoCount += 1
                }
                person.checkStringProperty(kABPersonOrganizationProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonJobTitleProperty) {
                    infoCount += 2
                }
                person.checkStringProperty(kABPersonDepartmentProperty) {
                    infoCount += 2
                }
                person.checkMultiStringProperty(kABPersonEmailProperty) {
                    infoCount += 2
                }
                person.checkMultiDateTimeProperty(kABPersonBirthdayProperty) {
                    infoCount += 5
                }
                person.checkStringProperty(kABPersonNoteProperty) {
                    infoCount += 2
                }

                //                scanMultiDateTime(kABPersonCreationDateProperty)
                //                scanMultiDateTime(kABPersonModificationDateProperty)

                person.checkMultiDictionaryProperty(kABPersonAddressProperty) {
                    infoCount += 3
                }
                person.checkMultiDateTimeProperty(kABPersonDateProperty) {
                    infoCount += 1
                }
                //                scanPersonalInfo(kABPersonAnniversaryLabel)

                person.checkMultiStringProperty(kABPersonPhoneProperty) {
                    infoCount += 20
                }
                person.checkMultiDictionaryProperty(kABPersonInstantMessageProperty) {
                    infoCount += 3
                }
                person.checkMultiDictionaryProperty(kABPersonSocialProfileProperty) {
                    infoCount += 2
                }
                person.checkMultiStringProperty(kABPersonURLProperty) {
                    infoCount += 2
                }
                person.checkMultiStringProperty(kABPersonRelatedNamesProperty) {
                    infoCount += 1
                }

                person.checkPropertyExists(kABPersonAlternateBirthdayProperty) {
                    infoCount += 1
                }
                person.infoCount = infoCount
                person.clearCache()
                if self.cancelled {
                    break
                }
            }
            autoreleasepool({
                if !self.cancelled {
                    if self.sortedContacts == nil {
                        self.sortedContacts = sorted(self.contacts, {
                            (p1: SwiftAddressBookPerson, p2: SwiftAddressBookPerson) -> Bool in
                            return p1.infoCount > p2.infoCount
                        })
                    }

                    var endIndex = self.sortedContacts.count - 1
                    if endIndex < 0 {
                        return //if array is empty
                    }
                    if endIndex > 20 {
                        endIndex = 20
                    }

                    let top10 = self.sortedContacts[0 ... endIndex]
                    for contact in top10 {
                        if let phoneNumbers: [MultivalueEntry] = contact.phoneNumbers {
                            var value = phoneNumbers[0].value
                            var label = phoneNumbers[0].label
                            if phoneNumbers.count > 1 {
                                for phoneNumber in phoneNumbers {
                                    if phoneNumber.label == kDefaultLabel {
                                        value = phoneNumber.value
                                        label = phoneNumber.label
                                        break
                                    }
                                }
                            }
                            Favorite.addEntry(value, abRecordID: contact.getRecordID(), label: label, reason: FavoriteReasons.Contents, priority: Int64(contact.infoCount), save: false)
                        }
                        if self.cancelled {
                            break
                        }
                    }
                    if !self.cancelled {
                        CDModelManager.sharedInstance.commit()
                    }
                    //iterate through call logs - everytime
                }
                logDebug("exiting iterateContacts")
            })

        })
    }
}