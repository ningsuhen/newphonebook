//
//  DummyScanOperation.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/18/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBook
import CoreData

class DummyContactsScanOperation: NSOperation {
    var contacts = [SwiftAddressBookPerson]()
    var dummyRecordIds = [ABRecordID]()
    var progressBlock: ((stats:ContactsProcessingStatus) -> Void)?
    var stats = ContactsProcessingStatus(total: 0, completed: 0)
    override func main() {
        autoreleasepool({
            self.startScan()
        })
    }
    
    func startScan() {
        //Loop all contacts
        self.stats.total = self.contacts.count
        let startTime = NSDate()
        var lastProgressTime = NSDate()
        for (index, person) in enumerate(self.contacts) {
            self.stats.completed = index
            if progressBlock != nil {
                if NSDate().timeIntervalSinceDate(lastProgressTime) > 0.05 {
                    progressBlock!(stats: self.stats)
                    lastProgressTime = NSDate()
                }
            }
            if self.cancelled {
                //cancelled stop all processing
                break
            }
            let personRecordID = person.getRecordID()
            
            //iterate name properties - check if there are useful info
            var usefulInfoPresent = false
            
            
            person.checkStringProperty(kABPersonOrganizationProperty) {
                usefulInfoPresent = true
            }
            person.checkStringProperty(kABPersonJobTitleProperty) {
                usefulInfoPresent = true
            }
            
            person.checkStringProperty(kABPersonDepartmentProperty) {
                usefulInfoPresent = true
            }
            
            person.checkMultiStringProperty(kABPersonEmailProperty) {
                usefulInfoPresent = true
            }
            
            person.checkMultiDateTimeProperty(kABPersonBirthdayProperty) {
                usefulInfoPresent = true
            }
            person.checkStringProperty(kABPersonNoteProperty) {
                usefulInfoPresent = true
            }
            
            person.checkMultiDictionaryProperty(kABPersonAddressProperty) {
                usefulInfoPresent = true
            }
            person.checkMultiDateTimeProperty(kABPersonDateProperty) {
                usefulInfoPresent = true
            }
            
            person.checkMultiStringProperty(kABPersonPhoneProperty) {
                usefulInfoPresent = true
            }
            person.checkMultiDictionaryProperty(kABPersonInstantMessageProperty) {
                usefulInfoPresent = true
            }
            person.checkMultiDictionaryProperty(kABPersonSocialProfileProperty, callback: {
                }, filterCallback: {
                    (label: String, value: NSDictionary?) in
                    if label != "widgets" {
                        if value != nil {
                            let v: Dictionary = value! as Dictionary
                            if let service = v[kABPersonSocialProfileServiceKey] as? String {
                                if service != "widgets" {
                                    if v[kABPersonSocialProfileURLKey] != nil || v[kABPersonSocialProfileUsernameKey] != nil || v[kABPersonSocialProfileUserIdentifierKey] != nil {
                                        usefulInfoPresent = true
                                    }
                                }
                            }
                        }
                        
                    }
            })
            person.checkMultiStringProperty(kABPersonURLProperty) {
                usefulInfoPresent = true
            }
            person.checkMultiStringProperty(kABPersonRelatedNamesProperty) {
                usefulInfoPresent = true
            }
            
            person.checkPropertyExists(kABPersonAlternateBirthdayProperty) {
                usefulInfoPresent = true
            }
            if person.linkedTo != nil || (person.linkedPeople?.count > 1 ){
                usefulInfoPresent = true
            }
            if !usefulInfoPresent {
                logInfo("dummy - \(person.getFullName())")
                self.dummyRecordIds.append(personRecordID)
            }
        }
        
        
        if !self.cancelled {
            self.stats.completed += 1
            if progressBlock != nil {
                progressBlock!(stats: self.stats)
            }
        }
        logInfo("Dummy Scan completed in \(NSDate().timeIntervalSinceDate(startTime))")
    }
}