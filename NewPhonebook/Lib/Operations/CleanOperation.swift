//
//  DummyCleanOperation.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/18/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBook
import CoreData

class CleanOperation: NSOperation {
    var progressBlock: ((stats:ContactsProcessingStatus) -> Void)?
    var stats = ContactsProcessingStatus(total: 0, completed: 0)
    var records = [ABRecordID]()
    var cleanActivity: Activity?
    
    override func main() {
        autoreleasepool({
            self.startClean()
        })
    }
    
    func startClean() {
        stats.total = records.count
        let nsArray = NSMutableArray()
        for recordID in records {
            nsArray.addObject(NSNumber(int: recordID))
            
        }
        self.cleanActivity = Activity.addEntry(String(format: NSLocalizedString("Cleaned %d Duplicates", comment: ""), self.stats.total), type: ActivityTypes.CLEAN_DUMMIES, data: nsArray, save:false)
        
        for recordID in records {
            if let person = SCABManager.swiftAddressBook?.personFromRecordId(recordID) {
                let cleanDate = NSDate()
                let dateString = cleanDate.description.replace("[-\\s\\:\\+]", template: "")
                let vCardPath = String(format: NSLocalizedString("PersonVCard-%d-%@.vcf", comment: ""), recordID, dateString)
                let vCard = person.getVCard()
                var imagePath = IconSet.DefaultAvatar
                //Merge Image
                
                let dirPath = SCABManager.sharedInstance.libraryPath
                vCard.writeToFile(dirPath.stringByAppendingPathComponent(vCardPath), atomically: true)
                if person.hasImage {
                    let imageName = String(format: NSLocalizedString("PersonImage-%d-%@.png", comment: ""), recordID, dateString)
                    let writePath = dirPath.stringByAppendingPathComponent(imageName)
                    UIImagePNGRepresentation(person.image!).writeToFile(writePath, atomically: true)
                    imagePath = imageName
                }
                
                CleanItem.addEntry(person.getName(), abRecordID: recordID, vCardPath: vCardPath, parentActivity: self.cleanActivity!,
                    cleanDate: cleanDate, imagePath: imagePath, save: false)
                
                SCABManager.swiftAddressBook?.removeRecord(person)
            }
            stats.completed += 1
            if self.progressBlock != nil {
                self.progressBlock!(stats: stats)
            }
        }
        
        
        if self.cleanActivity!.cleanItems.count == 0 {
            CDModelManager.context.deleteObject(self.cleanActivity!)
        } else {
            self.cleanActivity!.title = String(format: NSLocalizedString("Cleaned %d Duplicates", comment: ""), self.cleanActivity!.cleanItems.count)
        }
        
        let error = SCABManager.sharedInstance.saveAddressBook()
        if error == nil {
            CDModelManager.sharedInstance.commit()
        } else {
            logError("error saving Clean Activity: \(error)")
        }
    }
}
