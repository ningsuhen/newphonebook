//
//  DuplicateScanOperation.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/18/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBook
import CoreData

class DuplicateScanOperation: NSOperation {
    var contacts = [SwiftAddressBookPerson]()
    var duplicateRecords = Dictionary<ABRecordID, ABRecordID>()
    var progressBlock: ((stats:ContactsProcessingStatus) -> Void)?
    var stats = ContactsProcessingStatus(total: 0, completed: 0)
    
    override func main() {
        autoreleasepool({
            self.startScan()
        })
    }
    
    
    
    func startScan() {
        //Loop all contacts
        self.stats.total = self.contacts.count
        let startTime = NSDate()
        
        let mergeSettings = Preferences.sharedInstance.mergeSettings
        
        var phoneNumberToPersonMap = Dictionary<String, SwiftAddressBookPerson>()
        var emailToPersonMap = Dictionary<String, SwiftAddressBookPerson>()
        var socialProfileToPersonMap = Dictionary<String, SwiftAddressBookPerson>()
        var urlProfileToPersonMap = Dictionary<String, SwiftAddressBookPerson>()
        var instantMessageToPersonMap = Dictionary<String, SwiftAddressBookPerson>()
        var compositeIdentifierToPersonMap = Dictionary<String, SwiftAddressBookPerson>()
        // var fullNameMap = Dictionary<String,SwiftAddressBookPerson>()
        
        var progressRange = self.stats.total
        if mergeSettings.partialNumber {
            progressRange = (self.stats.total / 25)
        }
        //let's not make it heavy, release resources once we are done here
        autoreleasepool({
            let count = self.contacts.count
            var lastProgressTime = NSDate()
            loopOuter: for var index = 0; index < (self.contacts.count); index++ {
                self.stats.completed = Int(Double(progressRange) * (Double(index + 1) / Double(count)))
                //update progress
                if self.progressBlock != nil {
                    if NSDate().timeIntervalSinceDate(lastProgressTime) > 0.05 {
                        self.progressBlock!(stats: self.stats)
                        lastProgressTime = NSDate()
                    }
                }
                //Break if cancelled
                if self.cancelled {
                    break
                }
                let currentPerson = self.contacts[index]
                
                //Scan Phone Numbers
                if let personPhoneSet = currentPerson.phoneNumberUniqueValues {
                    for numberString: String in personPhoneSet {
                        if let existingPerson: SwiftAddressBookPerson = phoneNumberToPersonMap[numberString] {
                            if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                //linked person do nothing
                                logDebug("linked person")
                            } else if existingPerson.skipScan {
                                //existing key points to a duplicate person
                                phoneNumberToPersonMap[numberString] = currentPerson
                            } else {
                                if !existingPerson.hasImage && currentPerson.hasImage {
                                    //we will merge existingPerson to currentPerson instead
                                    self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                    existingPerson.skipScan = true
                                } else {
                                    self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                    currentPerson.skipScan = true
                                    continue loopOuter
                                }
                            }
                        } else {
                            //just add new number
                            phoneNumberToPersonMap[numberString] = currentPerson
                        }
                    }
                }
                if mergeSettings.emailMatch {
                    //Scan emails
                    if let personEmailSet = currentPerson.emailUniqueValues {
                        for emailString: String in personEmailSet {
                            if let existingPerson = emailToPersonMap[emailString] {
                                if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                    logDebug("linked person")
                                } else if existingPerson.skipScan {
                                    //existing key points to a duplicate person
                                    emailToPersonMap[emailString] = currentPerson
                                } else {
                                    if !existingPerson.hasImage && currentPerson.hasImage {
                                        //we will merge existingPerson to currentPerson instead
                                        self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                        existingPerson.skipScan = true
                                    } else {
                                        self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                        currentPerson.skipScan = true
                                        continue loopOuter
                                    }
                                }
                            } else {
                                //just add new number
                                emailToPersonMap[emailString] = currentPerson
                            }
                        }
                    }
                }
                
                if mergeSettings.socialProfile {
                    //Scan social Profiles
                    if let personSocialProfileSet = currentPerson.socialProfileUniqueValues {
                        for socialProfile: String in personSocialProfileSet {
                            if let existingPerson = socialProfileToPersonMap[socialProfile] {
                                if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                    logDebug("linked person")
                                } else if existingPerson.skipScan {
                                    //existing key points to a duplicate person
                                    socialProfileToPersonMap[socialProfile] = currentPerson
                                } else {
                                    if !existingPerson.hasImage && currentPerson.hasImage {
                                        //we will merge existingPerson to currentPerson instead
                                        self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                        existingPerson.skipScan = true
                                    } else {
                                        self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                        currentPerson.skipScan = true
                                        continue loopOuter
                                    }
                                }
                            } else {
                                //just add new number
                                socialProfileToPersonMap[socialProfile] = currentPerson
                            }
                        }
                    }
                }
                
                if mergeSettings.urlProfile {
                    //Scan url Profiles
                    if let personUrlProfileSet = currentPerson.urlProfileUniqueValues {
                        for urlProfile: String in personUrlProfileSet {
                            if let existingPerson = urlProfileToPersonMap[urlProfile] {
                                if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                    logDebug("linked person")
                                } else if existingPerson.skipScan {
                                    //existing key points to a duplicate person
                                    urlProfileToPersonMap[urlProfile] = currentPerson
                                } else {
                                    if !existingPerson.hasImage && currentPerson.hasImage {
                                        //we will merge existingPerson to currentPerson instead
                                        self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                        existingPerson.skipScan = true
                                    } else {
                                        self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                        currentPerson.skipScan = true
                                        continue loopOuter
                                    }
                                }
                            } else {
                                urlProfileToPersonMap[urlProfile] = currentPerson
                            }
                        }
                    }
                }
                
                if mergeSettings.instantMessage {
                    //Scan Instant Messenger
                    if let instantMessageSet = currentPerson.instantMessageUniqueValues {
                        for instantMessage: String in instantMessageSet {
                            if let existingPerson = instantMessageToPersonMap[instantMessage] {
                                if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                    logDebug("linked person")
                                } else if existingPerson.skipScan {
                                    //existing key points to a duplicate person
                                    instantMessageToPersonMap[instantMessage] = currentPerson
                                } else {
                                    if !existingPerson.hasImage && currentPerson.hasImage {
                                        //we will merge existingPerson to currentPerson instead
                                        self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                        existingPerson.skipScan = true
                                    } else {
                                        self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                        currentPerson.skipScan = true
                                        continue loopOuter
                                    }
                                }
                            } else {
                                instantMessageToPersonMap[instantMessage] = currentPerson
                            }
                        }
                    }
                    
                }
                
                
                
                if mergeSettings.nameAndAddress {
                    //Scan address Messenger
                    if let addressSet = currentPerson.addressUniqueValues {
                        for addressString in addressSet {
                            let uniqueCompositeIdentifier = currentPerson.getFullName() + ":address:" + addressString
                            if let existingPerson = compositeIdentifierToPersonMap[uniqueCompositeIdentifier] {
                                if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                    logDebug("linked person")
                                } else if existingPerson.skipScan {
                                    //existing key points to a duplicate person
                                    compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                                } else {
                                    if !existingPerson.hasImage && currentPerson.hasImage {
                                        //we will merge existingPerson to currentPerson instead
                                        self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                        existingPerson.skipScan = true
                                    } else {
                                        self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                        currentPerson.skipScan = true
                                        continue loopOuter
                                    }
                                }
                            } else {
                                compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                            }
                        }
                    }
                }
                
                if mergeSettings.nameAndBirthDay {
                    //Scan date of birth
                    if let birthday = currentPerson.birthday {
                        
                        let uniqueCompositeIdentifier = currentPerson.getFullName() + ":birthday:" + birthday.description
                        
                        if let existingPerson = compositeIdentifierToPersonMap[uniqueCompositeIdentifier] {
                            if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                logDebug("linked person")
                            } else if existingPerson.skipScan {
                                //existing key points to a duplicate person
                                compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                            } else {
                                if !existingPerson.hasImage && currentPerson.hasImage {
                                    //we will merge existingPerson to currentPerson instead
                                    self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                    existingPerson.skipScan = true
                                } else {
                                    self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                    currentPerson.skipScan = true
                                    continue loopOuter
                                }
                            }
                        } else {
                            compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                        }
                        
                    }
                }
                if mergeSettings.nameAndJobTitle {
                    //Scan date of birth
                    if let jobTitle = currentPerson.jobTitle {
                        let uniqueCompositeIdentifier = currentPerson.getFullName() + ":job:" + jobTitle
                        if let existingPerson = compositeIdentifierToPersonMap[uniqueCompositeIdentifier] {
                            if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                logDebug("linked person")
                            } else if existingPerson.skipScan {
                                //existing key points to a duplicate person
                                compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                            } else {
                                if !existingPerson.hasImage && currentPerson.hasImage {
                                    //we will merge existingPerson to currentPerson instead
                                    self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                    existingPerson.skipScan = true
                                } else {
                                    self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                    currentPerson.skipScan = true
                                    continue loopOuter
                                }
                            }
                        } else {
                            compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                        }
                    }
                }
                
                if mergeSettings.nameAndOrganization {
                    //Scan date of birth
                    if let organization = currentPerson.organization {
                        let uniqueCompositeIdentifier = currentPerson.getFullName() + ":organization:" + organization
                        if let existingPerson = compositeIdentifierToPersonMap[uniqueCompositeIdentifier] {
                            if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                logDebug("linked person")
                            } else if existingPerson.skipScan {
                                //existing key points to a duplicate person
                                compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                            } else {
                                if !existingPerson.hasImage && currentPerson.hasImage {
                                    //we will merge existingPerson to currentPerson instead
                                    self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                    existingPerson.skipScan = true
                                } else {
                                    self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                    currentPerson.skipScan = true
                                    continue loopOuter
                                }
                            }
                        } else {
                            compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                        }
                    }
                }
                
                if mergeSettings.nameAndNickname {
                    //Scan date of birth
                    if let nickname = currentPerson.nickname {
                        let uniqueCompositeIdentifier = currentPerson.getFullName() + ":nickname:" + nickname
                        if let existingPerson = compositeIdentifierToPersonMap[uniqueCompositeIdentifier] {
                            if currentPerson.linkedTo == existingPerson.getRecordID() || existingPerson.linkedTo ==  currentPerson.getRecordID(){
                                logDebug("linked person")
                            } else if existingPerson.skipScan {
                                //existing key points to a duplicate person
                                compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                            } else {
                                if !existingPerson.hasImage && currentPerson.hasImage {
                                    //we will merge existingPerson to currentPerson instead
                                    self.duplicateRecords[existingPerson.getRecordID()] = currentPerson.getRecordID()
                                    existingPerson.skipScan = true
                                } else {
                                    self.duplicateRecords[currentPerson.getRecordID()] = existingPerson.getRecordID()
                                    currentPerson.skipScan = true
                                    continue loopOuter
                                }
                            }
                        } else {
                            compositeIdentifierToPersonMap[uniqueCompositeIdentifier] = currentPerson
                        }
                    }
                }
                
                
            }
        })
        
        logInfo("Duplicate Middle Duration: \(NSDate().timeIntervalSinceDate(startTime))")
        
        if mergeSettings.partialNumber {
            
            let phoneNumbers = phoneNumberToPersonMap.keys.array
            let ratio = self.stats.total / phoneNumbers.count
            
            let count = phoneNumbers.count
            let secondProgressRange = (self.stats.total / 25) * 24
            
            loopOuter: for var index = 0; index < (phoneNumbers.count - 1); index++ {
                self.stats.completed = progressRange + Int(Double(secondProgressRange) * (Double(index + 1) / Double(count)))
                //update progress
                if self.progressBlock != nil {
                    self.progressBlock!(stats: self.stats)
                }
                //Break if cancelled
                if self.cancelled {
                    break
                }
                
                let numberString = phoneNumbers[index]
                let numberRegex = NSRegularExpression(pattern: "^\\+[0-9]{0,5}\(numberString)$", options: NSRegularExpressionOptions(0), error: nil)
                
                for var innerIndex = index + 1; innerIndex < (phoneNumbers.count); innerIndex++ {
                    let number2String = phoneNumbers[innerIndex]
                    let number2Regex = NSRegularExpression(pattern: "^\\+[0-9]{0,5}\(number2String)$", options: NSRegularExpressionOptions(0), error: nil)
                    if number2String.rangeOfString(numberString) != nil {
                        //number2 contains number
                        //check if person's number is a substring of person2's number
                        let range: NSRange = NSMakeRange(0, countElements(numberString))
                        if number2Regex?.matchesInString(numberString, options: NSMatchingOptions(0), range: range).count > 0 {
                            let person = phoneNumberToPersonMap[numberString]!
                            let person2 = phoneNumberToPersonMap[number2String]!
                            //check if person2 has no image but the other does
                            if person == person2 {
                                logDebug("same person")
                            }
                            else if person.linkedTo == person2.getRecordID() || person2.linkedTo ==  person.getRecordID(){
                                logDebug("linked person")
                            }
                            else {
                                if !person2.hasImage && person.hasImage {
                                    logInfo("\(number2String) ~> \(numberString)")
                                    self.duplicateRecords[person2.getRecordID()] = person.getRecordID()
                                    person2.skipScan = true
                                    continue
                                } else {
                                    logDebug("\(numberString) ~> \(number2String)")
                                    self.duplicateRecords[person.getRecordID()] = person2.getRecordID()
                                    person.skipScan = true
                                    continue loopOuter
                                }
                            }
                            
                        }
                        logInfo("\(number2String) ~? \(numberString)")
                        
                    } else if numberString.rangeOfString(number2String) != nil {
                        //number contains number2
                        //check if person2's number is a substring of person's number
                        let range: NSRange = NSMakeRange(0, countElements(number2String))
                        if numberRegex?.matchesInString(number2String, options: NSMatchingOptions(0), range: range).count > 0 {
                            let person = phoneNumberToPersonMap[numberString]!
                            let person2 = phoneNumberToPersonMap[number2String]!
                            if person == person2 {
                                logDebug("same person")
                            }
                            else if person.linkedTo == person2.getRecordID() || person2.linkedTo ==  person.getRecordID(){
                                logDebug("linked person")
                            }
                            else {
                                //check if person has no image but the other does
                                if !person.hasImage && person2.hasImage {
                                    logInfo("\(numberString) ~> \(number2String)")
                                    self.duplicateRecords[person.getRecordID()] = person2.getRecordID()
                                    person.skipScan = true
                                    continue loopOuter
                                } else {
                                    logInfo("\(number2String) ~> \(numberString)")
                                    self.duplicateRecords[person2.getRecordID()] = person.getRecordID()
                                    person2.skipScan = true
                                    continue
                                }
                            }
                        }
                        logInfo("\(number2String) ~? \(numberString)")
                    }
                    
                }
            }
        }
        if !self.cancelled {
            self.stats.completed = self.stats.total
            if progressBlock != nil {
                progressBlock!(stats: self.stats)
            }
        }
        logInfo("Duplicate Scan Duration: \(NSDate().timeIntervalSinceDate(startTime))")
    }
}
