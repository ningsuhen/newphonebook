//
//  MergeOperation.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/18/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBook
import CoreData

class MergeOperation: NSOperation {
    var progressBlock: ((stats:ContactsProcessingStatus) -> Void)?
    var stats = ContactsProcessingStatus(total: 0, completed: 0)
    var recordsMap = Dictionary<ABRecordID, ABRecordID>()
    var mergeActivity: Activity?
    
    
    override func main() {
        autoreleasepool({
            self.stats.total = self.recordsMap.count
            let nsDict = NSMutableDictionary()
            for (key, value) in self.recordsMap {
                nsDict.setObject(NSNumber(int: value), forKey: NSNumber(int: key))
            }
            self.mergeActivity = Activity.addEntry(String(format: NSLocalizedString("Merged %d Duplicates", comment: ""), self.stats.total), type: ActivityTypes.MERGE_DUPLICATES, data: nsDict, save:false)
            
            self.mergeIteration(self.recordsMap)
            if self.mergeActivity!.mergeItems.count == 0 {
                CDModelManager.context.deleteObject(self.mergeActivity!)
            } else {
                self.mergeActivity!.title = String(format: NSLocalizedString("Merged %d Duplicates", comment: ""), self.mergeActivity!.mergeItems.count)
            }
            let error = SCABManager.sharedInstance.saveAddressBook()
            if error == nil {
                CDModelManager.sharedInstance.commit()
            } else {
                logError("error saving address book after merge: \(error)")
            }
        })
        
    }
    
    func mergeIteration(recordsMap: Dictionary<ABRecordID, ABRecordID>) {
        autoreleasepool({
            let keys = recordsMap.keys
            let values = recordsMap.values
            var deferredRecords = Dictionary<ABRecordID, ABRecordID>()
            var count = 0
            
            for key in keys {
                
                let value = recordsMap[key]!
                //check if this
                if contains(values, key) {
                    deferredRecords[key] = value
                    continue
                }
                autoreleasepool({
                    if let targetPerson = SCABManager.swiftAddressBook?.personFromRecordId(value) {
                        if let duplicatePerson = SCABManager.swiftAddressBook?.personFromRecordId(key) {
                            
                            let mergeDate = NSDate()
                            let dateString = mergeDate.description.replace("[-\\s\\:\\+]", template: "")
                            
                            let dcRecordID = duplicatePerson.getRecordID()
                            let dcVCardPath = NSLocalizedString("PersonVCard-\(dcRecordID)-\(dateString).vcf", comment: "")
                            var dcImagePath = IconSet.DefaultAvatar
                            let dcVCard = duplicatePerson.getVCard()
                            
                            
                            let tcRecordID = targetPerson.getRecordID()
                            let tcVCardPath = NSLocalizedString("PersonVCard-\(tcRecordID)-\(dateString).vcf", comment: "")
                            var tcImagePath = IconSet.DefaultAvatar
                            let tcVCard = targetPerson.getVCard()
                            
                            let dirPath = SCABManager.sharedInstance.libraryPath
                            
                            dcVCard.writeToFile(dirPath.stringByAppendingPathComponent(dcVCardPath), atomically: true)
                            tcVCard.writeToFile(dirPath.stringByAppendingPathComponent(tcVCardPath), atomically: true)
                            
                            if duplicatePerson.hasImage {
                                let imageName = NSLocalizedString("PersonImage-\(dcRecordID)-\(dateString).png", comment: "")
                                let writePath = dirPath.stringByAppendingPathComponent(imageName)
                                UIImagePNGRepresentation(duplicatePerson.image!).writeToFile(writePath, atomically: true)
                                dcImagePath = imageName
                                //Merge: Copy Image
                                if targetPerson.hasImage {
                                    targetPerson.setImage(duplicatePerson.image!)
                                }
                            }
                            if targetPerson.hasImage {
                                let imageName = NSLocalizedString("PersonImage-\(tcRecordID)-\(dateString).png", comment: "")
                                let writePath = dirPath.stringByAppendingPathComponent(imageName)
                                UIImagePNGRepresentation(targetPerson.image!).writeToFile(writePath, atomically: true)
                                tcImagePath = imageName
                            }
                            MergeItem.addEntry(self.mergeActivity!, mergeDate: mergeDate, dcName: duplicatePerson.getName(), dcRecordID: duplicatePerson.getRecordID(), dcVCardPath: dcVCardPath, dcImagePath: dcImagePath, tcName: targetPerson.getName(), tcRecordID: tcRecordID, tcVCardPath: tcVCardPath, tcImagePath: tcImagePath, save:false)
                            
                            //TODO: needs rework to link contacts instead of merging.
                            targetPerson.mergeContact(duplicatePerson)
                            SCABManager.swiftAddressBook?.removeRecord(duplicatePerson)
                            
                            
                            
                            self.stats.completed += 1
                            if self.progressBlock != nil {
                                self.progressBlock!(stats: self.stats)
                            }
                        } else {
                            logError("dupe Person record Id doesn't exist")
                        }
                    } else {
                        logError("target Person record Id doesn't exist")
                    }
                })
            }
            
            if deferredRecords.count > 0 {
                logDebug("deferred \(deferredRecords)")
                self.mergeIteration(deferredRecords)
            }
        })
    }
}