//
//  SearchOperation.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/18/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBook
import CoreData



class SearchOperation: NSOperation {

    var searchText: NSString = ""
    var scope: NSString = kSearchDefaultScopeLabel
    var contacts = [SwiftAddressBookPerson]()
    //    var searchSettings:SearchSettings!

    var filteredContacts = [SwiftAddressBookPerson]()
    override func main() {
        autoreleasepool({
            var searchSettings = Preferences.sharedInstance.searchSettings
            if self.scope != kSearchDefaultScopeLabel {
                searchSettings = SearchSettings(defaultSearchName: false, defaultSearchJobTitle: false, defaultSearchDepartment: false, defaultSearchOrganization: false, defaultSearchEmails: false, defaultSearchPhoneNumbers: false, defaultSearchAddress: false, defaultSearchSocialProfiles: false, defaultSearchUrlProfiles: false, defaultSearchInstantMessage: false)
                if self.scope == LocalizedSearchScopes.Name {
                    searchSettings.defaultSearchName = true
                } else if self.scope == LocalizedSearchScopes.Phone {
                    searchSettings.defaultSearchPhoneNumbers = true
                } else if self.scope == LocalizedSearchScopes.Job {
                    searchSettings.defaultSearchJobTitle = true
                    searchSettings.defaultSearchDepartment = true
                    searchSettings.defaultSearchOrganization = true
                } else if self.scope == LocalizedSearchScopes.Social {
                    searchSettings.defaultSearchSocialProfiles = true
                    searchSettings.defaultSearchUrlProfiles = true
                } else if self.scope == LocalizedSearchScopes.Address {
                    searchSettings.defaultSearchAddress = true
                }
            }
            let startTime = NSDate()
            let searchText = self.searchText.lowercaseString
            var filteredContacts = [SwiftAddressBookPerson]()
            for person in self.contacts {
                if self.cancelled {
                    return
                }
                if person.linkedTo != nil {
                    continue
                }
                if self.doSearch(person, searchText: searchText, settings: searchSettings) {
                    // found in person
                    filteredContacts.append(person)
                    continue
                } else if person.linkedPeople != nil {
                    // person has linked contacts
                    for linkedPerson in person.linkedPeople! {
                        // search in linked contacts
                        if self.doSearch(linkedPerson, searchText: searchText, settings: searchSettings) {
                            // found in linked contact
                            filteredContacts.append(person)
                            break
                        }
                    }
                }
            }
            self.filteredContacts = filteredContacts
            logDebug("filter time elapsed for searchtext \(self.searchText): \(NSDate().timeIntervalSinceDate(startTime))")
        })
    }

   


    func doSearch(person: SwiftAddressBookPerson, searchText: String, settings: SearchSettings) -> Bool {
        if settings.defaultSearchName {
            //0.150345981121063 to 5.80549240112305e-05
            if person.fullNameForSearch.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
                return true
            }
        }

        if settings.defaultSearchEmails {
            //0.279181003570557 to 0.000590026378631592
            if let emails = person.emailUniqueValues {
                for email in emails {
                    if email.rangeOfString(searchText) != nil {
                        return true
                    }
                }
            }
        }

        if settings.defaultSearchPhoneNumbers {
            //0.21637099981308 to 6.49690628051758e-05
            if let phoneNumbers = person.phoneNumberUniqueValues {
                for phoneNumber in phoneNumbers {
                    if phoneNumber.rangeOfString(searchText) != nil {
                        return true
                    }
                }
            }
        }


        if settings.defaultSearchJobTitle {
            //0.567950010299683 to 0.0209410190582275
            if let jobTitle = person.jobTitle {
                if jobTitle.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
                    return true
                }
            }
        }
        if settings.defaultSearchDepartment {
            if let department = person.department {
                if department.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
                    return true
                }
            }
        }
        if settings.defaultSearchOrganization {
            if let organization = person.organization {
                if organization.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch) != nil {
                    return true
                }
            }
        }


        if settings.defaultSearchSocialProfiles {
            //0.688919007778168 to 0.183387994766235
            if let socialProfiles = person.socialProfileUniqueValues {
                for socialProfile in socialProfiles {
                    if socialProfile.rangeOfString(searchText) != nil {
                        return true
                    }
                }
            }
        }
        if settings.defaultSearchUrlProfiles {
            if let socialProfiles = person.urlProfileUniqueValues {
                for socialProfile in socialProfiles {
                    if socialProfile.rangeOfString(searchText) != nil {
                        return true
                    }
                }
            }
        }

        if settings.defaultSearchAddress {
            // 0.405813992023468 to 0.156459987163544
            if let addresses = person.addressUniqueValues {
                for address in addresses {
                    if address.rangeOfString(searchText) != nil {
                        return true
                    }
                }
            }
        }
        if settings.defaultSearchInstantMessage {
            // 0.405813992023468 to 0.156459987163544
            if let values = person.instantMessageUniqueValues {
                for value in values {
                    if value.rangeOfString(searchText) != nil {
                        return true
                    }
                }
            }
        }
        return false
    }

  
}

