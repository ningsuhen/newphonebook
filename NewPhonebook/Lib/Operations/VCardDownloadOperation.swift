/*//
//  VCardDownloadOperation.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/18/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import AddressBook
import CoreData
class VCardDownloader: NSObject {
    var data: NSMutableData?
    var downloadSize: Int64 = 0
    var progressBlock: ((progress:Float) -> Void)?
    var completionBlock: ((data:NSData) -> Void)?
    var restoreUrl: String?
    
    func main() {
        autoreleasepool({
            self.startDownload()
        })
    }
    func startDownload(){
        data = NSMutableData()
        if restoreUrl != nil {
            if let url = NSURL(string: restoreUrl!) {
                var request: NSURLRequest = NSURLRequest(URL: url)
                var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: true)!
                connection.start()
            } else {
                logError("Unable to convert restore url to NSURL")
            }
        } else {
            logError("invalid restore url")
        }
    }
    
    func connection(connection: NSURLConnection!, didReceiveResponse response: NSHTTPURLResponse!) {
        let statusCode_ = response.statusCode
        if statusCode_ == 200 {
            downloadSize = response.expectedContentLength
        } else {
            logError("status code not 200 \(statusCode_)")
        }
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.data?.appendData(data)
        if progressBlock != nil {
            if downloadSize != 0 {
                let progress = Float(Double(self.data!.length) / Double(downloadSize))
                progressBlock!(progress: progress)
            }
            
        }
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        if completionBlock != nil {
            completionBlock!(data: data!)
        }
    }
}*/
