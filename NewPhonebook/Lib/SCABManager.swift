//
//  CDModelManager.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/26/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import CoreData
import AddressBook
import UIKit

private let _sharedSCABManagerInstance = SCABManager()

typealias ABExecutionPriority = NSOperationQueuePriority

struct ABExecutionPriorities {
    static let UIDEPENDENT: ABExecutionPriority = NSOperationQueuePriority.VeryHigh
    static let NORMAL: ABExecutionPriority = NSOperationQueuePriority.Normal
    static let BACKGROUND: ABExecutionPriority = NSOperationQueuePriority.VeryLow
}

typealias ABExecutionType = Int

struct ABExecutionTypes {
    static let CANCELLABLE: ABExecutionType = 0
    static let NONCANCELLABLE: ABExecutionType = 1
    static let BACKGROUND_DEFERABLE: ABExecutionType = 2
}

@objc
class SCABManager: NSObject, iCloudDelegate, DBRestClientDelegate {
    //Lazy Queues
    lazy var searchQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Search Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
        }()
    lazy var dupeScanQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Duplicate Scan Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
        }()
    lazy var dummyScanQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Dummy Contacts Scan Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
        }()
    
    lazy var mergeQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Merge Operation Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
        }()
    lazy var cleanQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Clean Operation Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
        }()
    
    
    
    lazy var favoriteProcessingQueue: NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Favorites Processing Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
        }()
    
    class func addressBookBlock(block: () -> Void, priority: ABExecutionPriority! = ABExecutionPriorities.NORMAL, type: ABExecutionType! = ABExecutionTypes.CANCELLABLE, waitUntilFinished: Bool = false, completion: (() -> Void)? = nil) {
        let abManager = SCABManager.sharedInstance
        abManager.addressBook?.addBlock(block, waitUntilFinished: waitUntilFinished, completion: completion)
    }
    
    
    func getThreadSafeFullName(person: SwiftAddressBookPerson) {
        
    }
    
    
    class WeakHandler {
        weak var value: SCDataChangeDelegate?
        init(value: SCDataChangeDelegate) {
            self.value = value
        }
    }
    
    var contactChangeHandlers = [WeakHandler]()
    func registerContactsChangeHandler(handler: SCDataChangeDelegate) {
        logDebug("registering controller for contactChangeHandlers")
        contactChangeHandlers.append(WeakHandler(value: handler))
        if _contacts.count > 0 {
            handler.onContactsChange(contacts)
        }
    }
    
    
    
    //Variables
    var _contacts = [SwiftAddressBookPerson]()
    var contacts: [SwiftAddressBookPerson] {
        get {
            return _contacts
        }
        set {
            _contacts = newValue
            // call all handlers
            // need to iterate in reverse order so that deallocated handlers can be removed
            for index in stride(from: contactChangeHandlers.count - 1, through: 0, by: -1) {
                let weakHandler = contactChangeHandlers[index]
                if let handler = weakHandler.value {
                    logDebug("calling handler at index \(index)")
                    handler.onContactsChange(contacts)
                } else {
                    logDebug("removing handler at index \(index)")
                    contactChangeHandlers.removeAtIndex(index)
                }
            }
        }
    }
    var dbOperation: DropboxOperation?
    var _documentsPath: String?
    var documentsPath: String {
        if self._documentsPath != nil {
            return self._documentsPath!
        }
        let nsDocumentsDirectory = NSSearchPathDirectory.DocumentDirectory
        let nsUserDomainMask = NSSearchPathDomainMask.UserDomainMask
        if let paths = NSSearchPathForDirectoriesInDomains(nsDocumentsDirectory, nsUserDomainMask, true) {
            if paths.count > 0 {
                if let dirPath = paths[0] as? String {
                    self._documentsPath = dirPath
                }
            }
        }
        
        if self._documentsPath == nil {
            logError("Unable to find Documents Path")
        }
        return self._documentsPath!
    }
    
    var _libraryPath: String?
    var libraryPath: String {
        if self._libraryPath != nil {
            return self._libraryPath!
        }
        let nsLibraryDirectory = NSSearchPathDirectory.LibraryDirectory
        let nsUserDomainMask = NSSearchPathDomainMask.UserDomainMask
        if let paths = NSSearchPathForDirectoriesInDomains(nsLibraryDirectory, nsUserDomainMask, true) {
            if paths.count > 0 {
                if let dirPath = paths[0] as? String {
                    var libPath = dirPath.stringByAppendingPathComponent("SCApplicationData")
                    var error: NSError?
                    if (!NSFileManager.defaultManager().fileExistsAtPath(libPath)) {
                        NSFileManager.defaultManager() .createDirectoryAtPath(libPath, withIntermediateDirectories: false, attributes: nil, error: &error)
                    }
                    self._libraryPath = libPath
                }
            }
        }
        if self._libraryPath == nil {
            logError("Unable to find Library Path")
        }
        return self._libraryPath!
    }
    
    var _sharedICloud: iCloud?
    var sharedICloud: iCloud {
        if self._sharedICloud != nil {
            return self._sharedICloud!
        }
        iCloud.sharedCloud().delegate = self // setDelegate:self
        iCloud.sharedCloud().verboseLogging = true
        iCloud.sharedCloud().setupiCloudDocumentSyncWithUbiquityContainer(nil)
        self._sharedICloud = iCloud.sharedCloud()
        return self._sharedICloud!
    }
    
    
    var _addressBook: SwiftAddressBook?
    var addressBook: SwiftAddressBook? {
        if _addressBook != nil {
            return _addressBook!
        }
        
        _addressBook = SwiftAddressBook()
        
        return _addressBook
    }
    
    
    var dropboxAccessCompletionBlock: (() -> Void)?
    lazy var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    
    var addressBookExternalChanged = false
    
    func addressBookDidChangeExternalNotification(notification: NSNotification) {
        logDebug("notification called")
        if !addressBook!.hasUnsavedChanges() {
            if timer != nil {
                timer!.invalidate()
            }
            timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "handleABExternalChangeTimer:", userInfo: nil, repeats: true)
        } else {
            addressBookExternalChanged = true
            logError("External Notification called but has unsaved Changes")
        }
    }
    
    func handleABExternalChangeTimer(timer: NSTimer) {
        self.refreshContacts()
        timer.invalidate()
    }
    
    var timer: NSTimer?
    
    var delayAddressBookPermission = false
    override init() {
        super.init()
        logDebug("registering for external change notification")
        ABExternalChangeCallbackBridge.sharedInstance()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "addressBookDidChangeExternalNotification:", name: "AddressBookDidChangeExternalNotification", object: nil)//
        if (FBSession.activeSession().state == FBSessionState.Open || FBSession.activeSession().state == FBSessionState.CreatedTokenLoaded) {
            self.getAddressBookPermission()
        } else {
            delayAddressBookPermission = true
        }
        
        //Dropbox
        let dbSession = DBSession(appKey: "npyxsdtx8l4cqnq", appSecret: "j2ssfwaupmenklv", root: kDBRootAppFolder) // either kDBRootAppFolder or kDBRootDropbox
        DBSession.setSharedSession(dbSession)
        
    }
    
    func getAddressBookPermission() {
        self.addressBook?.requestAccessWithCompletion({
            (success, error) -> Void in
            if success && error == nil {
                logDebug("Access granted for Address Book")
                //have obtained access to address book, load it
                self.addressBook?.getAllUniquePersons({
                    (persons: [SwiftAddressBookPerson]) in
                    self.contacts = persons
                    self.prepopulateData()
                })
            } else {
                //no success. Optionally evaluate error
                logError("Access denied for Address Book")
                let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
                appDelegate.showAddressBookPermissionAlert()
            }
        })
    }
    /**
    This function will be called when facebook login is completed
    */
    func facebookLoginCallback() {
        if delayAddressBookPermission {
            logDebug("delayAddressBookPermission, calling getAddressBookPermission")
            getAddressBookPermission()
        } else {
            logDebug("delayAddressBookPermission = false, calling prepopulateData")
            prepopulateData()
        }
    }
    
    func prepopulateData() {
        logDebug("prepopulate Data called")
        if !Preferences.sharedInstance.smartFavoritesGenerated {
            logDebug("inside  !Preferences.sharedInstance.smartFavoritesGenerated")
            //let's run this only when the contacts are loaded
            if self.contacts.count == 0 {
                return
            }
            if Preferences.sharedInstance.userInfo == nil {
                return
            }
            logDebug("calling   generateSmartFavorites")
            self.generateSmartFavorites() {
                Preferences.sharedInstance.smartFavoritesGenerated = true
            }
        }
        if !Preferences.sharedInstance.smartGroupsGenerated {
            logDebug("inside  !smartGroupsGenerated")
            //let's run this only when facebook login is done
            if Preferences.sharedInstance.userInfo == nil {
                return
            }
            
            logDebug("calling   generateSmartGroups")
            self.generateSmartGroups() {
                Preferences.sharedInstance.smartGroupsGenerated = true
            }
        }
    }
    
    func personExistsWithRecordID(recordID: ABRecordID) -> Bool {
        return (addressBook?.personFromRecordId(recordID) != nil)
    }
    
    
    func dummy() {
        
    }
    
    func refreshContacts() {
        logDebug("called refreshContacts")
        var contacts: [SwiftAddressBookPerson]?
        //access addressbook with dedicated thread
        self.addressBook?.revert()
        self.addressBook?.getAllUniquePersons({
            (persons: [SwiftAddressBookPerson]) in
            logDebug("setting new contacts")
            self.contacts = persons
        })
    }
    
    func saveAddressBook() -> CFError? {
        if let error = addressBook?.save() {
            logError("error saving addressBook in SCABManager: \(error)")
            return error
        }
        return nil
    }
    
    class var sharedInstance: SCABManager {
        return _sharedSCABManagerInstance
    }
    
    class var swiftAddressBook: SwiftAddressBook? {
        return sharedInstance.addressBook
    }
    
    
    func logoutFacebook() {
        FBSession.activeSession().closeAndClearTokenInformation()
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        var storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var viewController: LoginViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as LoginViewController
        appDelegate.window!.makeKeyAndVisible()
        appDelegate.window!.rootViewController!.presentViewController(viewController, animated: false, completion: nil)
    }
    
    func getSmartGroup(search: SCSearch, completion: (results:[SwiftAddressBookPerson]) -> Void) {
        self.searchQueue.cancelAllOperations()
        for op in self.searchQueue.operations as [NSOperation] {
            op.waitUntilFinished()
        }
        let op = SearchOperation()
        op.searchText = search.text
        op.scope = search.scope
        op.contacts = self.contacts
        op.queuePriority = NSOperationQueuePriority.High
        op.completionBlock = {
            NSOperationQueue.mainQueue().addOperationWithBlock({
                completion(results: op.filteredContacts)
            })
        }
        self.searchQueue.addOperation(op)
    }
    
    func filterContacts(contacts: [SwiftAddressBookPerson], text: String, scope: String, completion: (results:[SwiftAddressBookPerson]) -> Void) {
        self.searchQueue.cancelAllOperations()
        for op in self.searchQueue.operations as [NSOperation] {
            op.waitUntilFinished()
        }
        let op = SearchOperation()
        op.searchText = text
        op.scope = scope
        op.contacts = contacts
        op.queuePriority = NSOperationQueuePriority.High
        op.completionBlock = {
            if !op.cancelled {
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    completion(results: op.filteredContacts)
                })
            }
        }
        self.searchQueue.addOperation(op)
    }
    
    
    func scanDuplicates(completion: (duplicates:Dictionary<ABRecordID, ABRecordID>) -> Void, progress: ((stats:ContactsProcessingStatus) -> Void)?) {
        self.dupeScanQueue.cancelAllOperations()
        let op = DuplicateScanOperation()
        op.contacts = self.contacts
        op.queuePriority = NSOperationQueuePriority.High
        
        op.progressBlock = {
            (stats: ContactsProcessingStatus) in
            NSOperationQueue.mainQueue().addOperationWithBlock({
                if progress != nil {
                    progress!(stats: stats)
                }
            })
        }
        op.completionBlock = {
            NSOperationQueue.mainQueue().addOperationWithBlock({
                // if !op.cancelled{
                completion(duplicates: op.duplicateRecords)
                // }
            })
        }
        self.dupeScanQueue.addOperation(op)
        //        op.waitUntilFinished()
    }
    
    func cancelDuplicateScan() {
        self.dupeScanQueue.cancelAllOperations()
    }
    
    
    func mergeDuplicates(recordsMap: Dictionary<ABRecordID, ABRecordID>, progress: ((stats:ContactsProcessingStatus) -> Void)?, completion: (() -> Void)) {
        self.mergeQueue.cancelAllOperations()
        
        let op = MergeOperation()
        op.recordsMap = recordsMap
        op.queuePriority = NSOperationQueuePriority.High
        op.progressBlock = {
            (stats: ContactsProcessingStatus) in
            NSOperationQueue.mainQueue().addOperationWithBlock({
                if progress != nil {
                    progress!(stats: stats)
                }
            })
        }
        op.completionBlock = {
            NSOperationQueue.mainQueue().addOperationWithBlock({
                completion()
            })
        }
        self.mergeQueue.addOperation(op)
        
        
    }
    
    
    func scanDummyContacts(completion: (results:[ABRecordID]) -> Void, progress: ((stats:ContactsProcessingStatus) -> Void)?) {
        self.dummyScanQueue.cancelAllOperations()
        let op = DummyContactsScanOperation()
        op.contacts = self.contacts
        op.queuePriority = NSOperationQueuePriority.High
        op.progressBlock = {
            (stats: ContactsProcessingStatus) in
            NSOperationQueue.mainQueue().addOperationWithBlock({
                if progress != nil {
                    progress!(stats: stats)
                }
            })
        }
        op.completionBlock = {
            NSOperationQueue.mainQueue().addOperationWithBlock({
                completion(results: op.dummyRecordIds)
            })
        }
        self.dummyScanQueue.addOperation(op)
    }
    
    func removeContact(person: SwiftAddressBookPerson) {
        addressBook?.removeRecord(person)
        let error = saveAddressBook()
        if error != nil {
            logError("error removing record: \(error)")
        }
    }
    
    func cancelDummyContactsScan() {
        self.dummyScanQueue.cancelAllOperations()
    }
    
    
    func cleanDummies(records: [ABRecordID], progress: ((stats:ContactsProcessingStatus) -> Void)?, completion: (() -> Void)) {
        self.cleanQueue.cancelAllOperations()
        let op = CleanOperation()
        op.records = records
        op.queuePriority = NSOperationQueuePriority.High
        op.progressBlock = {
            (stats: ContactsProcessingStatus) in
            NSOperationQueue.mainQueue().addOperationWithBlock({
                if progress != nil {
                    progress!(stats: stats)
                }
            })
        }
        op.completionBlock = {
            NSOperationQueue.mainQueue().addOperationWithBlock({
                //                self.refreshContacts()
                completion()
            })
        }
        self.cleanQueue.addOperation(op)
    }
    
    
    func getICloudAccess() -> Bool {
        let cloudIsAvailable = sharedICloud.checkCloudAvailability()
        return cloudIsAvailable
    }
    
    func uploadToiCloud(fileName: String, data: NSData, completion: (() -> Void)) {
        let cloudIsAvailable = sharedICloud.checkCloudAvailability()
        if cloudIsAvailable {
            //YES
            sharedICloud.saveAndCloseDocumentWithName(fileName, withContent: data) {
                (cloudDocument: UIDocument!, documentData: NSData!, error: NSError!) in
                if error == nil {
                    // Code here to use the UIDocument or NSData objects which have been passed with the completion handler
                    logInfo("Contacts uploaded to iCloud")
                    completion()
                }
            }
        } else {
            logError("iCloud not available but iCloud Upload called")
        }
    }
    
    func getDropboxAccess(controller: UIViewController? = nil, completion: (() -> Void)) {
        if !DBSession.sharedSession().isLinked() {
            self.dropboxAccessCompletionBlock = completion
            var vc = controller
            if controller == nil {
                vc = self.appDelegate.window!.rootViewController!
            }
            DBSession.sharedSession().linkFromController(vc)
        } else {
            completion()
        }
    }
    
    func hasDropboxAccess() -> Bool {
        return DBSession.sharedSession().isLinked()
    }
    
    func unlinkDropbox() {
        DBSession.sharedSession().unlinkAll()
        Preferences.sharedInstance.dropboxAccount = kUnlinkedAccount
    }
    
    var dbUserInfo: DBAccountInfo?
    
    var dbOp: DropboxOperation?
    
    func getDropboxUser(completion: ((userName:String) -> Void)) {
        if dbUserInfo == nil {
            if dbOp == nil {
                dbOp = DropboxOperation()
            }
            dbOp!.completionBlock = {
                (success:Bool) in
                self.dbUserInfo = self.dbOp!.userInfo
                Preferences.sharedInstance.dropboxAccount = self.dbUserInfo!.displayName
                completion(userName: self.dbUserInfo!.displayName)
            }
            if dbOp!.userInfo == nil {
                dbOp!.getUserInfo()
            } else {
                self.dbUserInfo = dbOp!.userInfo
                Preferences.sharedInstance.dropboxAccount = self.dbUserInfo!.displayName
                completion(userName: dbUserInfo!.displayName)
            }
        } else {
            Preferences.sharedInstance.dropboxAccount = self.dbUserInfo!.displayName
            completion(userName: self.dbUserInfo!.displayName)
        }
    }
    
    
    func uploadToDropBox(fileName: String, path: String, controller: UIViewController, progress: ((progress:Float) -> Void), completion: ((success:Bool) -> Void)) {
        if !DBSession.sharedSession().isLinked() {
            logError("Dropbox Upload called but not linked")
        } else {
            dbOperation = DropboxOperation()
            dbOperation!.progressBlock = progress
            dbOperation!.completionBlock = completion
            dbOperation!.uploadFile(fileName, path: path)
        }
    }
    
    func fetchDropboxItems(controller: UIViewController, completion: ((results:[DBMetadata]?) -> Void)) {
        if !DBSession.sharedSession().isLinked() {
            DBSession.sharedSession().linkFromController(controller)
        } else {
            dbOperation = DropboxOperation()
            //            dbOperation!.progressBlock = progress
            dbOperation!.listFilesCompletionBlock = completion
            dbOperation!.listFiles()
        }
    }
    
    
    func getVCard(completion: ((vCard:NSData, vCardFileName:String) -> Void)) {
        NSOperationQueue().addOperationWithBlock({
            autoreleasepool {
                let vCard = ABPersonCreateVCardRepresentationWithPeople(self.addressBook?.allRecords).takeRetainedValue() as NSData
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyyMMdd-hhmmss"
                let dateString = dateFormatter.stringFromDate(NSDate())
                let vCardFileName = String(format: NSLocalizedString("Backup-%@.vcf", comment: ""), dateString)
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    completion(vCard: vCard, vCardFileName: vCardFileName)
                })
            }
        })
    }
    
    func saveVCard(completion: ((vCardPath:String, vCardFileName:String) -> Void)) {
        NSOperationQueue().addOperationWithBlock({
            autoreleasepool {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyyMMdd-hhmmss"
                let dateString = dateFormatter.stringFromDate(NSDate())
                let vCardFileName = String(format: NSLocalizedString("Backup-%@.vcf", comment: ""), dateString)
                let vCardPath = self.documentsPath.stringByAppendingPathComponent(vCardFileName)
                
                let records = self.addressBook?.allRecords
                var cfData = ABPersonCreateVCardRepresentationWithPeople(records)
                let vCard = cfData.takeUnretainedValue() as NSData
                vCard.writeToFile(vCardPath, atomically: true)
                cfData.release()
                
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    completion(vCardPath: vCardPath, vCardFileName: vCardFileName)
                })
            }
        })
    }
    func removeVCard(vCardPath:String) {
        NSOperationQueue().addOperationWithBlock({
            autoreleasepool {
                let fileManager = NSFileManager.defaultManager()
                var error:NSError?
                let success = fileManager.removeItemAtPath(vCardPath,error:&error)
                if (success) {
                    logDebug("successfully deleted VCard")
                }
                else
                {
                    logError("Error deleting VCard, \(vCardPath)")
                }
            }
        })
    }
    
    
    func getVCardFilesInPhone(completion: ((files:Array<Dictionary<String, AnyObject>>) -> Void)) {
        let fileManager = NSFileManager.defaultManager()
        var files = Array<Dictionary<String, AnyObject>>()
        if let enumerator: NSDirectoryEnumerator = fileManager.enumeratorAtPath(self.documentsPath) {
            while let element = enumerator.nextObject() as? String {
                if element.hasSuffix("vcf") {
                    // checks the extension
                    var item = Dictionary<String, AnyObject>()
                    if let attrs = fileManager.attributesOfItemAtPath(documentsPath.stringByAppendingPathComponent(element), error: nil) {
                        item["creationDate"] = attrs[NSFileCreationDate] as NSDate
                        item["size"] = attrs[NSFileSize] as NSNumber
                        
                        item["title"] = element
                        files.append(item)
                    }
                    
                    
                }
            }
            completion(files: files)
        } else {
            completion(files: files)
        }
    }
    
    
    func fetchVCardFromUrl(restoreUrl: NSURL, progress: ((progress:Float) -> Void)?, completion: ((data:NSData?) -> Void)) {
        let vdl = VCardDownloader()
        vdl.restoreUrl = restoreUrl
        vdl.progressBlock = progress
        vdl.completionBlock = completion
        vdl.main()
    }
    
    func readVCardFromLocalFile(fileName: String, completion: ((data:NSData?) -> Void)) {
        let filePath = self.documentsPath.stringByAppendingPathComponent(fileName)
        var data: NSData? = NSData(contentsOfFile: filePath)
        completion(data: data)
    }
    
    func readVCardFromICloudFile(fileName: String, completion: ((data:NSData?) -> Void)) {
        sharedICloud.retrieveCloudDocumentWithName(fileName) {
            (cloudDocument: UIDocument!, documentData: NSData!, error: NSError!) in
            if error == nil {
                completion(data: documentData)
            }
        }
    }
    
    func readVCardFromDropbox(fileName: String, progress: ((progress:Float) -> Void)?, completion: ((data:NSData?) -> Void)) {
        let localFilePath = self.documentsPath.stringByAppendingPathComponent("db" + fileName)
        if !DBSession.sharedSession().isLinked() {
            logError("Dropbox Download called but not linked")
        } else {
            dbOperation = DropboxOperation()
            dbOperation!.progressBlock = progress
            dbOperation!.completionBlock = {
                (success:Bool) in
                if let data = NSData(contentsOfFile: localFilePath) {
                    completion(data: data)
                }
            }
            dbOperation!.downloadFile("/" + fileName, localPath: localFilePath)
        }
    }
    
    func readVCardFromGoogleDrive(file: GTLDriveFile, progress: ((progress:Float) -> Void)?, completion: ((data:NSData?) -> Void)) {
        self.downloadFile(file, progress: progress, completion: completion)
    }
    
    var driveService: GTLServiceDrive?
    var gDriveAccessCompletionBlock: (() -> Void)?
    
    
    func unlinkGoogleDrive() {
        GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kKeychainItemName)
        self.driveService!.authorizer = nil
        self.driveService = nil
        Preferences.sharedInstance.googleDriveAccount = kUnlinkedAccount
    }
    
    func getGDriveAccess(controller: UIViewController? = nil, completion: (() -> Void)) {
        if self.driveService == nil {
            self.driveService = GTLServiceDrive()
        }
        if self.driveService!.authorizer == nil {
            self.driveService!.authorizer = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kKeychainItemName, clientID: kClientID, clientSecret: kClientSecret)
        }
        if !self.isGDriveAuthorized() {
            self.gDriveAccessCompletionBlock = completion
            let authViewController = self.createAuthController()
            //            authViewController.navigationItem.rightBarButtonItem = nil
            let cancelButton: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: UIBarButtonItemStyle.Plain,
                target: self, action: "didCanceledAuthorization:")
            authViewController.navigationItem.leftBarButtonItem = cancelButton
            authViewController.navigationItem.title = NSLocalizedString("Google Drive", comment: "")
            let nvc = UINavigationController(rootViewController: authViewController)
            
            self.appDelegate.window!.rootViewController!.presentViewController(nvc, animated: true,completion:nil)
        } else {
            completion()
        }
    }
    
    func getGDriveUser() -> String? {
        if let userEmail = self.driveService?.authorizer?.userEmail {
            Preferences.sharedInstance.googleDriveAccount = userEmail
            return userEmail
        }
        return nil
    }
    
    func didCanceledAuthorization(sender: AnyObject?) {
        self.appDelegate.window!.rootViewController!.presentedViewController?.dismissViewControllerAnimated(true) {
            
        }
    }
    
    
    func isGDriveAuthorized() -> Bool {
        if self.driveService == nil {
            self.driveService = GTLServiceDrive()
        }
        if self.driveService!.authorizer == nil {
            self.driveService!.authorizer = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kKeychainItemName, clientID: kClientID, clientSecret: kClientSecret)
        }
        return (self.driveService!.authorizer as GTMOAuth2Authentication).canAuthorize
    }
    
    func createAuthController() -> GTMOAuth2ViewControllerTouch {
        let authController = GTMOAuth2ViewControllerTouch(scope: kGTLAuthScopeDriveFile, clientID: kClientID, clientSecret: kClientSecret, keychainItemName: kKeychainItemName, delegate: self, finishedSelector: "viewController:finishedWithAuth:error:")
        return authController;
    }
    
    func viewController(viewController: GTMOAuth2ViewControllerTouch?, finishedWithAuth authResult: GTMOAuth2Authentication?, error: NSError?) {
        if error != nil {
            logError("error while authenticating Google Drive: \(error)")
            self.driveService!.authorizer = nil
        } else {
            self.driveService!.authorizer = authResult
            viewController!.dismissViewControllerAnimated(true) {
                if self.gDriveAccessCompletionBlock != nil {
                    self.gDriveAccessCompletionBlock!()
                }
            }
        }
    }
    
    func uploadFile(fileName: String, data: NSData, progress: ((progress:Float) -> Void), completion: ((success:Bool) -> Void)) {
        let dateFormat = NSDateFormatter()
        dateFormat.dateFormat = NSLocalizedString("'Quickstart Uploaded File ('EEEE MMMM d, YYYY h:mm a, zzz')", comment: "")
        let file = GTLDriveFile.object() as GTLDriveFile
        file.title = fileName
        file.descriptionProperty = NSLocalizedString("Uploaded from the Google Drive iOS Quickstart", comment: "")
        file.mimeType = kVCardMimeType
        
        
        let uploadParameters = GTLUploadParameters(data: data, MIMEType: file.mimeType)
        let query = GTLQueryDrive.queryForFilesInsertWithObject(file, uploadParameters: uploadParameters) as GTLQuery
        
        let handler = {
            (ticket: GTLServiceTicket?, insertedFileObject: AnyObject?, error: NSError?) -> Void in
            
            if error == nil {
                if let insertedFile = insertedFileObject as? GTLDriveFile {
                    logInfo("File ID: \(insertedFile.identifier)")
                    completion(success:true)
                }
                else{
                    logError("Could not get GTLDriveFile from Upload: \(insertedFileObject)")
                    completion(success:false)
                    
                }
            } else {
                logError("An error occurred while uploading to GDrive: \(error)")
                completion(success:false)
                
            }
            
            } as GTLServiceCompletionHandler
        let uploadTicket = self.driveService!.executeQuery(query, completionHandler: handler) as GTLServiceTicket
        uploadTicket.uploadProgressBlock = {
            (ticket: GTLServiceTicket!, totalBytesWritten: UInt64, totalBytesExpectedToWrite: UInt64) in
            let completed = Float(Double(totalBytesWritten) / Double(totalBytesExpectedToWrite))
            progress(progress: completed)
        }
    }
    
    var driveFetcher: GTMHTTPFetcher?
    
    func downloadFile(file: GTLDriveFile, progress: ((progress:Float) -> Void)?, completion: ((data:NSData) -> Void)) {
        driveFetcher = driveService!.fetcherService.fetcherWithURLString(file.downloadUrl) as GTMHTTPFetcher
        driveFetcher!.beginFetchWithCompletionHandler({
            (data: NSData!, error: NSError!) in
            if error == nil {
                if progress != nil {
                    progress!(progress: 1.0)
                }
                // Do something with data
                completion(data: data)
            } else {
                
            }
        })
        
        //        var lastDataSize = 0
        var count = 0
        driveFetcher!.receivedDataBlock = {
            (data: NSData!) in
            if progress != nil {
                if self.driveFetcher!.response.expectedContentLength > -1 {
                    let completed = Float(Double(self.driveFetcher!.downloadedLength) / Double(self.driveFetcher!.response.expectedContentLength))
                    progress!(progress: completed)
                } else {
                    let completed = Float(Double(count) / Double(count + 500))
                    
                    progress!(progress: completed)
                    count += 1
                }
            }
            
        }
    }
    
    
    func listGDriveFiles(completion: ((results:GTLDriveFileList?) -> Void)) {
        var query = GTLQueryDrive.queryForFilesList() as GTLQueryDrive
        query.q = "mimeType = '\(kVCardMimeType)'"
        
        let handler = {
            (ticket: GTLServiceTicket?, result: AnyObject?, error: NSError?) -> Void in
            if error == nil {
                let files = result as GTLDriveFileList?
                completion(results: files)
            } else {
                logError("error fetching list of files from google drive")
                completion(results: nil)
            }
            } as GTLServiceCompletionHandler
        
        self.driveService!.executeQuery(query, completionHandler: handler)
    }
    
    
    func generateSmartFavorites(completion: (() -> Void)? = nil) {
        self.favoriteProcessingQueue.cancelAllOperations()
        autoreleasepool({
            var favOp: SmartFavoritesOperation! = SmartFavoritesOperation()
            favOp.contacts = self.contacts
            favOp.queuePriority = NSOperationQueuePriority.High
            favOp.completionBlock = {
                NSOperationQueue.mainQueue().addOperationWithBlock({
                    if completion != nil {
                        completion!()
                    }
                })
            }
            self.favoriteProcessingQueue.addOperation(favOp)
        })
    }
    
    func generateSmartGroups(completion: (() -> Void)? = nil) {
        NSOperationQueue().addOperationWithBlock({
            logDebug("inside generateSmartGroups")
            SCSearch.addEntry("facebook", title: "Contacts in Facebook", scope: "Social")
            SCSearch.addEntry("twitter", title: "Contacts in Twitter", scope: "Social")
            SCSearch.addEntry("linkedin", title: "Contacts in Linkedin", scope: "Social")
            SCSearch.addEntry("plus.google", title: "Contacts in Google Plus", scope: "Social")
            
            var locationGroupsGenerated = false
            if let street = Preferences.sharedInstance.userInfo?.street {
                logDebug("creating Search for street \(street)")
                SCSearch.addEntry(street, title: String(format: NSLocalizedString("Contacts in %@", comment: ""), street), scope: "Address")
                locationGroupsGenerated = true
            }
            if let city = Preferences.sharedInstance.userInfo?.city {
                logDebug("creating Search for city \(city)")
                SCSearch.addEntry(city, title: String(format: NSLocalizedString("Contacts in %@", comment: ""), city), scope: "Address")
                locationGroupsGenerated = true
            }
            if let state = Preferences.sharedInstance.userInfo?.state {
                logDebug("creating Search for state \(state)")
                SCSearch.addEntry(state, title: String(format: NSLocalizedString("Contacts in %@", comment: ""), state), scope: "Address")
                locationGroupsGenerated = true
            }
            if !locationGroupsGenerated {
                logDebug("inside !locationGroupsGenerated")
                if let location = Preferences.sharedInstance.userInfo?.location {
                    logDebug("inside let location = Preferences.sharedInstance.userInfo?.location")
                    logDebug("creating Search for location \(location)")
                    var locations = split(location, { $0 == ","}, allowEmptySlices: false)
                    for value in locations {
                        let trimmedValue = value.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                        SCSearch.addEntry(trimmedValue, title: String(format: NSLocalizedString("Contacts in %@", comment: ""), trimmedValue), scope: "Address")
                        locationGroupsGenerated = true
                    }
                }
            }
            Preferences.sharedInstance.smartGroupsGenerated = true
        })
    }
    
    func cleanAppData() {
        //Clean all Models
        
        //Favorites - Keep Added, delete others
        Favorite.clean()
        //ContactLog - Clean All
        ContactLog.clean()
        //MergeItem - Clean All
        MergeItem.clean()
        //CleanItem - Clean All
        CleanItem.clean()
        //Actvitity - Clean All
        Activity.clean()
        //clean directories
        //all .vcf files, keep backup files && all Images
        let fileManager = NSFileManager.defaultManager()
        let enumerator: NSDirectoryEnumerator? = fileManager.enumeratorAtPath(self.libraryPath)
        while let element = enumerator?.nextObject() as? String {
            // do things with element
            var err: NSError?
            logDebug("deleting \(element)")
            let res = fileManager.removeItemAtPath(self.libraryPath.stringByAppendingPathComponent(element), error: &err)
            if !res && err != nil {
                logDebug("oops: \(err)")
            }
        }
    }
    
    func cleanMemoryCaches(){
        for contact in self.contacts{
            contact.clearCache()
        }
        dbOp = nil
    }
    
    
}

