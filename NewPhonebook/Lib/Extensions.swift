func telURL(string: String) -> NSURL {
    return NSURL(string: kCallPhoneNumberScheme + string)!
}

extension String {
    subscript(i: Int) -> String {
        return String(Array(self)[i])
    }
}


