#import <MacTypes.h>
#import "ABExternalChangeCallbackBridge.h"

void MyAddressBookExternalChangeCallback(
        ABAddressBookRef addressBook,
        CFDictionaryRef info,
        void *context
) {
    NSLog(@"external callback called ");
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AddressBookDidChangeExternalNotification" object:nil];
    });
}

@implementation ABExternalChangeCallbackBridge


+ (ABExternalChangeCallbackBridge *)sharedInstance {
    static ABExternalChangeCallbackBridge *sharedInstance = nil;

    @synchronized (self) {
        if (sharedInstance == nil) {
            sharedInstance = [[ABExternalChangeCallbackBridge alloc] init];
        }
    }
    return sharedInstance;
}


- (id)init {
    if ((self = [super init])) {
        //sharedInstance = self;
        addressBook = ABAddressBookCreateWithOptions(nil, nil);
        NSLog(@"registering external callback (Objective C Module)");
        ABAddressBookRegisterExternalChangeCallback(addressBook,
                MyAddressBookExternalChangeCallback,
                (__bridge void *) (self)
        );
    }
    return self;
}

@end