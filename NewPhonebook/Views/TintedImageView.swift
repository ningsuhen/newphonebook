//
//  TintedImageView.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/9/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import UIKit

@IBDesignable
class TintedImageView: UIImageView {

    @IBInspectable
    var tintImage: UIImage? {
        get {
            return image
        }
        set {
            if newValue != nil {
                image = newValue!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
            }
        }
    }
}
