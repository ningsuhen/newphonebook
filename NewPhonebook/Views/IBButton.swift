//
//  IBButton.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import UIKit

@IBDesignable
class IBButton: UIButton {

    @IBInspectable var borderColor: UIColor = UIColor.clearColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }


}
