//
//  NumberPad.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


protocol NumberPadDelegate: NSObjectProtocol {
    func callButtonTapped(numberValue: String)
    
    func addContactButtonTapped(numberValue: String)
}

let soundMap: Dictionary<String, SystemSoundID> = ["0": 1200, "1": 1201, "2": 1202, "3": 1203, "4": 1204, "5": 1205, "6": 1206, "7": 1207, "8": 1208, "9": 1209, "*": 1210, "#": 1201]

//@IBDesignable

class NumberPad: UIView, DialerButtonDelegateProtocol, UITextFieldDelegate {
    var delegate: NumberPadDelegate!
    var timer: NSTimer?
    
    @IBAction func longPressedDeleteButton(sender: UILongPressGestureRecognizer) {
        if (sender.state == UIGestureRecognizerState.Began) {
            if timer != nil {
                timer!.invalidate()
            }
            timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "deleteNumber:", userInfo: nil, repeats: true)
        } else if (sender.state == UIGestureRecognizerState.Ended || sender.state == UIGestureRecognizerState.Cancelled) {
            if timer != nil {
                timer!.invalidate()
            }
        }
    }
    @IBOutlet var addContactButton: UIButton!
    @IBOutlet var deleteButton: UIButton!
    @IBAction func deleteButtonClicked(sender: AnyObject) {
        currentNumber.becomeFirstResponder()
        self.deleteCurrentNumber()
    }
    
    func deleteNumber(timer: NSTimer) {
        deleteCurrentNumber()
    }
    
    func deleteCurrentNumber() {
        currentNumber.deleteBackward()
        if countElements(currentNumber.text) < 1 {
            deleteButton.hidden = true
            addContactButton.hidden = true
        }
    }
    
    @IBAction func addContactButtonTapped(sender: AnyObject) {
        if delegate != nil {
            delegate.addContactButtonTapped(currentNumber.text)
        }
    }
    
    @IBOutlet var currentNumber: UITextField!
    
    @IBOutlet var callButton: IBButton!
    
    @IBAction func callButtonTapped(sender: IBButton) {
        if delegate != nil {
            delegate.callButtonTapped(currentNumber.text)
        }
    }
    
    @IBOutlet var dialerButtons: [DialerButton]!
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String!) -> Bool {
        if countElements(currentNumber.text) > 0 && countElements(string) == 0 && countElements(currentNumber.text) == range.length {
            deleteButton.hidden = true
            addContactButton.hidden = true
        } else {
            deleteButton.hidden = false
            addContactButton.hidden = false
        }
        return textField == currentNumber
    }
    
    func dialerButtonTouchesBegan(sender: DialerButton) {
        currentNumber.becomeFirstResponder()
        if let typedValue = sender.button.titleForState(UIControlState.Normal) {
            if let soundId = soundMap[typedValue] {
                AudioServicesPlaySystemSound(soundId)
            }
            showAccessories()
            currentNumber.insertText(typedValue)
        }
    }
    
    func dialerButtonTouchesEnded(sender: DialerButton) {
        if timer != nil {
            timer!.invalidate()
        }
    }
    
    func hideAccessories() {
        deleteButton.hidden = true
        addContactButton.hidden = true
    }
    
    func showAccessories() {
        deleteButton.hidden = false
        addContactButton.hidden = false
    }
    
    func dialerButtonLongPressed(button: DialerButton, sender: UIGestureRecognizer) {
        let localizedPlusString = NSLocalizedString("+", comment: "")
        if (sender.state == UIGestureRecognizerState.Began) {
            currentNumber.becomeFirstResponder()
            if localizedPlusString == button.secondaryLabel.text {
                timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "playSound:", userInfo: NSString(string: "0"), repeats: true)
                showAccessories()
                currentNumber.deleteBackward()
                currentNumber.insertText(localizedPlusString)
            } else if let typedValue = button.button.titleForState(UIControlState.Normal) {
                timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "playSound:", userInfo: NSString(string: typedValue), repeats: true)
            }
        } else if (sender.state == UIGestureRecognizerState.Ended || sender.state == UIGestureRecognizerState.Cancelled) {
            if timer != nil {
                timer!.invalidate()
            }
        }
    }
    
    func playSound(timer: NSTimer) {
        if let value = timer.userInfo as? NSString {
            if let soundID = soundMap[value] {
                AudioServicesPlaySystemSound(soundID)
            }
        }
    }
    
    private var proxyView: NumberPad!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        var view = self.loadNib()
        view.frame = self.bounds
        view.autoresizingMask = .FlexibleWidth | .FlexibleHeight
        self.proxyView = view
        self.addSubview(self.proxyView)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeAfterUsingCoder(aDecoder: NSCoder) -> AnyObject {
        if self.subviews.count == 0 {
            var view = self.loadNib()
            view.setTranslatesAutoresizingMaskIntoConstraints(false)
            let contraints = self.constraints()
            self.removeConstraints(contraints)
            view.addConstraints(contraints)
            view.proxyView = view
            return view
        }
        return self
    }
    
    private func loadNib() -> NumberPad {
        let bundle = NSBundle(forClass: self.dynamicType)
        var view = bundle.loadNibNamed("NumberPad", owner: nil, options: nil)[0] as NumberPad
        return view
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        currentNumber.delegate = self
        currentNumber.inputView = UIView(frame: CGRectZero)
        for button in dialerButtons{
            button.buttonDelegate = self
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    
}