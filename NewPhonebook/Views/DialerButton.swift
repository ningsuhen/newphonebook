//
//  DialerButton.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/5/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import UIKit
import AVFoundation

@objc protocol DialerButtonDelegateProtocol: NSObjectProtocol {
    
    func dialerButtonTouchesBegan(sender: DialerButton) -> Void
    
    optional func dialerButtonTouchesEnded(sender: DialerButton) -> Void
    
    func dialerButtonLongPressed(button: DialerButton, sender: UIGestureRecognizer) -> Void
}

func dictionaryOfNames(arr:UIView...) -> Dictionary<String,UIView> {
    var d = Dictionary<String,UIView>()
    for (ix,v) in enumerate(arr) {
        d["v\(ix+1)"] = v
    }
    return d
}

@IBDesignable
class DialerButton: UIView {
    
    var button: UIButton!
    var secondaryLabel: UILabel!
    
    @IBOutlet
    weak var buttonDelegate: DialerButtonDelegateProtocol?
    
    
    func buttonReleased(sender: AnyObject) {
        if buttonDelegate != nil && buttonDelegate!.dialerButtonTouchesEnded != nil {
            buttonDelegate!.dialerButtonTouchesEnded!(self)
        }
        else if let delegate = self.superview?.superview as? DialerButtonDelegateProtocol {
            if delegate.dialerButtonTouchesEnded != nil {
                delegate.dialerButtonTouchesEnded!(self)
            }
        }
    }
    
    func buttonClicked(sender: AnyObject) {
        if buttonDelegate != nil {
            buttonDelegate!.dialerButtonTouchesBegan(self)
        }
        else if let delegate = self.superview?.superview as? DialerButtonDelegateProtocol {
            delegate.dialerButtonTouchesBegan(self)
        }
    }
    
    
    func buttonLongPressed(sender: UIGestureRecognizer) {
        if buttonDelegate != nil {
            buttonDelegate!.dialerButtonLongPressed(self, sender: sender)
        }
        else if let delegate = self.superview?.superview as? DialerButtonDelegateProtocol {
            delegate.dialerButtonLongPressed(self, sender: sender)
        }
    }
    
    
    @IBInspectable var primaryText: String? = "" {
        didSet {
            self.button?.setTitle(primaryText, forState: UIControlState.Normal)
        }
    }
    @IBInspectable var secondaryText: String? = "" {
        didSet {
            self.secondaryLabel?.text = secondaryText
            
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clearColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
   
    @IBInspectable var labelOffset: CGFloat = 0{
        didSet{
            button.titleEdgeInsets = UIEdgeInsetsMake(labelOffset, 0.0, 0.0, 0.0)
            
        }
    }
    
    var mySound: SystemSoundID = 1000
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //set corners
        self.layoutIfNeeded()
        let radius = (self.frame.size.height / 2)
        
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = radius
        //resize button
        button.frame = self.bounds
        
        let height = self.bounds.height
        let titleLabelheight = ceil(height/2)
        let secondaryLabelHeight = ceil(height/6)
        
        
        // set font size
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Thin", size: titleLabelheight)
        //resize secondary label
        secondaryLabel.frame = CGRectMake(0, ((bounds.size.height + titleLabelheight)/2 - 4),
            bounds.size.width, secondaryLabelHeight)
        
        secondaryLabel.font = UIFont(name:"AppleSDGothicNeo-Light", size:secondaryLabelHeight)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createSubviews()
    }
    
    func createSubviews() {
        //create button
        
        button = UIButton.buttonWithType(UIButtonType.System) as? UIButton
        button.contentMode = UIViewContentMode.ScaleAspectFill
        
        
        
        
        //add event handlers
        button.addTarget(self, action: "buttonClicked:", forControlEvents: UIControlEvents.TouchDown)
        button.addTarget(self, action: "buttonReleased:", forControlEvents: UIControlEvents.TouchUpInside)
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: "buttonLongPressed:")
        gestureRecognizer.minimumPressDuration = 0.5
        gestureRecognizer.cancelsTouchesInView = false
        gestureRecognizer.delaysTouchesBegan = false
        gestureRecognizer.delaysTouchesEnded = false
        button.addGestureRecognizer(gestureRecognizer)
        //add to view
        addSubview(button)
        
        //add secondary label
        secondaryLabel = UILabel()
        secondaryLabel.textColor = UIColor.grayColor()
        
        
        
        secondaryLabel.textAlignment = NSTextAlignment.Center
        secondaryLabel.userInteractionEnabled = false
        addSubview(secondaryLabel)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        createSubviews()
    }
    
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
}
