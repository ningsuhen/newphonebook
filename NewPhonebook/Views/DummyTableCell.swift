//
//  DummyTableCell.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 1/21/15.
//  Copyright (c) 2015 NWK. All rights reserved.
//

import Foundation

class DummyTableCell: UITableViewCell {

    @IBOutlet var leftProfileImage: UIImageView!
    @IBOutlet var leftContactName: UILabel!
    @IBOutlet var checkboxButton: UIButton!

    weak var delegate: MergeTableCellDelegate?

    var cellIndex = -1
    var checkboxSelected: Bool = false {
        didSet {
            if checkboxSelected {
                checkboxButton.setImage(UIImage(named: IconSet.CheckboxTicked), forState: UIControlState.Normal)
                checkboxButton.tintColor = kDefaultTint
            } else {
                checkboxButton.setImage(UIImage(named: IconSet.Checkbox), forState: UIControlState.Normal)
                checkboxButton.tintColor = UIColor.lightGrayColor()
            }
        }
    }
    override var userInteractionEnabled: Bool {
        didSet {
            if !userInteractionEnabled {
                checkboxButton.tintColor = UIColor.lightGrayColor()
                checkboxButton.setImage(UIImage(named: IconSet.Checkbox), forState: UIControlState.Normal)
                var attributedString = NSMutableAttributedString(string: leftContactName.attributedText.string)
                attributedString.addAttribute(NSStrikethroughStyleAttributeName, value: NSNumber(int: 2), range: NSMakeRange(0, attributedString.length))
                leftContactName.attributedText = attributedString
                leftContactName.textColor = UIColor.lightGrayColor()
            } else {
                checkboxButton.tintColor = kDefaultTint
                var attributedString = NSMutableAttributedString(string: leftContactName.attributedText.string)
                leftContactName.attributedText = attributedString
                leftContactName.textColor = UIColor.blackColor()
            }
        }
    }


    @IBInspectable
    var showCheckbox: Bool = true {
        didSet {
            if showCheckbox && !checkboxButton.isDescendantOfView(self) {
                logDebug("addSubview")
                addSubview(checkboxButton)
            } else if !showCheckbox && checkboxButton.isDescendantOfView(self) {
                logDebug("removeFromSuperview")
                checkboxButton.removeFromSuperview()
            } else {
                logDebug("subview Nothing")
            }
        }
    }


}

