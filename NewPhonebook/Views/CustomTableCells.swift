//
//  CustomTableCells.swift
//  NewPhonebook
//
//  Created by Ningsuhen Waikhom on 12/23/14.
//  Copyright (c) 2014 NWK. All rights reserved.
//

import Foundation
import UIKit


typealias MergeDirection = Bool

struct MergeDirections {
    static let LeftToRight: MergeDirection = true
    static let RightToLeft: MergeDirection = false
}

protocol MergeTableCellDelegate: NSObjectProtocol {
    func leftCardTapped(cell: MergeTableCell)

    func rightCardTapped(cell: MergeTableCell)
}

class MergeTableCell: UITableViewCell {
    //@IBOutlet var checkbox: UIButton!
    @IBOutlet var leftProfileImage: UIImageView!
    @IBOutlet var rightProfileImage: UIImageView!
    @IBOutlet var mergeDirectionButton: UIButton!
    @IBOutlet var leftContactName: UILabel!
    @IBOutlet var rightContactName: UILabel!
    @IBOutlet var checkboxButton: UIButton!

    weak var delegate: MergeTableCellDelegate?

    var cellIndex = -1


    @IBAction func leftCardTapped(sender: UIButton) {
        delegate?.leftCardTapped(self)
    }


    @IBAction func rightCardTapped(sender: UIButton) {
        delegate?.rightCardTapped(self)
    }


    @IBInspectable
    var showCheckbox: Bool = true {
        didSet {
            if showCheckbox && !checkboxButton.isDescendantOfView(self) {
                addSubview(checkboxButton)
            } else if !showCheckbox && checkboxButton.isDescendantOfView(self) {
                checkboxButton.removeFromSuperview()
            } else {
                //placeholder, nothing to do for now
            }
        }
    }

    override var userInteractionEnabled: Bool {
        didSet {
            if !userInteractionEnabled {
                mergeDirectionButton.tintColor = UIColor.lightGrayColor()
                checkboxButton.tintColor = UIColor.lightGrayColor()
                checkboxButton.setImage(UIImage(named: IconSet.Checkbox), forState: UIControlState.Normal)
            } else {
                mergeDirectionButton.tintColor = kDefaultTint
                checkboxButton.tintColor = kDefaultTint
            }
        }
    }

    var direction: MergeDirection = MergeDirections.RightToLeft {
        didSet {
            if direction == MergeDirections.LeftToRight {
                // Left item is disabled
                leftContactName.textColor = UIColor.lightGrayColor()
                var attributedString = NSMutableAttributedString(string: leftContactName.attributedText.string)
                attributedString.addAttribute(NSStrikethroughStyleAttributeName, value: NSNumber(int: 2), range: NSMakeRange(0, attributedString.length))
                leftContactName.attributedText = attributedString

                // direction
                mergeDirectionButton.setImage(UIImage(named: IconSet.MergeRight), forState: UIControlState.Normal)

                // Right item is enabled
                rightContactName.textColor = UIColor.blackColor()
                var attributedString2 = NSMutableAttributedString(string: rightContactName.attributedText.string)
                rightContactName.attributedText = attributedString2
            } else {
                // Left item is enabled
                leftContactName.textColor = UIColor.blackColor()
                var attributedString = NSMutableAttributedString(string: leftContactName.attributedText.string)
                leftContactName.attributedText = attributedString

                // direction
                mergeDirectionButton.setImage(UIImage(named: IconSet.MergeLeft), forState: UIControlState.Normal)

                // Right item is disabled
                rightContactName.textColor = UIColor.lightGrayColor()
                var attributedString2 = NSMutableAttributedString(string: rightContactName.attributedText.string)
                attributedString2.addAttribute(NSStrikethroughStyleAttributeName, value: NSNumber(int: 2), range: NSMakeRange(0, attributedString2.length))
                rightContactName.attributedText = attributedString2
            }
        }
    }
}

class MergeItemTableCell: UITableViewCell {
    @IBOutlet var leftProfileImage: UIImageView!
    @IBOutlet var rightProfileImage: UIImageView!
    @IBOutlet var leftContactName: UILabel!
    @IBOutlet var rightContactName: UILabel!
}

class ContactsTableCell: UITableViewCell {
    @IBOutlet var fullName: UILabel!
    @IBOutlet var picture: UIImageView!
    @IBOutlet var status: UILabel!
    @IBOutlet var callButton: UIButton!
    @IBOutlet var fullNameCentered: UILabel!
}

class ToolsTableViewCell: UITableViewCell {
    @IBOutlet var toolIcon: UIImageView!

    @IBOutlet var toolInstruction: UILabel!
    @IBOutlet var toolDescription: UILabel!
    @IBOutlet var toolName: UILabel!
}


class FavoriteTableCell: UITableViewCell {

    @IBOutlet var subStatus: UILabel!
    @IBOutlet var status: UILabel!
    @IBOutlet var fullName: UILabel!
    @IBOutlet var picture: UIImageView!


}

class GroupsTableCell: UITableViewCell {
    
    @IBOutlet var icon: TintedImageView!
    
    @IBOutlet var descriptionLabel: UILabel!
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var tagLabel: UILabel!
    
}


class SettingsTableCell: UITableViewCell {
    @IBOutlet var button: UIButton!
    @IBOutlet var subTitle: UILabel!
    @IBOutlet var title: UILabel!
    @IBOutlet var icon: TintedImageView!
    var identifier: String!
    weak var delegate: SettingsTableCellDelegate?
    @IBAction func buttonTapped(sender: UIButton) {
        if delegate != nil {
            delegate?.buttonTappedForCell(self)
        }
    }
}

//@IBDesignable

class PaddingTableCell: UITableViewCell {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    @IBInspectable var borderColor: UIColor = UIColor.clearColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var inset: CGFloat = 6


    override var frame: CGRect {
        get {
            return super.frame
        }
        set {
            //            newValue.origin.x = CGFloat(6)
            //            newValue.size.width = CGFloat(6)
            var newRect = CGRect(origin: newValue.origin, size: newValue.size)
            newRect.origin.x = newValue.origin.x + CGFloat(inset)
            newRect.size.width = newValue.size.width - CGFloat(2 * inset)
            super.frame = newRect
        }
    }

}

typealias ButtonTableCellType = Int

struct ButtonTableCellTypes {
    static let Default: ButtonTableCellType = 0
    static let Plain: ButtonTableCellType = 1
    static let Subtitle: ButtonTableCellType = 2
}


class ButtonTableCell: UITableViewCell {

    @IBOutlet var icon: UIImageView!

    @IBOutlet var title: UILabel!

    @IBOutlet var altTitle: UILabel!

    @IBOutlet var subtitle: UILabel!

    @IBOutlet var progressView: UIProgressView!

    @IBOutlet var progressCount: UILabel!


    var iconImage: UIImage? {
        didSet {
            icon.image = iconImage
        }
    }

    var titleText: String? {
        get {
            if buttonType == ButtonTableCellTypes.Default {
                return title.text
            } else if buttonType == ButtonTableCellTypes.Plain {
                return altTitle.text
            }
            return ""
        }
        set {
            if buttonType == ButtonTableCellTypes.Default {
                title.text = newValue
            } else if buttonType == ButtonTableCellTypes.Plain {
                altTitle.text = newValue
            }

        }
    }
    override func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return false
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    @IBInspectable var borderColor: UIColor = UIColor.clearColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var inset: CGFloat = 6


    var buttonType: ButtonTableCellType = ButtonTableCellTypes.Default {
        didSet {
            if buttonType == ButtonTableCellTypes.Default {
                //
                title.hidden = false
                subtitle.hidden = false
                progressView.hidden = false
                progressCount.hidden = false

                altTitle.hidden = true
            } else if buttonType == ButtonTableCellTypes.Plain {
                title.hidden = true
                subtitle.hidden = true
                progressView.hidden = true
                progressCount.hidden = true

                altTitle.hidden = false
            }
        }
    }

    override var frame: CGRect {
        get {
            return super.frame
        }
        set {
            var newRect = CGRect(origin: newValue.origin, size: newValue.size)
            newRect.origin.x = newValue.origin.x + CGFloat(inset)
            newRect.size.width = newValue.size.width - CGFloat(2 * inset)
            super.frame = newRect
        }
    }
}

protocol SettingsTableCellDelegate: NSObjectProtocol {
    func buttonTappedForCell(cell: SettingsTableCell)
}









